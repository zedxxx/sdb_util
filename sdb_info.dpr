program sdb_info;

uses
  Forms,
  MidasLib,
  frm_MainInfo in 'src\frm_MainInfo.pas' {frmMainInfo},
  VirtualTrees in 'includes\VirtualTreeView\VirtualTrees.pas',
  VTAccessibility in 'includes\VirtualTreeView\VTAccessibility.pas',
  VTAccessibilityFactory in 'includes\VirtualTreeView\VTAccessibilityFactory.pas',
  VTHeaderPopup in 'includes\VirtualTreeView\VTHeaderPopup.pas',
  u_Tools in 'src\u_Tools.pas',
  u_BerkeleyDBTools in 'src\u_BerkeleyDBTools.pas',
  u_ResourceStrings in 'src\u_ResourceStrings.pas',
  u_FileNameIteratorHelper in 'src\u_FileNameIteratorHelper.pas',
  u_BerkeleyDBVerifyCache in 'src\u_BerkeleyDBVerifyCache.pas',
  i_FormInfoConfig in 'src\i_FormInfoConfig.pas',
  u_FormInfoConfig in 'src\u_FormInfoConfig.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMainInfo, frmMainInfo);
  Application.Run;
end.
