program sdb_util;

uses
  Windows,
  Forms,
  frm_Main in 'src\frm_Main.pas' {frmMain},
  frm_Config in 'src\frm_Config.pas' {frmConfig},
  u_BerkeleyUtils in 'src\u_BerkeleyUtils.pas',
  u_BerkeleyUtilsConfig in 'src\u_BerkeleyUtilsConfig.pas',
  i_ConsoleAppRunner in 'src\i_ConsoleAppRunner.pas',
  u_ConsoleAppRunner in 'src\u_ConsoleAppRunner.pas',
  u_ResourceStrings in 'src\u_ResourceStrings.pas',
  u_FormConfig in 'src\u_FormConfig.pas',
  i_BerkeleyUtilsConfig in 'src\i_BerkeleyUtilsConfig.pas',
  i_FormConfig in 'src\i_FormConfig.pas',
  i_BerkeleyUtils in 'src\i_BerkeleyUtils.pas',
  i_DbConfigPresetList in 'src\i_DbConfigPresetList.pas',
  u_DbConfigPresetList in 'src\u_DbConfigPresetList.pas',
  u_BerkeleyDBVerifyCache in 'src\u_BerkeleyDBVerifyCache.pas',
  i_BerkeleyDBVerifyCache in 'src\i_BerkeleyDBVerifyCache.pas',
  u_BerkeleyDBTools in 'src\u_BerkeleyDBTools.pas',
  u_Tools in 'src\u_Tools.pas',
  t_BerkeleyDB in 'src\t_BerkeleyDB.pas',
  i_MainConfig in 'src\i_MainConfig.pas',
  u_MainConfig in 'src\u_MainConfig.pas',
  u_LogFile in 'src\u_LogFile.pas',
  i_LogFile in 'src\i_LogFile.pas',
  u_BerkeleyDBVerifyCacheImplDirect in 'src\u_BerkeleyDBVerifyCacheImplDirect.pas',
  frm_ConfigLog in 'src\frm_ConfigLog.pas' {frmConfigLog},
  c_BerkeleyUtils in 'src\c_BerkeleyUtils.pas';

{$R *.res}

{$SetPEFlags IMAGE_FILE_RELOCS_STRIPPED}
{$SetPEFlags IMAGE_FILE_EXECUTABLE_IMAGE}

{$IFNDEF DEBUG}
  {$SetPEFlags IMAGE_FILE_DEBUG_STRIPPED}
  {$SetPEFlags IMAGE_FILE_LINE_NUMS_STRIPPED}
  {$SetPEFlags IMAGE_FILE_LOCAL_SYMS_STRIPPED}
{$ENDIF}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
