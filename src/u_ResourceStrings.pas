unit u_ResourceStrings;

interface

resourcestring

  rsHint = 'Hint: To recover broken cache, just select folder and press Run';
  rsRun = 'Run';
  rsAbort = 'Abort';
  rsOk = 'Ok';
  rsErrors = 'Errors';
  rsSkipped = 'Skipped';
  rsTotalProcessed = 'Total processed';
  rsNothingToDo = 'Nothing to do!';
  rsVerifyCacheNotAvailable = 'Verify cache is unavailable!' + #13#10 +
    'Press <Ignore>, to continue in any case.';

  rsBrokenFileIsRenamed = 'Broken file is renamed';
  rsCantRenameBrokenFile = 'Can''t rename broken file';
  rsBrokenFileIsDeleted = 'Broken file is deleted';
  rsCantDeleteBrokenFile = 'Can''t delete broken file';
  rsEnvIsDeleted = 'Env is deleted';
  rsCantDeleteEnv = 'Can''t delete env';
  rsCantDeleteDbConfig = 'Can''t delete DB_CONFIG';
  rsCantDeleteDumpFile = 'Can''t delete dump file';
  rsCreateDump = 'Create dump';
  rsRestoreDataFromDump = 'Restore data from dump';
  rsVerify = 'Verify';
  rsResetLSN = 'Reset LSN';

implementation

end.
