unit u_LogFile;

interface

uses
  Classes,
  i_LogFile;

type
  TLogFile = class(TInterfacedObject, ILogFile)
  private
    FFileName: string;
    FFileStream: TFileStream;
    procedure Open;
    procedure Close;
  private
    { ILogFile }
    procedure Truncate;
    procedure WriteText(const AText: string);
  public
    constructor Create(const AFileName: string);
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils;

{ TLogFile }

constructor TLogFile.Create(const AFileName: string);
begin
  inherited Create;
  FFileName := AFileName;
  FFileStream := nil;
end;

destructor TLogFile.Destroy;
begin
  Close;
  inherited Destroy;
end;

procedure TLogFile.Open;
begin
  if not Assigned(FFileStream) then begin
    if not FileExists(FFileName) then begin
      Truncate;
    end;
    FFileStream := TFileStream.Create(FFileName, fmOpenWrite or fmShareDenyNone);
  end;
end;

procedure TLogFile.Close;
begin
  if Assigned(FFileStream) then begin
    FreeAndNil(FFileStream);
  end;
end;

procedure TLogFile.Truncate;
begin
  FFileStream := TFileStream.Create(FFileName, fmCreate);
  FreeAndNil(FFileStream);
end;

procedure TLogFile.WriteText(const AText: string);
var
  VText: UTF8String;
begin
  if AText <> '' then begin
    Open;
    VText := AnsiToUtf8(AText);
    FFileStream.WriteBuffer(VText[1], Length(VText));
  end;
end;

end.
