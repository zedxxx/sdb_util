unit u_Tools;

interface

uses
  Windows;

function FileTimeToInt64(const AFileTime: TFileTime): Int64; inline;
function DateTimeToFileTimeInt64(const ADateTime: TDateTime): Int64; inline;
function FileTime64ToDateTime(const AFileTime: Int64): TDateTime; inline;
function GetFileSize(const AFileName: string): Int64;

function GetFileInfo(
  const AFileName: string;
  out ASize: Int64;
  out ACreationTime: Int64;
  out ALastWriteTime: Int64
): Boolean;

function CheckIfSameFile(
  const AFileName: string;
  const ASize: Int64;
  const ACreationTime: Int64;
  const ALastWriteTime: Int64
): Boolean;

function DataSizeToStr(const ASize: Int64): string;

function GetAppVersion: string;

implementation

uses
  Classes,
  SysUtils;

function FileTimeToInt64(const AFileTime: TFileTime): Int64; inline;
begin
  Result :=
    Int64(AFileTime.dwHighDateTime) shl Int64(32) +
    Int64(AFileTime.dwLowDateTime);
end;

function DateTimeToFileTimeInt64(const ADateTime: TDateTime): Int64; inline;
var
  VSysTime: TSystemTime;
  VLocalFileTime, VFileTime: TFileTime;
begin
  DateTimeToSystemTime(ADateTime, VSysTime);
  if not SystemTimeToFileTime(VSysTime, VLocalFileTime) then begin
    RaiseLastOSError;
  end;
  if not LocalFileTimeToFileTime(VLocalFileTime, VFileTime) then begin
    RaiseLastOSError;
  end;
  Result := FileTimeToInt64(VFileTime);
end;

function FileTime64ToDateTime(const AFileTime: Int64): TDateTime;
var
  VSysTime: TSystemTime;
  VLocalFileTime, VFileTime: TFileTime;
begin
  VFileTime.dwLowDateTime := AFileTime and Int64($FFFFFFFF);
  VFileTime.dwHighDateTime := AFileTime shr 32;
  if not FileTimeToLocalFileTime(VFileTime, VLocalFileTime) then begin
    RaiseLastOSError;
  end;
  if not FileTimeToSystemTime(VLocalFileTime, VSysTime) then begin
    RaiseLastOSError;
  end;
  Result := SystemTimeToDateTime(VSysTime)
end;

{$WARN SYMBOL_PLATFORM OFF}
function GetFileSize(const AFileName: string): Int64;
var
  VSearch: TSearchRec;
begin
  if SysUtils.FindFirst(AFileName, faAnyFile, VSearch) = 0 then begin
    Result :=
      Int64(VSearch.FindData.nFileSizeHigh) shl Int64(32) +
      Int64(VSearch.FindData.nFileSizeLow);
  end else begin
    Result := -1;
  end;
  SysUtils.FindClose(VSearch);
end;

function GetFileInfo(
  const AFileName: string;
  out ASize: Int64;
  out ACreationTime: Int64;
  out ALastWriteTime: Int64
): Boolean;
var
  VSearch: TSearchRec;
begin
  if SysUtils.FindFirst(AFileName, faAnyFile, VSearch) = 0 then begin
    ACreationTime := FileTimeToInt64(VSearch.FindData.ftCreationTime);
    ALastWriteTime := FileTimeToInt64(VSearch.FindData.ftLastWriteTime);
    ASize :=
      Int64(VSearch.FindData.nFileSizeHigh) shl Int64(32) +
      Int64(VSearch.FindData.nFileSizeLow);
    Result := True;
  end else begin
    Result := False;
  end;
  SysUtils.FindClose(VSearch) ;
end;
{$WARN SYMBOL_PLATFORM ON}

function CheckIfSameFile(
  const AFileName: string;
  const ASize: Int64;
  const ACreationTime: Int64;
  const ALastWriteTime: Int64
): Boolean;
var
  VSize: Int64;
  VCreationTime: Int64;
  VLastWriteTime: Int64;
begin
  if GetFileInfo(AFileName, VSize, VCreationTime, VLastWriteTime) then begin
    Result :=
      (ASize = VSize) and
      (ACreationTime = VCreationTime) and
      (ALastWriteTime = VLastWriteTime);
  end else begin
    Result := False;
  end;
end;

function DataSizeToStr(const ASize: Int64): string;
var
  VSize: Double;
begin
  VSize := ASize / 1024;
  if VSize > (1024 * 1024) then begin
    Result := FormatFloat('0.0', VSize / (1024 * 1024)) + ' GB';
  end else begin
    if VSize > 1024 then begin
      Result := FormatFloat('0.0', VSize / 1024) + ' MB';
    end else begin
      Result := FormatFloat('0.0', VSize) + ' kB';
    end;
  end;
end;

function GetAppVersion: string;
type
  TVerInfo=packed record
    Nevazhno: array[0..47] of Byte;
    Minor, Major, Build, Release: Word;
  end;
var
  VInfo: TVerInfo;
  VStream: TResourceStream;
begin
  Result:='';
  try
    VStream := TResourceStream.Create(HInstance,'#1', RT_VERSION);
    try
      if VStream.Size > 0 then begin
        VStream.ReadBuffer(VInfo, SizeOf(VInfo));
        Result := 'v' +
          IntToStr(VInfo.Major) + '.' +
          IntToStr(VInfo.Minor) + '.' +
          IntToStr(VInfo.Release) + '.' +
          IntToStr(VInfo.Build);
      end;
    finally
      VStream.Free;
    end;
  except;
    Result := '';
  end;
end;

end.
