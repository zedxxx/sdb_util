unit i_LogFile;

interface

type
  ILogFile = interface
    ['{ABCD0505-EF7C-494B-9877-479402ECD8B0}']
    procedure Truncate;
    procedure WriteText(const AText: string);
  end;

implementation

end.
