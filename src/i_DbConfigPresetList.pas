unit i_DbConfigPresetList;

interface

type
  TDbConfigPresetType = (
    dbcpLowDurability       = 0,
    dbcpNormalDurability    = 1,
    dbcpHighDurability      = 2,
    dbcpExtraHighDurability = 3,
    dbcpUserDefined         = 4
  );

  IDbConfigPresetListStatic = interface
    ['{ED9AA4BC-6AA6-4265-A282-E9A520DDBA7C}']
    function GetPresetItem(const AType: TDbConfigPresetType): string;
    property Item[const AType: TDbConfigPresetType]: string read GetPresetItem;

    function GetActivePreset: TDbConfigPresetType;
    property Preset: TDbConfigPresetType read GetActivePreset;
  end;

  IDbConfigPresetList = interface
    ['{E150A646-CB8B-421C-B56A-FDBCE38CD562}']
    function GetPresetItem(const AType: TDbConfigPresetType): string;
    procedure SetPresetItem(const AType: TDbConfigPresetType; const AValue: string);
    property Item[const AType: TDbConfigPresetType]: string read GetPresetItem write SetPresetItem;

    function GetActivePreset: TDbConfigPresetType;
    procedure SetActivePreset(const AValue: TDbConfigPresetType);
    property Preset: TDbConfigPresetType read GetActivePreset write SetActivePreset;

    function CreateStatic: IDbConfigPresetListStatic;
  end;

implementation

end.
