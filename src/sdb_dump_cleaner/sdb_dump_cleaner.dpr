program sdb_dump_cleaner;

{$APPTYPE CONSOLE}
{$IOCHECKS ON}

{$R VersionInfo.res}

uses
  Types,
  Classes,
  SysUtils,
  i_BerkeleyDBKeyValue in 'include\i_BerkeleyDBKeyValue.pas',
  i_BinaryData in 'include\i_BinaryData.pas',
  u_BerkeleyDBKey in 'include\u_BerkeleyDBKey.pas',
  u_BerkeleyDBValue in 'include\u_BerkeleyDBValue.pas',
  u_BerkeleyDBValueZlib in 'include\u_BerkeleyDBValueZlib.pas',
  u_BinaryDataByMemStream in 'include\u_BinaryDataByMemStream.pas',
  ALZLibEx in 'include\alcinoe\source\ALZLibEx.pas',
  ALZLibExApi in 'include\alcinoe\source\ALZLibExApi.pas',
  CRC32 in 'include\CRC32.pas',
  u_InOut in 'u_InOut.pas';

{$DEFINE LOG_KEY_VAL_VERIFY_ERRORS}

type
  TDataRec = record
    MemPtr: Pointer;
    MemSize: Integer;
    DataSize: Integer;
  end;
  PDataRec = ^TDataRec;

var
  gKeyRec, gValueRec: TDataRec;
  gKeyValVerifyErrorsCount: Int64 = 0;

procedure WriteError(const E: Exception);
begin
  Writeln(ErrOutput, 'sdb_dump_cleaner: [' + E.Classname + '] ' + E.Message);
end;

function CheckBerkeleyDBKeyVal(const AKey, AValue: string): Boolean;

  function IsValidHexStr(const AStr: string): Boolean;
  begin
    Result := (AStr <> '') and ((Length(AStr) mod 2) = 0);
  end;

  function TryHexStrToBin(const AStr: string; const ARec: PDataRec): Boolean;
  begin
    ARec.DataSize := Length(AStr) div 2;
    if ARec.MemSize < ARec.DataSize then begin
      ReallocMem(ARec.MemPtr, ARec.DataSize);
      ARec.MemSize := ARec.DataSize;
    end;
    Result := HexToBin(PChar(AStr), ARec.MemPtr, ARec.DataSize) = ARec.DataSize;
  end;

var
  VKey: IBerkeleyDBKey;
  VVerKey: IBerkeleyDBVersionedKey;
  VValue: IBerkeleyDBValue;
  VMetaValue: IBerkeleyDBMetaValue;
  VVerMetaValue: IBerkeleyDBVersionedMetaValue;
begin
  try
    // �������� �� ���������� ������� �����
    if not IsValidHexStr(AKey) then begin
      raise Exception.Create('Invalid hex string: ' + AKey);
    end;
    if not IsValidHexStr(AValue) then begin
      raise Exception.Create('Invalid hex string: ' + AValue);
    end;

    // ��������������� ����� � ������
    if not TryHexStrToBin(AKey, @gKeyRec) then begin
      raise Exception.Create('Can''t convert str to binary: ' + AKey);
    end;
    if not TryHexStrToBin(AValue, @gValueRec) then begin
      raise Exception.Create('Can''t convert str to binary: ' + AValue);
    end;

    // ��������� ������
    if gKeyRec.DataSize = 8 then begin
      VKey := TBerkeleyDBKey.Create(gKeyRec.MemPtr, gKeyRec.DataSize, False);
      if IsMetaKey(VKey) then begin
        VMetaValue := TBerkeleyDBMetaValue.Create(0);
        if not VMetaValue.Assign(gValueRec.MemPtr, gValueRec.DataSize, False) then begin
          raise Exception.Create('Meta-Value assign filed: ' + AValue);
        end;
      end else begin
        VValue := TBerkeleyDBValue.Create(gValueRec.MemPtr, gValueRec.DataSize, False);
      end;
    end else begin
      VVerKey := TBerkeleyDBVersionedKey.Create(Point(0, 0), 0);
      if not VVerKey.Assign(gKeyRec.MemPtr, gKeyRec.DataSize, False) then begin
        raise Exception.Create('Versioned meta-key assign filed: ' + AKey);
      end;
      if IsMetaKey(VVerKey) then begin
        VVerMetaValue := TBerkeleyDBVersionedMetaValue.Create(
          gValueRec.MemPtr, gValueRec.DataSize, False
        );
      end else begin
        VValue := TBerkeleyDBValue.Create(gValueRec.MemPtr, gValueRec.DataSize, False);
      end;
    end;

    Result := True;
  except
    on E: Exception do begin
      Inc(gKeyValVerifyErrorsCount);
      {$IFDEF LOG_KEY_VAL_VERIFY_ERRORS}
      WriteError(E);
      {$ENDIF}
      Result := False;
    end;
  end;
end;

procedure ProcessDump(
  const AReader: TInputReader;
  const AWriter: TOutputWriter
);
var
  VLen: Integer;
  VIsDataStarted: Boolean;
  VLine, VKey, VValue: string;
begin
  Assert(AReader <> nil);
  Assert(AWriter <> nil);

  VIsDataStarted := False;

  // �������� "����������" ��������� ���������� �����
  AWriter.WriteLine('VERSION=3');
  AWriter.WriteLine('format=bytevalue');
  AWriter.WriteLine('database=');
  AWriter.WriteLine('type=btree');
  AWriter.WriteLine('db_pagesize=1024');
  AWriter.WriteLine('HEADER=END');

  // ���������� ������ �����
  while not AReader.IsEOF do begin
    VLen := AReader.ReadLine(VLine);

    // ���������� ��������� � ���� ��������� ����� ����� ���
    if not VIsDataStarted then begin
      VIsDataStarted := (VLine = 'HEADER=END');
      Continue;
    end;

    // ���� ������ �� ����� ��������� � ������ �����
    // (16 - ������������, 20 - ����������), ��:
    if (VLen = 16) or (VLen = 20) then begin
      VKey := VLine;
      // ������ ��������� ������� - ��� ���������������� ����
      VLen := AReader.ReadLine(VValue);
      // �� ��������� ������� ������, ��� ���� ������ ����� ��� ������,
      // ��, ����� �� �������� �����, ���������� "��������" ����� ����������
      // �� ��� ���, ���� ����� ������ ������ �� ���������� ��������� ����� �����:
      while (VLen = 16) or (VLen = 20) do begin
        VKey := VValue;
        VLen := AReader.ReadLine(VValue);
      end;

      // ���������, ��� Key � Value �������� �������� ������
      if CheckBerkeleyDBKeyVal(VKey, VValue) then begin
        AWriter.WriteLine(' ' + VKey);
        AWriter.WriteLine(' ' + VValue);
      end;
    end;
  end;

  // �������� ����� �����
  AWriter.WriteLine('DATA=END');
end;

procedure InitKeyValRec;
begin
  with gKeyRec do begin
    MemPtr := nil;
    MemSize := 0;
    DataSize := 0;
  end;
  with gValueRec do begin
    MemPtr := nil;
    MemSize := 0;
    DataSize := 0;
  end;
end;

procedure FinKeyValRec;
begin
  if gKeyRec.MemPtr <> nil then begin
    FreeMem(gKeyRec.MemPtr);
    gKeyRec.MemPtr := nil;
  end;
  if gValueRec.MemPtr <> nil then begin
    FreeMem(gValueRec.MemPtr);
    gValueRec.MemPtr := nil;
  end;
end;

procedure ReadCmdLineParams(out AInFileName, AOutFileName: string);
var
  I, VCount: Integer;
begin
  AInFileName := '';
  AOutFileName := '';
  VCount := ParamCount;
  for I := 1 to VCount do begin
    if LowerCase(ParamStr(I)) = '-fi' then begin
      if I < VCount then begin
        AInFileName := ParamStr(I+1);
      end else begin
        raise Exception.Create('Input file name not specified!');
      end;
    end;
    if LowerCase(ParamStr(I)) = '-fo' then begin
      if I < VCount then begin
        AOutFileName := ParamStr(I+1);
      end else begin
        raise Exception.Create('Output file name not specified!');
      end;
    end;
  end;
end;

var
  VReader: TInputReader;
  VWriter: TOutputWriter;
  VInFileName, VOutFileName: string;
begin
  try
    VReader := TInputReader.Create;
    try
      VWriter := TOutputWriter.Create;
      try
        ReadCmdLineParams(VInFileName, VOutFileName);
        if VInFileName <> '' then begin
          VReader.OpenFile(VInFileName);
        end;
        if VOutFileName <> '' then begin
          VWriter.OpenFile(VOutFileName);
        end;
        InitKeyValRec;
        try
          ProcessDump(VReader, VWriter);
        finally
          FinKeyValRec;
        end;
      finally
        FreeAndNil(VWriter);
      end;
    finally
      FreeAndNil(VReader);
    end;
    if gKeyValVerifyErrorsCount <> 0 then begin
      Writeln(
        ErrOutput,
        Format(
          'sdb_dump_cleaner: Found %d errors due cleaning',
          [gKeyValVerifyErrorsCount]
        )
      );
    end;
  except
    on E: Exception do begin
      WriteError(E);
    end;
  end;
end.
