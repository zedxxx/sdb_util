unit u_InOut;

interface

{$I+}

type
  TInputReader = class
  private
    FFile: TextFile;
    FIsFile: Boolean;
  public
    procedure OpenFile(const AFileName: string);
    function ReadLine(out ALine: string): Integer;
    function IsEOF: Boolean;
    constructor Create;
    destructor Destroy; override;
  end;

  TOutputWriter = class
  private
    FFile: TextFile;
    FIsFile: Boolean;
  public
    procedure OpenFile(const AFileName: string);
    procedure WriteLine(const ALine: string);
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils;

{ TInputReader }

constructor TInputReader.Create;
begin
  inherited Create;
  FIsFile := False;
end;

destructor TInputReader.Destroy;
begin
  if FIsFile then begin
    CloseFile(FFile);
  end;
  inherited Destroy;
end;

procedure TInputReader.OpenFile(const AFileName: string);
begin
  AssignFile(FFile, AFileName);
  Reset(FFile);
  FIsFile := True;
end;

function TInputReader.ReadLine(out ALine: string): Integer;
begin
  ALine := '';
  if FIsFile then begin
    ReadLn(FFile, ALine);
  end else begin
    ReadLn(ALine);
  end;
  ALine := Trim(ALine);
  Result := Length(ALine);
end;

function TInputReader.IsEOF: Boolean;
begin
  if FIsFile then begin
    Result := Eof(FFile);
  end else begin
    Result := Eof;
  end;
end;

{ TOutputWriter }

constructor TOutputWriter.Create;
begin
  inherited Create;
  FIsFile := False;
end;

destructor TOutputWriter.Destroy;
begin
  if FIsFile then begin
    CloseFile(FFile);
  end;
  inherited Destroy;
end;

procedure TOutputWriter.OpenFile(const AFileName: string);
begin
  AssignFile(FFile, AFileName);
  Rewrite(FFile);
  FIsFile := True;
end;

procedure TOutputWriter.WriteLine(const ALine: string);
begin
  if FIsFile then begin
    Writeln(FFile, ALine);
  end else begin
    Writeln(ALine);
  end;
end;

end.
