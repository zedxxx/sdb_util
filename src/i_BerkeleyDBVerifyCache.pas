unit i_BerkeleyDBVerifyCache;

interface

uses
  t_BerkeleyDB;

type
  TFileInfoRec = record
    FID: TRecID;
    FSize: Int64;
    FCreationTime: TDateTime;
    FLastWriteTime: TDateTime;
    FVerifyStatus: TBerkeleyDBVerifyStatus;
    FVerifyResult: Cardinal;
    FVerifyTime: TDateTime;
  end;

  IBerkeleyDBVerifyCache = interface
  ['{4B110CF6-92B9-4F6A-9C41-49468722ECAC}']
    function Add(
      const AFileName: string;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): TRecID;

    function Delete(
      const AID: TRecID
    ): Boolean;

    function Read(
      const AFileName: string;
      out AIsChanged: Boolean;
      out AFileInfo: TFileInfoRec
    ): Boolean;

    function Update(
      const AID: TRecID;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): Boolean;

    function UpdateFull(
      const AFileName: string;
      const AID: TRecID;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): Boolean;

    function GetIsAvailable: Boolean;
    property IsAvailable: Boolean read GetIsAvailable;
  end;

implementation

end.
