unit u_BerkeleyDBVerifyCacheImplDirect;

interface

uses
  Windows,
  SysUtils,
  ALSqlite3Wrapper,
  SQLite3Handler,
  t_BerkeleyDB,
  i_StringListStatic,
  i_BerkeleyDBVerifyCache;

type
  TBerkeleyDBVerifyCacheImplDirect = class(TInterfacedObject, IBerkeleyDBVerifyCache)
  private
    FLastCachePath: string;
    FLastCachePathID: TRecID;
    FSQLite3DbHandler: TSQLite3DbHandler;
    FSQLMakeSession: IStringListStatic;
    FSQLForNewTable: IStringListStatic;
    procedure InternalLoadParams;
    procedure OpenDB(const AFileName: TFileName);
    procedure CloseDB;
    function GetCachePathID(
      const ACachePath: string;
      const AForceCreateNew: Boolean
    ): TRecID;
  private
    procedure CallbackSelectCachePathID(
      const AHandler: PSQLite3DbHandler;
      const ACallbackPtr: Pointer;
      const AStmtData: PSQLite3StmtData
    );
    procedure CallbackSelectFileInfo(
      const AHandler: PSQLite3DbHandler;
      const ACallbackPtr: Pointer;
      const AStmtData: PSQLite3StmtData
    );
  private
    function Add(
      const AFileName: string;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): TRecID;

    function Delete(
      const AID: TRecID
    ): Boolean;

    function Read(
      const AFileName: string;
      out AIsChanged: Boolean;
      out AFileInfo: TFileInfoRec
    ): Boolean;

    function Update(
      const AID: TRecID;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): Boolean;

    function UpdateFull(
      const AFileName: string;
      const AID: TRecID;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): Boolean;

    function GetIsAvailable: Boolean;
  public
    constructor Create(const AFileName: string);
    destructor Destroy; override;
  end;

implementation

uses
  Classes,
  u_StringListStatic,
  u_Tools,
  u_BerkeleyDBTools;

const
  cTableFileInfo = 'FileInfo';
  cTableCachePath = 'CachePath';

function ALQuotedStr(const S: AnsiString; const Quote: AnsiChar = ''''): AnsiString;
var
  I: Integer;
begin
  Result := S;
  for I := Length(Result) downto 1 do begin
    if Result[I] = Quote then Insert(Quote, Result, I);
  end;
  Result := Quote + Result + Quote;
end;

{ TBerkeleyDBVerifyCacheImplDirect }

constructor TBerkeleyDBVerifyCacheImplDirect.Create(const AFileName: string);
begin
  inherited Create;
  Assert(AFileName <> '');
  FLastCachePath := '';
  FLastCachePathID := 0;
  OpenDB(AFileName);
end;

destructor TBerkeleyDBVerifyCacheImplDirect.Destroy;
begin
  CloseDB;
  inherited Destroy;
end;

procedure TBerkeleyDBVerifyCacheImplDirect.InternalLoadParams;
var
  VList: TStringList;
  VFileName: string;
begin
  VList := TStringList.Create;
  try
    VFileName := 'db_verify.sql';
    if FileExists(VFileName) then begin
      FSQLMakeSession := TStringListStatic.CreateFromFile(VFileName);
    end else begin
      VList.Clear;
      VList.Add('PRAGMA encoding="UTF-8"');
      VList.Add('PRAGMA cache_size=2000');
      VList.Add('PRAGMA main.journal_mode=WAL');
      VList.Add('PRAGMA synchronous=FULL');
      FSQLMakeSession := TStringListStatic.CreateByStrings(VList);
    end;

    VList.Clear;

    VList.Add(
      'CREATE TABLE IF NOT EXISTS ' + cTableCachePath + ' (' +
      'ID   INTEGER PRIMARY KEY AUTOINCREMENT,'+
      'Path TEXT COLLATE SYSTEMNOCASE);'
    );

    VList.Add(
      'CREATE TABLE IF NOT EXISTS ' + cTableFileInfo + ' (' +
      'ID            INTEGER PRIMARY KEY AUTOINCREMENT,' +
      'Path          INTEGER,' +
      'FileType      INTEGER,' +
      'Zoom          INTEGER,' +
      'X             INTEGER,' +
      'Y             INTEGER,' +
      'Size          INTEGER,' +
      'CreationTime  INTEGER,' +
      'LastWriteTime INTEGER,' +
      'VerifyStatus  INTEGER,' +
      'VerifyResult  INTEGER,' +
      'VerifyTime    INTEGER);'
    );

    VList.Add(
      'CREATE INDEX IF NOT EXISTS Index' + cTableFileInfo + 'Path ON ' +
      cTableFileInfo + '(Path);'
    );

    VList.Add(
      'CREATE UNIQUE INDEX IF NOT EXISTS ' +
      'Index' + cTableFileInfo + 'PathFileTypeZoomXY ON ' +
      cTableFileInfo + '(Path,FileType,Zoom,X,Y);'
    );

    FSQLForNewTable := TStringListStatic.CreateByStrings(VList);
  finally
    VList.Free;
  end;
end;

procedure TBerkeleyDBVerifyCacheImplDirect.OpenDB(const AFileName: TFileName);
var
  I: Integer;
  VOpenFlags: Integer;
begin
  if FSQLite3DbHandler.Init then begin
    VOpenFlags := SQLITE_OPEN_READWRITE or SQLITE_OPEN_CREATE;
    FSQLite3DbHandler.Open(AFileName, VOpenFlags, True);

    InternalLoadParams;

    for I := 0 to FSQLMakeSession.Count - 1 do begin
      FSQLite3DbHandler.ExecSQL(AnsiString(FSQLMakeSession.Items[I]));
    end;

    for I := 0 to FSQLForNewTable.Count - 1 do begin
      FSQLite3DbHandler.ExecSQL(AnsiString(FSQLForNewTable.Items[I]));
    end;

    FSQLite3DbHandler.SetBusyTryCount(3);
  end;
end;

procedure TBerkeleyDBVerifyCacheImplDirect.CloseDB;
begin
  if FSQLite3DbHandler.Opened then begin
    FSQLite3DbHandler.Close;
  end;
end;

type
  TSelectCachePathRec = record
    FID: Int64;
  end;
  PSelectCachePathRec = ^TSelectCachePathRec;

procedure TBerkeleyDBVerifyCacheImplDirect.CallbackSelectCachePathID(
  const AHandler: PSQLite3DbHandler;
  const ACallbackPtr: Pointer;
  const AStmtData: PSQLite3StmtData
);
var
  VData: PSelectCachePathRec;
begin
  AStmtData^.Cancelled := True;
  VData := PSelectCachePathRec(ACallbackPtr);
  VData.FID := AStmtData^.ColumnInt64(0);
end;

function TBerkeleyDBVerifyCacheImplDirect.GetCachePathID(
  const ACachePath: string;
  const AForceCreateNew: Boolean
): TRecID;
var
  VSQLText: AnsiString;
  VCachePathUTF8: UTF8String;
  VData: TSelectCachePathRec;
  VRowsAffected: Integer;
begin
  if (FLastCachePath = ACachePath) and ((FLastCachePathID <> 0) or not AForceCreateNew) then begin
    Result := FLastCachePathID;
  end else begin
    VData.FID := 0;
    VCachePathUTF8 := ALQuotedStr(AnsiToUtf8(ACachePath));
    VSQLText := Format(
      'SELECT ID FROM %s WHERE Path=%s LIMIT 1;',
      [cTableCachePath, VCachePathUTF8]
    );
    FSQLite3DbHandler.OpenSQL(
      VSQLText,
      CallbackSelectCachePathID,
      @VData,
      True
    );
    if VData.FID = 0 then begin
      if AForceCreateNew then begin
        VSQLText := Format(
          'INSERT INTO %s (Path) VALUES (%s);',
          [cTableCachePath, VCachePathUTF8]
        );
        VRowsAffected := 0;
        FSQLite3DbHandler.ExecSQL(VSQLText, @VRowsAffected);
        if VRowsAffected > 0 then begin
          VData.FID := FSQLite3DbHandler.LastInsertedRowId;
        end;
      end;
    end;
    Result := VData.FID;
    FLastCachePath := ACachePath;
    FLastCachePathID := Result;
  end;
end;

function TBerkeleyDBVerifyCacheImplDirect.Add(
  const AFileName: string;
  const AStatus: TBerkeleyDBVerifyStatus;
  const AResultCode: Cardinal
): TRecID;
var
  VSize: Int64;
  VCreationTime: Int64;
  VLastWriteTime: Int64;
  VCachePathID: TRecID;
  VFileNameParser: TBerkeleyDBFileNameParser;
  VSQLText: AnsiString;
  VRowsAffected: Integer;
begin
  if not VFileNameParser.DoParse(AFileName) then begin
    raise Exception.CreateFmt('Error parsing file name: %s', [AFileName]);
  end;

  VCachePathID := GetCachePathID(VFileNameParser.FCachePath, True);

  if VCachePathID <= 0 then begin
    raise Exception.CreateFmt('Error retrieving cache path ID: %s', [VFileNameParser.FCachePath]);
  end;

  if not GetFileInfo(AFileName, VSize, VCreationTime, VLastWriteTime) then begin
    raise Exception.CreateFmt('Error GetFileInfo: %s', [AFileName]);
  end;

  with VFileNameParser do begin
    VSQLText := Format(
      'INSERT INTO %s (Path,FileType,Zoom,X,Y,Size,CreationTime,LastWriteTime,' +
      'VerifyStatus,VerifyResult,VerifyTime) VALUES (%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d);',
      [cTableFileInfo, VCachePathID, Integer(FFileType), FZoom, FFileX, FFileY,
      VSize, VCreationTime, VLastWriteTime, Integer(AStatus), AResultCode,
      DateTimeToFileTimeInt64(Now)]
    );
  end;

  VRowsAffected := 0;
  FSQLite3DbHandler.ExecSQL(VSQLText, @VRowsAffected);
  if VRowsAffected > 0 then begin
    Result := FSQLite3DbHandler.LastInsertedRowId;
  end else begin
    // error
    Result := 0;
  end;
end;

type
  TSelectFileInfoRec = record
    FID: Int64;
    FSize: Int64;
    FCreationTime: Int64;
    FLastWriteTime: Int64;
    FVerifyStatus: Int64;
    FVerifyResult: Int64;
    FVerifyTime: Int64;
  end;
  PSelectFileInfoRec = ^TSelectFileInfoRec;

procedure TBerkeleyDBVerifyCacheImplDirect.CallbackSelectFileInfo(
  const AHandler: PSQLite3DbHandler;
  const ACallbackPtr: Pointer;
  const AStmtData: PSQLite3StmtData
);
var
  VData: PSelectFileInfoRec;
begin
  AStmtData^.Cancelled := True;
  VData := PSelectFileInfoRec(ACallbackPtr);
  VData.FID := AStmtData^.ColumnInt64(0);
  VData.FSize := AStmtData^.ColumnInt64(1);
  VData.FCreationTime := AStmtData^.ColumnInt64(2);
  VData.FLastWriteTime := AStmtData^.ColumnInt64(3);
  VData.FVerifyStatus := AStmtData^.ColumnInt64(4);
  VData.FVerifyResult := AStmtData^.ColumnInt64(5);
  VData.FVerifyTime := AStmtData^.ColumnInt64(6);
end;

function TBerkeleyDBVerifyCacheImplDirect.Read(
  const AFileName: string;
  out AIsChanged: Boolean;
  out AFileInfo: TFileInfoRec
): Boolean;
var
  VCachePathID: TRecID;
  VFileNameParser: TBerkeleyDBFileNameParser;
  VSQLText: AnsiString;
  VData: TSelectFileInfoRec;
begin
  if not VFileNameParser.DoParse(AFileName) then begin
    raise Exception.CreateFmt('Error parsing file name: %s', [AFileName]);
  end;

  VCachePathID := GetCachePathID(VFileNameParser.FCachePath, False);
  if VCachePathID <= 0 then begin
    Result := False;
    Exit;
  end;

  with VFileNameParser do begin
    VSQLText := Format(
      'SELECT ' +
      'ID,Size,CreationTime,LastWriteTime,VerifyStatus,VerifyResult,VerifyTime' +
      ' FROM %s WHERE ' +
      'Path=%d AND FileType=%d AND Zoom=%d AND X=%d AND Y=%d LIMIT 1;',
      [cTableFileInfo, VCachePathID, Integer(FFileType), FZoom, FFileX, FFileY]
    );
  end;

  VData.FID := 0;
  FSQLite3DbHandler.OpenSQL(
    VSQLText,
    CallbackSelectFileInfo,
    @VData,
    True
  );

  Result := VData.FID <> 0;

  if Result then begin
    with AFileInfo do begin
      FID := VData.FID;
      FSize := VData.FSize;
      FCreationTime := FileTime64ToDateTime(VData.FCreationTime);
      FLastWriteTime := FileTime64ToDateTime(VData.FLastWriteTime);
      FVerifyStatus := TBerkeleyDBVerifyStatus(VData.FVerifyStatus);
      FVerifyResult := VData.FVerifyResult;
      FVerifyTime := FileTime64ToDateTime(VData.FVerifyTime);
    end;

    AIsChanged := not CheckIfSameFile(
      AFileName,
      VData.FSize,
      VData.FCreationTime,
      VData.FLastWriteTime
    );
  end;
end;

function TBerkeleyDBVerifyCacheImplDirect.Update(
  const AID: TRecID;
  const AStatus: TBerkeleyDBVerifyStatus;
  const AResultCode: Cardinal
): Boolean;
var
  VSQLText: AnsiString;
  VRowsAffected: Integer;
begin
  VSQLText := Format(
    'UPDATE %s SET VerifyStatus=%d,VerifyResult=%d,VerifyTime=%d WHERE ID=%d',
    [cTableFileInfo, Integer(AStatus), AResultCode, DateTimeToFileTimeInt64(Now), AID]
  );
  VRowsAffected := 0;
  FSQLite3DbHandler.ExecSQL(VSQLText, @VRowsAffected);
  Result := (VRowsAffected > 0);
end;

function TBerkeleyDBVerifyCacheImplDirect.UpdateFull(
  const AFileName: string;
  const AID: TRecID;
  const AStatus: TBerkeleyDBVerifyStatus;
  const AResultCode: Cardinal
): Boolean;
var
  VSize: Int64;
  VCreationTime: Int64;
  VLastWriteTime: Int64;
  VSQLText: AnsiString;
  VRowsAffected: Integer;
begin
  if not GetFileInfo(AFileName, VSize, VCreationTime, VLastWriteTime) then begin
    raise Exception.CreateFmt('Error GetFileInfo: %s', [AFileName]);
  end;

  VSQLText := Format(
    'UPDATE %s SET Size=%d,CreationTime=%d,LastWriteTime=%d,VerifyStatus=%d,' +
    'VerifyResult=%d,VerifyTime=%d WHERE ID=%d;',
    [cTableFileInfo, VSize, VCreationTime, VLastWriteTime, Integer(AStatus),
    AResultCode, DateTimeToFileTimeInt64(Now), AID]
  );

  VRowsAffected := 0;
  FSQLite3DbHandler.ExecSQL(VSQLText, @VRowsAffected);
  Result := (VRowsAffected > 0);
end;

function TBerkeleyDBVerifyCacheImplDirect.Delete(const AID: TRecID): Boolean;
var
  VSQLText: AnsiString;
  VRowsAffected: Integer;
begin
  VSQLText := Format('DELETE FROM %s WHERE ID=%d', [cTableFileInfo, AID]);
  VRowsAffected := 0;
  FSQLite3DbHandler.ExecSQL(VSQLText, @VRowsAffected);
  Result := (VRowsAffected > 0);
end;

function TBerkeleyDBVerifyCacheImplDirect.GetIsAvailable: Boolean;
begin
  Result := FSQLite3DbHandler.Opened;
end;

end.
