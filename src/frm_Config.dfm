object frmConfig: TfrmConfig
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Config'
  ClientHeight = 362
  ClientWidth = 527
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsStayOnTop
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnCancel: TButton
    Left = 439
    Top = 329
    Width = 80
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    TabOrder = 0
    OnClick = btnCancelClick
    ExplicitTop = 307
  end
  object btnApply: TButton
    Left = 353
    Top = 329
    Width = 80
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = 'Apply'
    TabOrder = 1
    OnClick = btnApplyClick
    ExplicitTop = 307
  end
  object grpRecover: TGroupBox
    Left = 8
    Top = 8
    Width = 249
    Height = 41
    Align = alCustom
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Recover [db_recover]'
    TabOrder = 2
    object chkCatastr: TCheckBox
      Left = 10
      Top = 16
      Width = 223
      Height = 17
      Caption = 'Catastrophic recovery (use -c switch)'
      TabOrder = 0
    end
  end
  object grpResetLSN: TGroupBox
    Left = 8
    Top = 55
    Width = 249
    Height = 42
    Align = alCustom
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Reset LSN [db_load]'
    TabOrder = 3
    object chkRemoveEnv: TCheckBox
      Left = 10
      Top = 16
      Width = 223
      Height = 17
      Caption = 'Delete env folder on finish'
      Checked = True
      State = cbChecked
      TabOrder = 0
    end
  end
  object rgVerify: TRadioGroup
    Left = 8
    Top = 103
    Width = 249
    Height = 98
    Align = alCustom
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Verify [db_verify]'
    ItemIndex = 0
    Items.Strings = (
      'Only report'
      'Rename broken files to *.bad'
      'Delete broken files'
      'Restore broken files (take a lot of time)')
    TabOrder = 4
  end
  object rgRestore: TRadioGroup
    Left = 8
    Top = 207
    Width = 249
    Height = 77
    Align = alCustom
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Restore [db_dump]'
    ItemIndex = 0
    Items.Strings = (
      'Default (no any specific switches)'
      'Salvage data (use -r switch)'
      'Aggressively salvage data (use -R switch)')
    TabOrder = 5
  end
  object grpDBCOFIG: TGroupBox
    Left = 262
    Top = 8
    Width = 263
    Height = 305
    Caption = 'DB_CONFIG (on db_recover call)'
    TabOrder = 6
    object lblPresets: TLabel
      Left = 10
      Top = 23
      Width = 40
      Height = 13
      Caption = 'Presets:'
    end
    object mmoDbConf: TMemo
      Left = 3
      Top = 47
      Width = 257
      Height = 229
      Lines.Strings = (
        '')
      ScrollBars = ssVertical
      TabOrder = 0
    end
    object chkOverwriteifExists: TCheckBox
      Left = 7
      Top = 282
      Width = 253
      Height = 17
      Caption = 'Overwrite if exists'
      Checked = True
      State = cbChecked
      TabOrder = 1
    end
    object cbbDbConfPreset: TComboBox
      Left = 56
      Top = 20
      Width = 204
      Height = 21
      ItemHeight = 13
      ItemIndex = 2
      TabOrder = 2
      Text = 'High Durability'
      OnChange = cbbDbConfPresetChange
      Items.Strings = (
        'Low Durability'
        'Normal Durability'
        'High Durability'
        'Extremely High Durability'
        'User Defined')
    end
  end
  object chkRemoveBadFileOnSuccess: TCheckBox
    Left = 18
    Top = 336
    Width = 238
    Height = 17
    Caption = 'Remove *.bad files on successful restore'
    TabOrder = 7
  end
  object chkUseDumpCleaner: TCheckBox
    Left = 18
    Top = 290
    Width = 238
    Height = 17
    Caption = 'Use dump cleaner [sdb_dump_cleaner]'
    TabOrder = 8
  end
  object chkMinDiskUsage: TCheckBox
    Left = 18
    Top = 313
    Width = 238
    Height = 17
    Caption = 'Minimize disk usage on restore *.bad files'
    TabOrder = 9
  end
end
