object frmMain: TfrmMain
  Left = 0
  Top = 0
  Caption = 'sdb_util'
  ClientHeight = 448
  ClientWidth = 695
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblPath: TLabel
    Left = 8
    Top = 16
    Width = 85
    Height = 13
    Caption = 'Root Cache Path:'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object mmoCmd: TMemo
    Left = 0
    Top = 67
    Width = 695
    Height = 325
    Align = alCustom
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvRaised
    BorderStyle = bsNone
    Color = clBlack
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clSilver
    Font.Height = -13
    Font.Name = 'Verdana'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    ScrollBars = ssBoth
    TabOrder = 0
  end
  object btnRun: TButton
    Left = 521
    Top = 398
    Width = 80
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = 'Run'
    TabOrder = 1
    OnClick = btnRunClick
  end
  object edtCmdStr: TEdit
    Left = 99
    Top = 13
    Width = 555
    Height = 21
    Align = alCustom
    Anchors = [akLeft, akTop, akRight]
    TabOrder = 2
    OnChange = edtCmdStrChange
  end
  object cbbUtils: TComboBox
    Left = 8
    Top = 402
    Width = 345
    Height = 21
    Align = alCustom
    Anchors = [akLeft, akBottom]
    ItemHeight = 13
    ItemIndex = 0
    TabOrder = 3
    Text = 'Restore cache after crash: Recover & Verify (default)'
    OnChange = cbbUtilsChange
    Items.Strings = (
      'Restore cache after crash: Recover & Verify (default)'
      'Recover environment [cmd: db_recover -v]'
      'Prepare cache for backup (reset LSN) [cmd: db_load -r lsn]'
      'Verify cache (find broken files) [cmd: db_verify]'
      'Restore broken files from *.bad [cmd: db_dump && db_load]')
  end
  object btnExit: TButton
    Left = 607
    Top = 398
    Width = 80
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = 'Exit'
    TabOrder = 4
    OnClick = btnExitClick
  end
  object btnOpenFolder: TButton
    Left = 660
    Top = 13
    Width = 27
    Height = 21
    Align = alCustom
    Anchors = [akTop, akRight]
    Caption = '...'
    TabOrder = 5
    OnClick = btnOpenFolderClick
  end
  object statBottom: TStatusBar
    Left = 0
    Top = 429
    Width = 695
    Height = 19
    Panels = <
      item
        Width = 180
      end
      item
        Width = 250
      end
      item
        Width = 50
      end>
  end
  object btnConfig: TButton
    Left = 359
    Top = 402
    Width = 66
    Height = 21
    Align = alCustom
    Anchors = [akLeft, akBottom]
    Caption = 'Config'
    TabOrder = 7
    OnClick = btnConfigClick
  end
  object chkWithSubFolders: TCheckBox
    Left = 8
    Top = 40
    Width = 129
    Height = 17
    Caption = 'Process Subfolders '
    Checked = True
    State = cbChecked
    TabOrder = 8
    OnClick = chkWithSubFoldersClick
  end
  object btnClearLog: TButton
    Left = 591
    Top = 40
    Width = 96
    Height = 21
    Align = alCustom
    Anchors = [akTop, akRight]
    Caption = 'Clear Log'
    TabOrder = 9
    OnClick = btnClearLogClick
  end
  object btnConfigLog: TButton
    Left = 489
    Top = 40
    Width = 96
    Height = 21
    Align = alCustom
    Anchors = [akTop, akRight]
    Caption = 'Logging Config'
    TabOrder = 10
    OnClick = btnConfigLogClick
  end
end
