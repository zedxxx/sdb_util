unit frm_Config;

interface

uses
  Classes,
  Controls,
  Forms,
  Spin,
  StdCtrls,
  ExtCtrls,
  i_BerkeleyUtilsConfig;

type
  TfrmConfig = class(TForm)
    btnCancel: TButton;
    btnApply: TButton;
    grpRecover: TGroupBox;
    chkCatastr: TCheckBox;
    grpResetLSN: TGroupBox;
    chkRemoveEnv: TCheckBox;
    rgVerify: TRadioGroup;
    rgRestore: TRadioGroup;
    grpDBCOFIG: TGroupBox;
    mmoDbConf: TMemo;
    chkOverwriteifExists: TCheckBox;
    cbbDbConfPreset: TComboBox;
    lblPresets: TLabel;
    chkRemoveBadFileOnSuccess: TCheckBox;
    chkUseDumpCleaner: TCheckBox;
    chkMinDiskUsage: TCheckBox;
    procedure FormShow(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure cbbDbConfPresetChange(Sender: TObject);
  private
    FBerkeleyUtilsConfig: IBerkeleyUtilsConfig;
  public
    constructor Create(
      AOwner: TComponent;
      const ABerkeleyUtilsConfig: IBerkeleyUtilsConfig
    ); reintroduce;
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils,
  c_BerkeleyUtils,
  i_DbConfigPresetList;

{$R *.dfm}

constructor TfrmConfig.Create(
  AOwner: TComponent;
  const ABerkeleyUtilsConfig: IBerkeleyUtilsConfig
);
begin
  inherited Create(AOwner);
  FBerkeleyUtilsConfig := ABerkeleyUtilsConfig;
end;

destructor TfrmConfig.Destroy;
begin
  FBerkeleyUtilsConfig := nil;
  inherited Destroy;
end;

procedure TfrmConfig.FormShow(Sender: TObject);
begin
  chkCatastr.Checked := FBerkeleyUtilsConfig.CatastrophicRecover;
  chkRemoveEnv.Checked := FBerkeleyUtilsConfig.RemoveEnvOnLSNReset;
  rgRestore.ItemIndex := Integer(FBerkeleyUtilsConfig.DumpMode);
  rgVerify.ItemIndex := Integer(FBerkeleyUtilsConfig.VerifyMode);
  chkOverwriteifExists.Checked := FBerkeleyUtilsConfig.OverwriteDbConfig;
  chkRemoveBadFileOnSuccess.Checked := FBerkeleyUtilsConfig.RemoveBadFileOnSuccessRestore;
  cbbDbConfPreset.ItemIndex := Integer(FBerkeleyUtilsConfig.DbConfigPresetList.Preset);
  cbbDbConfPresetChange(Self);
  chkUseDumpCleaner.Enabled := FileExists(sdb_dump_cleaner_exe);
  chkUseDumpCleaner.Checked := FBerkeleyUtilsConfig.UseDumpCleaner;
  chkMinDiskUsage.Checked := FBerkeleyUtilsConfig.UsePipelineOnRestore;
end;

procedure TfrmConfig.btnApplyClick(Sender: TObject);
var
  VPreset: TDbConfigPresetType;
begin
  FBerkeleyUtilsConfig.CatastrophicRecover := chkCatastr.Checked;
  FBerkeleyUtilsConfig.RemoveEnvOnLSNReset := chkRemoveEnv.Checked;
  FBerkeleyUtilsConfig.DumpMode := TDumpMode(rgRestore.ItemIndex);
  FBerkeleyUtilsConfig.VerifyMode := TVerifyMode(rgVerify.ItemIndex);
  FBerkeleyUtilsConfig.OverwriteDbConfig := chkOverwriteifExists.Checked;
  FBerkeleyUtilsConfig.RemoveBadFileOnSuccessRestore := chkRemoveBadFileOnSuccess.Checked;
  FBerkeleyUtilsConfig.UsePipelineOnRestore := chkMinDiskUsage.Checked;

  VPreset := TDbConfigPresetType(cbbDbConfPreset.ItemIndex);
  FBerkeleyUtilsConfig.DbConfigPresetList.Preset := VPreset;
  if VPreset = dbcpUserDefined then begin
    FBerkeleyUtilsConfig.DbConfigPresetList.Item[VPreset] := mmoDbConf.Lines.Text;
  end;

  if chkUseDumpCleaner.Enabled then begin
    FBerkeleyUtilsConfig.UseDumpCleaner := chkUseDumpCleaner.Checked;
  end;

  Close;
end;

procedure TfrmConfig.btnCancelClick(Sender: TObject);
begin
  Close;
end; 

procedure TfrmConfig.cbbDbConfPresetChange(Sender: TObject);
var
  VPreset: TDbConfigPresetType;
begin
  VPreset := TDbConfigPresetType(cbbDbConfPreset.ItemIndex);
  mmoDbConf.Lines.Text := FBerkeleyUtilsConfig.DbConfigPresetList.Item[VPreset];
  mmoDbConf.ReadOnly := not (VPreset = dbcpUserDefined);
end;

end.
