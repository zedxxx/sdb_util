unit i_FormConfig;

interface

uses
  Types;

type
  IFormConfig = interface
    ['{CD84DDC1-61BA-40CD-A3C1-40CB5D3B2EE1}']
    function GetBoundsRect: TRect;
    procedure SetWindowPosition(const ARect: TRect);

    function GetConsoleColor: Integer;
    procedure SetConsoleColor(const AColor: Integer);

    function GetConsoleFontColor: Integer;
    procedure SetConsoleFontColor(const AColor: Integer);
  end;

implementation

end.
