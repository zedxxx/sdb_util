unit u_BerkeleyDBVerifyCacheModel;

interface

uses
  mORMot,
  SynCommons,
  t_BerkeleyDB;

type
  TSQLCachePath = class(TSQLRecord)
  public
    FPath: RawUTF8;
  published
    property Path: RawUTF8 read FPath write FPath;
  end;

  TSQLFileInfo = class(TSQLRecord)
  public
    FPath: TID;
    FType: TBerkeleyDBFileType;
    FZoom: Byte;
    FFileX: Int64;
    FFileY: Int64;
    FSize: Int64;
    FCreationTime: Int64;
    FLastWriteTime: Int64;
    FVerifyStatus: TBerkeleyDBVerifyStatus;
    FVerifyResult: Cardinal;
    FVerifyTime: Int64;
  published
    property Path: TID read FPath write FPath;
    property FileType: TBerkeleyDBFileType read FType write FType;
    property Zoom: Byte read FZoom write FZoom;
    property X: Int64 read FFileX write FFileX;
    property Y: Int64 read FFileY write FFileY;
    property Size: Int64 read FSize write FSize;
    property CreationTime: Int64 read FCreationTime write FCreationTime;
    property LastWriteTime: Int64 read FLastWriteTime write FLastWriteTime;
    property VerifyStatus: TBerkeleyDBVerifyStatus read FVerifyStatus write FVerifyStatus;
    property VerifyResult: Cardinal read FVerifyResult write FVerifyResult;
    property VerifyTime: Int64 read FVerifyTime write FVerifyTime;
  end;

function CreateVerifyCacheModel: TSQLModel;

implementation

function CreateVerifyCacheModel: TSQLModel;
begin
  Result := TSQLModel.Create([TSQLCachePath, TSQLFileInfo]);
end;

end.
