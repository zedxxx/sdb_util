unit u_FormInfoConfig;

interface

uses
  Types,
  i_FormInfoConfig,
  i_ConfigDataProvider,
  i_ConfigDataWriteProvider;

type
  TFormInfoConfig = class(TInterfacedObject, IFormInfoConfig)
  private
    FConfigData: IConfigDataWriteProvider;
    FBoundsRect: TRect;
    FTreeColors: TColorsRec;
    FTreeColumnsState: TTreeColumnsState;
    FTreeShowOpt: TTreeShowOptRec;
    FCachePath: string;
  private
    procedure DoReadConfig(const AConfigData: IConfigDataProvider);
    procedure DoWriteConfig(const AConfigData: IConfigDataWriteProvider);
  private
    { IFormInfoConfig }
    function GetBoundsRect: TRect;
    procedure SetWindowPosition(const ARect: TRect);

    function GetTreeColors: TColorsRec;
    procedure SetTreeColors(const AColors: TColorsRec);

    function GetTreeColumnsState: TTreeColumnsState;
    procedure SetTreeColumnsState(const AValue: TTreeColumnsState);

    function GetTreeShowOpt: TTreeShowOptRec;
    procedure SetTreeShowOpt(const AValue: TTreeShowOptRec);

    function GetCachePath: string;
    procedure SetCachePath(const AValue: string);
  public
    constructor Create(
      const AConfigData: IConfigDataWriteProvider;
      const ABoundsRect: TRect;
      const ATreeColors: TColorsRec;
      const ATreeColumnsState: TTreeColumnsState;
      const ATreeShowOpt: TTreeShowOptRec;
      const ACachePath: string
    );
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils;

{ TFormInfoConfig }

constructor TFormInfoConfig.Create(
  const AConfigData: IConfigDataWriteProvider;
  const ABoundsRect: TRect;
  const ATreeColors: TColorsRec;
  const ATreeColumnsState: TTreeColumnsState;
  const ATreeShowOpt: TTreeShowOptRec;
  const ACachePath: string
);
begin
  inherited Create;
  FConfigData := AConfigData;
  FBoundsRect := ABoundsRect;
  FTreeColors := ATreeColors;
  FTreeColumnsState := ATreeColumnsState;
  FTreeShowOpt := ATreeShowOpt;
  FCachePath := ACachePath;
  DoReadConfig(FConfigData);
end;

destructor TFormInfoConfig.Destroy;
begin
  DoWriteConfig(FConfigData);
  inherited Destroy;
end;

procedure TFormInfoConfig.DoReadConfig(const AConfigData: IConfigDataProvider);
var
  I: Integer;
begin
  if AConfigData <> nil then begin
    FBoundsRect := Bounds(
      AConfigData.ReadInteger('Left', FBoundsRect.Left),
      AConfigData.ReadInteger('Top', FBoundsRect.Top),
      AConfigData.ReadInteger('Width', FBoundsRect.Right - FBoundsRect.Top),
      AConfigData.ReadInteger('Height', FBoundsRect.Bottom - FBoundsRect.Top)
    );
    FTreeColors.ColorForOK := StrToInt(
      AConfigData.ReadString('ColorForOK', '$' + IntToHex(FTreeColors.ColorForOK, 8))
    );
    FTreeColors.ColorForErr := StrToInt(
      AConfigData.ReadString('ColorForErr', '$' + IntToHex(FTreeColors.ColorForErr, 8))
    );
    FTreeColors.ColorForChanged := StrToInt(
      AConfigData.ReadString('ColorForChanged', '$' + IntToHex(FTreeColors.ColorForChanged, 8))
    );
    FTreeColors.ColorForUnk := StrToInt(
      AConfigData.ReadString('ColorForUnk', '$' + IntToHex(FTreeColors.ColorForUnk, 8))
    );
    for I := 0 to Length(FTreeColumnsState.State) - 1 do begin
      FTreeColumnsState.State[I].Size :=
        AConfigData.ReadInteger('ColSize_' + IntToStr(I), FTreeColumnsState.State[I].Size);
      FTreeColumnsState.State[I].Position :=
        AConfigData.ReadInteger('ColPosition_' + IntToStr(I), FTreeColumnsState.State[I].Position);
    end;
    FCachePath := AConfigData.ReadString('CachePath', FCachePath);
    FTreeShowOpt.ShowWithOkStatus := AConfigData.ReadBool('ShowWithOkStatus', FTreeShowOpt.ShowWithOkStatus);
    FTreeShowOpt.ShowWithUnkStatus := AConfigData.ReadBool('ShowWithUnkStatus', FTreeShowOpt.ShowWithUnkStatus);
    FTreeShowOpt.SortColumn := AConfigData.ReadInteger('SortColumn', FTreeShowOpt.SortColumn);
    FTreeShowOpt.SortDirection := AConfigData.ReadInteger('SortDirection', FTreeShowOpt.SortDirection);
  end;
end;

procedure TFormInfoConfig.DoWriteConfig(const AConfigData: IConfigDataWriteProvider);
var
  I: Integer;
begin
  if AConfigData <> nil then begin
    AConfigData.WriteInteger('Left', FBoundsRect.Left);
    AConfigData.WriteInteger('Top', FBoundsRect.Top);
    AConfigData.WriteInteger('Width', FBoundsRect.Right - FBoundsRect.Left);
    AConfigData.WriteInteger('Height', FBoundsRect.Bottom - FBoundsRect.Top);

    AConfigData.WriteString('ColorForOK', '$' + IntToHex(FTreeColors.ColorForOK, 8));
    AConfigData.WriteString('ColorForErr', '$' + IntToHex(FTreeColors.ColorForErr, 8));
    AConfigData.WriteString('ColorForChanged', '$' + IntToHex(FTreeColors.ColorForChanged, 8));
    AConfigData.WriteString('ColorForUnk', '$' + IntToHex(FTreeColors.ColorForUnk, 8));

    for I := 0 to Length(FTreeColumnsState.State) - 1 do begin
      AConfigData.WriteInteger('ColSize_' + IntToStr(I), FTreeColumnsState.State[I].Size);
      AConfigData.WriteInteger('ColPosition_' + IntToStr(I), FTreeColumnsState.State[I].Position);
    end;

    AConfigData.WriteString('CachePath', FCachePath);
    AConfigData.WriteBool('ShowWithOkStatus', FTreeShowOpt.ShowWithOkStatus);
    AConfigData.WriteBool('ShowWithUnkStatus', FTreeShowOpt.ShowWithUnkStatus);
    AConfigData.WriteInteger('SortColumn', FTreeShowOpt.SortColumn);
    AConfigData.WriteInteger('SortDirection', FTreeShowOpt.SortDirection);
  end;
end;

function TFormInfoConfig.GetBoundsRect: TRect;
begin
  Result := FBoundsRect;
end;

function TFormInfoConfig.GetCachePath: string;
begin
  Result := FCachePath;
end;

function TFormInfoConfig.GetTreeColors: TColorsRec;
begin
  Result := FTreeColors;
end;

function TFormInfoConfig.GetTreeColumnsState: TTreeColumnsState;
begin
  Result := FTreeColumnsState;
end;

function TFormInfoConfig.GetTreeShowOpt: TTreeShowOptRec;
begin
  Result := FTreeShowOpt;
end;

procedure TFormInfoConfig.SetCachePath(const AValue: string);
begin
  FCachePath := AValue;
end;

procedure TFormInfoConfig.SetTreeColors(const AColors: TColorsRec);
begin
  FTreeColors := AColors;
end;

procedure TFormInfoConfig.SetTreeColumnsState(const AValue: TTreeColumnsState);
begin
  FTreeColumnsState := AValue;
end;

procedure TFormInfoConfig.SetTreeShowOpt(const AValue: TTreeShowOptRec);
begin
  FTreeShowOpt := AValue;
end;

procedure TFormInfoConfig.SetWindowPosition(const ARect: TRect);
begin
  FBoundsRect := ARect;
end;

end.
