unit u_BerkeleyDBVerifyCacheImplORM;

interface

uses
  Windows,
  SysUtils,
  mORMot,
  mORMotSQLite3,
  SynSQLite3,
  SynSQLite3Static,
  SynCommons,
  t_BerkeleyDB,
  i_BerkeleyDBVerifyCache,
  u_BerkeleyDBVerifyCacheModel;

type
  TBerkeleyDBVerifyCacheImplORM = class(TInterfacedObject, IBerkeleyDBVerifyCache)
  private
    FModel: TSQLModel;
    FClientDB: TSQLRestClientDB;
    FLastCachePath: string;
    FLastCachePathID: TRecID;
    procedure OpenDB(const AFileName: TFileName);
    procedure CloseDB;
    function GetCachePathID(
      const ACachePath: string;
      const AForceCreateNew: Boolean
    ): TRecID;
  private
    function Add(
      const AFileName: string;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): TRecID;

    function Delete(
      const AID: TRecID
    ): Boolean;

    function Read(
      const AFileName: string;
      out AIsChanged: Boolean;
      out AFileInfo: TFileInfoRec
    ): Boolean;

    function Update(
      const AID: TRecID;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): Boolean;

    function UpdateFull(
      const AFileName: string;
      const AID: TRecID;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): Boolean;

    function GetIsAvailable: Boolean;
  public
    constructor Create(const AFileName: string);
    destructor Destroy; override;
  end;

implementation

uses
  u_Tools,
  u_BerkeleyDBTools;

{ TBerkeleyDBVerifyCacheImplORM }

constructor TBerkeleyDBVerifyCacheImplORM.Create(const AFileName: string);
begin
  inherited Create;
  Assert(AFileName <> '');
  FLastCachePath := '';
  FLastCachePathID := 0;
  OpenDB(AFileName);
end;

destructor TBerkeleyDBVerifyCacheImplORM.Destroy;
begin
  CloseDB;
  inherited Destroy;
end;

procedure TBerkeleyDBVerifyCacheImplORM.OpenDB(const AFileName: TFileName);
begin
  FModel := CreateVerifyCacheModel;

  FClientDB := TSQLRestClientDB.Create(FModel, nil, AFileName, TSQLRestServerDB);

  FClientDB.DB.WALMode := True;
  FClientDB.DB.LockingMode := lmNormal;
  FClientDB.DB.Synchronous := smFull;

  FClientDB.Server.CreateMissingTables;

  FClientDB.Server.CreateSQLMultiIndex(
    TSQLFileInfo,
    ['Path', 'FileType', 'Zoom', 'X', 'Y'],
    True // Unique
  );
end;

procedure TBerkeleyDBVerifyCacheImplORM.CloseDB;
begin
  if Assigned(FClientDB) then begin
    FreeAndNil(FClientDB);
  end;
  if Assigned(FModel) then begin
    FreeAndNil(FModel);
  end;
end;

function TBerkeleyDBVerifyCacheImplORM.GetCachePathID(
  const ACachePath: string;
  const AForceCreateNew: Boolean
): TRecID;
var
  VCachePath: TSQLCachePath;
  VCachePathUTF8: RawUTF8;
begin
  Result := 0;
  if (FLastCachePath = ACachePath) and ((FLastCachePathID <> 0) or not AForceCreateNew) then begin
    Result := FLastCachePathID;
  end else begin
    VCachePathUTF8 := StringToUTF8(ACachePath);
    VCachePath := TSQLCachePath.Create(FClientDB, 'Path=?', [VCachePathUTF8]);
    try
      if VCachePath.ID = 0 then begin
        // not found
        if AForceCreateNew then begin
          VCachePath.FPath := VCachePathUTF8;
          Result := FClientDB.Add(VCachePath, True);
        end;
      end else begin
        Result := VCachePath.ID;
      end;
    finally
      VCachePath.Free;
    end;
    FLastCachePath := ACachePath;
    FLastCachePathID := Result;
  end;
end;

function TBerkeleyDBVerifyCacheImplORM.Add(
  const AFileName: string;
  const AStatus: TBerkeleyDBVerifyStatus;
  const AResultCode: Cardinal
): TRecID;
var
  VSize: Int64;
  VCreationTime: Int64;
  VLastWriteTime: Int64;
  VCachePathID: TID;
  VFileNameParser: TBerkeleyDBFileNameParser;
  VSQLFileInfo: TSQLFileInfo;
begin
  if not VFileNameParser.DoParse(AFileName) then begin
    raise Exception.CreateFmt('Error parsing file name: %s', [AFileName]);
  end;

  VCachePathID := GetCachePathID(VFileNameParser.FCachePath, True);

  if VCachePathID <= 0 then begin
    raise Exception.CreateFmt('Error retrieving cache path ID: %s', [VFileNameParser.FCachePath]);
  end;

  if not GetFileInfo(AFileName, VSize, VCreationTime, VLastWriteTime) then begin
    raise Exception.CreateFmt('Error GetFileInfo: %s', [AFileName]);
  end;

  VSQLFileInfo := TSQLFileInfo.Create;
  try
    VSQLFileInfo.FPath := VCachePathID;
    VSQLFileInfo.FType := VFileNameParser.FFileType;
    VSQLFileInfo.FZoom := VFileNameParser.FZoom;
    VSQLFileInfo.FFileX := VFileNameParser.FFileX;
    VSQLFileInfo.FFileY := VFileNameParser.FFileY;
    VSQLFileInfo.FSize := VSize;
    VSQLFileInfo.FCreationTime := VCreationTime;
    VSQLFileInfo.FLastWriteTime := VLastWriteTime;
    VSQLFileInfo.FVerifyStatus := AStatus;
    VSQLFileInfo.FVerifyResult := AResultCode;
    VSQLFileInfo.FVerifyTime := DateTimeToFileTimeInt64(Now);

    Result := FClientDB.Add(VSQLFileInfo, True);
  finally
    VSQLFileInfo.Free;
  end;
end;

function TBerkeleyDBVerifyCacheImplORM.Read(
  const AFileName: string;
  out AIsChanged: Boolean;
  out AFileInfo: TFileInfoRec
): Boolean;
var
  VCachePathID: TID;
  VFileNameParser: TBerkeleyDBFileNameParser;
  VSQLWhere: RawUTF8;
  VFieldsCSV: RawUTF8;
  VSQLFileInfo: TSQLFileInfo;
begin
  if not VFileNameParser.DoParse(AFileName) then begin
    raise Exception.CreateFmt('Error parsing file name: %s', [AFileName]);
  end;

  VCachePathID := GetCachePathID(VFileNameParser.FCachePath, False);
  if VCachePathID <= 0 then begin
    Result := False;
    Exit;
  end;

  with VFileNameParser do begin
    VSQLWhere := FormatUTF8(
      'Path=? AND FileType=? AND Zoom=? AND X=? AND Y=?', [],
      [VCachePathID, Integer(FFileType), FZoom, FFileX, FFileY]
    );
  end;

  VFieldsCSV := 'RowID,Size,CreationTime,LastWriteTime,VerifyStatus,VerifyResult,VerifyTime';

  VSQLFileInfo := TSQLFileInfo.Create;
  try
    Result := FClientDB.Retrieve(VSQLWhere, VSQLFileInfo, VFieldsCSV);
    if Result then begin
      with AFileInfo do begin
        FID := VSQLFileInfo.ID;
        FSize := VSQLFileInfo.FSize;
        FCreationTime := FileTime64ToDateTime(VSQLFileInfo.FCreationTime);
        FLastWriteTime := FileTime64ToDateTime(VSQLFileInfo.FLastWriteTime);
        FVerifyStatus := VSQLFileInfo.FVerifyStatus;
        FVerifyResult := VSQLFileInfo.FVerifyResult;
        FVerifyTime := FileTime64ToDateTime(VSQLFileInfo.FVerifyTime);
      end;

      AIsChanged := not CheckIfSameFile(
        AFileName,
        VSQLFileInfo.FSize,
        VSQLFileInfo.FCreationTime,
        VSQLFileInfo.FLastWriteTime
      );
    end;
  finally
    VSQLFileInfo.Free;
  end;
end;

function TBerkeleyDBVerifyCacheImplORM.Update(
  const AID: TRecID;
  const AStatus: TBerkeleyDBVerifyStatus;
  const AResultCode: Cardinal
): Boolean;
var
  VSQLFileInfo: TSQLFileInfo;
begin
  VSQLFileInfo := TSQLFileInfo.Create;
  try
    VSQLFileInfo.IDValue := AID;
    VSQLFileInfo.FVerifyStatus := AStatus;
    VSQLFileInfo.FVerifyResult := AResultCode;
    VSQLFileInfo.FVerifyTime := DateTimeToFileTimeInt64(Now);
    Result := FClientDB.Update(VSQLFileInfo, 'VerifyStatus,VerifyResult,VerifyTime');
  finally
    VSQLFileInfo.Free;
  end;
end;

function TBerkeleyDBVerifyCacheImplORM.UpdateFull(
  const AFileName: string;
  const AID: TRecID;
  const AStatus: TBerkeleyDBVerifyStatus;
  const AResultCode: Cardinal
): Boolean;
var
  VSize: Int64;
  VCreationTime: Int64;
  VLastWriteTime: Int64;
  VSQLFileInfo: TSQLFileInfo;
begin
  if not GetFileInfo(AFileName, VSize, VCreationTime, VLastWriteTime) then begin
    raise Exception.CreateFmt('Error GetFileInfo: %s', [AFileName]);
  end;

  VSQLFileInfo := TSQLFileInfo.Create;
  try
    VSQLFileInfo.IDValue := AID;
    VSQLFileInfo.FSize := VSize;
    VSQLFileInfo.FCreationTime := VCreationTime;
    VSQLFileInfo.FLastWriteTime := VLastWriteTime;
    VSQLFileInfo.FVerifyStatus := AStatus;
    VSQLFileInfo.FVerifyResult := AResultCode;
    VSQLFileInfo.FVerifyTime := DateTimeToFileTimeInt64(Now);

    Result := FClientDB.Update(
      VSQLFileInfo,
      'Size,CreationTime,LastWriteTime,VerifyStatus,VerifyResult,VerifyTime'
    );
  finally
    VSQLFileInfo.Free;
  end;
end;

function TBerkeleyDBVerifyCacheImplORM.Delete(const AID: TRecID): Boolean;
begin
  Result := FClientDB.Delete(TSQLFileInfo, AID);
end;

function TBerkeleyDBVerifyCacheImplORM.GetIsAvailable: Boolean;
begin
  Result := True;
end;

end.
