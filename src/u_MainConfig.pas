unit u_MainConfig;

interface

uses
  Types,
  i_MainConfig,
  i_ConfigDataProvider,
  i_ConfigDataWriteProvider;

type
  TMainConfig = class(TInterfacedObject, IMainConfig)
  private
    FConfigData: IConfigDataWriteProvider;
    FTruncateLogFile: Boolean;
    FLogFileMode: TLogFileMode;
    FInMemoryLogSize: Integer;
    FLimitInMemoryLogSize: Boolean;
  private
    procedure DoReadConfig(const AConfigData: IConfigDataProvider);
    procedure DoWriteConfig(const AConfigData: IConfigDataWriteProvider);
    { IMainConfig }
    function GetTruncateLogFile: Boolean;
    procedure SetTruncateLogFile(const AValue: Boolean);

    function GetLogFileMode: TLogFileMode;
    procedure SetLogFileMode(const AValue: TLogFileMode);

    function GetInMemoryLogSize: Integer;
    procedure SetInMemoryLogSize(const AValue: Integer);

    function GetLimitInMemoryLogSize: Boolean;
    procedure SetLimitInMemoryLogSize(const AValue: Boolean);
  public
    constructor Create(
      const AConfigData: IConfigDataWriteProvider
    );
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils;

{ TMainConfig }

constructor TMainConfig.Create(
  const AConfigData: IConfigDataWriteProvider
);
begin
  inherited Create;
  FConfigData := AConfigData;
  FTruncateLogFile := True;
  FLogFileMode := lfmNormal;
  FInMemoryLogSize := 5;
  FLimitInMemoryLogSize := False;
  DoReadConfig(FConfigData);
end;

destructor TMainConfig.Destroy;
begin
  DoWriteConfig(FConfigData);
  inherited Destroy;
end;

procedure TMainConfig.DoReadConfig(const AConfigData: IConfigDataProvider);
begin
  if AConfigData <> nil then begin
    FTruncateLogFile := AConfigData.ReadBool('TruncateLogFile', FTruncateLogFile);
    FLogFileMode := TLogFileMode(AConfigData.ReadInteger('LogFileMode', Integer(FLogFileMode)));
    FInMemoryLogSize := AConfigData.ReadInteger('InMemoryLogSize', FInMemoryLogSize);
    FLimitInMemoryLogSize := AConfigData.ReadBool('LimitInMemoryLogSize', FLimitInMemoryLogSize);
  end;
end;

procedure TMainConfig.DoWriteConfig(const AConfigData: IConfigDataWriteProvider);
begin
  if AConfigData <> nil then begin
    AConfigData.WriteBool('TruncateLogFile', FTruncateLogFile);
    AConfigData.WriteInteger('LogFileMode', Integer(FLogFileMode));
    AConfigData.WriteInteger('InMemoryLogSize', FInMemoryLogSize);
    AConfigData.WriteBool('LimitInMemoryLogSize', FLimitInMemoryLogSize);
  end;
end;

function TMainConfig.GetInMemoryLogSize: Integer;
begin
  Result := FInMemoryLogSize;
end;

function TMainConfig.GetLimitInMemoryLogSize: Boolean;
begin
  Result := FLimitInMemoryLogSize;
end;

function TMainConfig.GetLogFileMode: TLogFileMode;
begin
  Result := FLogFileMode;
end;

function TMainConfig.GetTruncateLogFile: Boolean;
begin
  Result := FTruncateLogFile;
end;

procedure TMainConfig.SetInMemoryLogSize(const AValue: Integer);
begin
  FInMemoryLogSize := AValue;
end;

procedure TMainConfig.SetLimitInMemoryLogSize(const AValue: Boolean);
begin
  FLimitInMemoryLogSize := AValue;
end;

procedure TMainConfig.SetLogFileMode(const AValue: TLogFileMode);
begin
  FLogFileMode := AValue;
end;

procedure TMainConfig.SetTruncateLogFile(const AValue: Boolean);
begin
  FTruncateLogFile := AValue;
end;

end.
