unit i_Config;

interface

uses
  i_ConfigDataProvider,
  i_ConfigDataWriteProvider;

type
  IConfig = interface
    ['{3B0CF0E5-958E-4AFB-A56F-3CC946BFC771}']
    procedure DoReadConfig(const AConfigData: IConfigDataProvider);
    procedure DoWriteConfig(const AConfigData: IConfigDataWriteProvider);
  end;

implementation

end.
