unit i_BerkeleyUtils;

interface

uses
  i_BerkeleyUtilsConfig;

type
  IBerkeleyUtils =interface
    ['{83B281AF-503D-4F8F-BC6E-2EF579940179}']
    function Run(const AConfig: IBerkeleyUtilsConfigStatic): UInt64;

    function GetProcessedOk: UInt64;
    property ProcessedOk: UInt64 read GetProcessedOK;

    function GetProcessedFail: UInt64;
    property ProcessedFail: UInt64 read GetProcessedFail;

    function GetSkipped: UInt64;
    property Skipped: UInt64 read GetSkipped;
  end;

implementation

end.
