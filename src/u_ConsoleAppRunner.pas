unit u_ConsoleAppRunner;
 
interface

uses
  Windows,
  i_ConsoleAppRunner;

type
  TConsoleAppRunner = class(TInterfacedObject, IConsoleAppRunner)
  private
    FPipeName: string;
    FAborted: Boolean;
    FIsEchoOff: Boolean;
    FOnOutputString: TOnOutputString;
    FOnProcessMessages: TOnProcessMessages;
    FTerminateOnAbort: Boolean;
    FUserData: Pointer;
    saSecurity: TSecurityAttributes;
    hNamedPipe: THandle;
    hFile: THandle;
    suiStartup: TStartupInfo;
    OverLapRd: OVERLAPPED;
    hEventRd: THANDLE;
    procedure InitPipe;
    procedure FinPipe;
    function OnProcessMessages: Boolean; inline;
  private
    { IConsoleAppRunner }
    function Run(
      const ACommand: string;
      const AParameters: string;
      const ACurrentDirectory: string;
      const AEnvinronment: string = ''
    ): Cardinal;
    function GetTerminateOnAbort: Boolean;
    procedure SetTerminateOnAbort(const AValue: Boolean);
  public
    constructor Create(
      const AIsEchoOff: Boolean;
      const AOnOutputString: TOnOutputString;
      const AOnProcessMessages: TOnProcessMessages;
      const AUserData: Pointer = nil
    );
    destructor Destroy; override;
  end;

const
  cConsoleAppAbortedByUser = $FFFF;
  cConsoleAppError = $FFFFFFFF;

implementation

uses
  SysUtils;

type
  EConsoleAppRunner = class(Exception);

const
  cBufferSize = 16384; // 16k
  cPipeName = '\\.\PIPE\ConsoleApp';
  cPipeMaxInstance = 100;
  cPipeDefaultTimeOut = 5000;

const
  cMaxCmdLineLength = 32768;

const
  cCRLF = #13#10#13#10;

{ TConsoleAppRunner }

constructor TConsoleAppRunner.Create(
  const AIsEchoOff: Boolean;
  const AOnOutputString: TOnOutputString;
  const AOnProcessMessages: TOnProcessMessages;
  const AUserData: Pointer
);
begin
  inherited Create;
  FAborted := False;
  FTerminateOnAbort := True;
  FIsEchoOff := AIsEchoOff;
  FOnOutputString := AOnOutputString;
  FOnProcessMessages := AOnProcessMessages;
  FUserData := AUserData;
  hNamedPipe := INVALID_HANDLE_VALUE;
  hFile := INVALID_HANDLE_VALUE;
  FPipeName := cPipeName + IntToHex(GetTickCount, 8);
  InitPipe;
end;

destructor TConsoleAppRunner.Destroy;
begin
  FAborted := True;
  FinPipe;
  inherited Destroy;
end;

procedure TConsoleAppRunner.InitPipe;
begin
  saSecurity.nLength := SizeOf(TSecurityAttributes);
  saSecurity.bInheritHandle := True;
  saSecurity.lpSecurityDescriptor := nil;

  hNamedPipe := CreateNamedPipe(
    PChar(FPipeName),
    PIPE_ACCESS_DUPLEX or FILE_FLAG_OVERLAPPED,
    PIPE_WAIT or PIPE_TYPE_BYTE,
    cPipeMaxInstance,
    cBufferSize,
    cBufferSize,
    cPipeDefaultTimeOut,
    @saSecurity
  );
   
  if hNamedPipe = INVALID_HANDLE_VALUE then begin
    raise EConsoleAppRunner.Create(
      'CreateNamedPipe Fail! ' + SysErrorMessage(GetLastError)
    );
  end;

  hFile := CreateFile(
    PChar(FPipeName),
    GENERIC_WRITE or GENERIC_READ,
    FILE_SHARE_READ or FILE_SHARE_WRITE,
    nil,
    OPEN_EXISTING,
    FILE_FLAG_NO_BUFFERING or FILE_FLAG_OVERLAPPED,
    0
  );

  if hFile = INVALID_HANDLE_VALUE then begin
    raise EConsoleAppRunner.Create(
      'CreateFile Fail! ' + SysErrorMessage(GetLastError)
    );
  end;

  FillChar(suiStartup, SizeOf(TStartupInfo), #0);
  suiStartup.cb := SizeOf(TStartupInfo);
  suiStartup.hStdInput := GetStdHandle(STD_INPUT_HANDLE);
  suiStartup.hStdOutput := hNamedPipe;
  suiStartup.hStdError := hNamedPipe;
  suiStartup.dwFlags := STARTF_USESTDHANDLES or STARTF_USESHOWWINDOW;
  suiStartup.wShowWindow := SW_HIDE;

  hEventRd := CreateEvent(nil, True, False, nil);
  FillChar(OverLapRd, SizeOf(OVERLAPPED), #0);
  OverLapRd.hEvent := hEventRd;
end;

procedure TConsoleAppRunner.FinPipe;
begin
  if hFile <> INVALID_HANDLE_VALUE then begin
    CloseHandle(hFile);
  end;
  if hNamedPipe <> INVALID_HANDLE_VALUE then begin
    CloseHandle(hNamedPipe);
  end;
  CloseHandle(hEventRd);
end;

function TConsoleAppRunner.GetTerminateOnAbort: Boolean;
begin
  Result := FTerminateOnAbort;
end;

procedure TConsoleAppRunner.SetTerminateOnAbort(const AValue: Boolean);
begin
  FTerminateOnAbort := AValue;
end;

function TConsoleAppRunner.OnProcessMessages: Boolean;
begin
  if Assigned(FOnProcessMessages) then begin
    Result := FOnProcessMessages(FUserData);
    FAborted := not Result;
  end else begin
    Result := True;
  end;
end;

function TConsoleAppRunner.Run(
  const ACommand: string;
  const AParameters: string;
  const ACurrentDirectory: string;
  const AEnvinronment: string
): Cardinal;
var
  piProcess: TProcessInformation;
  pBuffer: array [0..cBufferSize] of AnsiChar;
  dRead: DWord;
  dTransRead: DWord;
  dRunning: DWord;
  sBuffer: string;
  bResult: Boolean;
  lastError: Cardinal;
  dWaitResult: DWORD;
  VProcessIsFinish: Boolean;
  VCmdLine: string;
  VEnv: Pointer;

  procedure _Abort;
  begin
    FAborted := True;
    if FTerminateOnAbort then begin
      TerminateProcess(piProcess.hProcess, 1);
    end;
  end;

begin
  Result := cConsoleAppError;

  VCmdLine := Format('"%s" %s', [ACommand, AParameters]);
  Assert(Length(VCmdLine) < cMaxCmdLineLength);

  if AEnvinronment <> '' then begin
    {$IFNDEF UNICODE}
    Assert(Length(AEnvinronment) < 32767);
    {$ENDIF}
    VEnv := PChar(AEnvinronment);
  end else begin
    VEnv := nil;
  end;

  // http://msdn.microsoft.com/en-us/library/windows/desktop/ms682425%28v=vs.85%29.aspx

  bResult := CreateProcess(
    PChar(ACommand),                        // Application Name
    PChar(VCmdLine),                        // Command Line
    @saSecurity,                            // Process Attributes
    @saSecurity,                            // Thread Attributes
    True,                                   // Inherit Handles
    NORMAL_PRIORITY_CLASS or
    CREATE_NEW_PROCESS_GROUP,               // Creation Flags
    VEnv,                                   // Envinronment
    PChar(ACurrentDirectory),               // Current Directory
    suiStartup,                             // Startup Info
    piProcess                               // Process Info
  );
  if bResult then begin
    try
      if not FIsEchoOff then begin
        if not FOnOutputString('>> ' + VCmdLine + ' [' + ACurrentDirectory + ']' + cCRLF, FUserData) then begin
          _Abort;
          Result := cConsoleAppAbortedByUser;
          Exit;
        end;
      end;
      repeat
        dRunning := WaitForSingleObject(piProcess.hProcess, 50);
        repeat
          dRead := 0;
          bResult := ReadFile(hFile, pBuffer[0], cBufferSize, dRead, @OverLapRd);
          if not bResult then begin
            lastError := GetLastError;
            if lastError = ERROR_IO_PENDING then begin
              VProcessIsFinish := False;
              repeat
                dWaitResult := WaitForSingleObject(hEventRd, 50);
                if dWaitResult = WAIT_OBJECT_0 then begin
                  Break;
                end else begin
                  if VProcessIsFinish then begin
                    if not FIsEchoOff then begin
                      FOnOutputString('<< ExitCode = ' + IntToStr(Result) + cCRLF, FUserData);
                    end;
                    Exit;
                  end;
                  if GetExitCodeProcess(piProcess.hProcess, Result) then begin
                    VProcessIsFinish := Result <> STILL_ACTIVE;
                  end;
                  if not OnProcessMessages then begin
                    _Abort;
                    Result := cConsoleAppAbortedByUser;
                    Exit;
                  end;
                end;
              until False;
            end else if lastError = ERROR_BROKEN_PIPE then begin
              if not FAborted then begin
                raise EConsoleAppRunner.Create(
                  'ReadFile: Pipe Broken! ' + SysErrorMessage(GetLastError)
                );
              end;
            end else begin
              if not FAborted then begin
                raise EConsoleAppRunner.Create(
                  'ReadFile: Fail! ' + SysErrorMessage(GetLastError)
                );
              end;
            end;
          end;
          if GetOverlappedResult(hFile, OverLapRd, dTransRead, False) then begin
            if dTransRead <> 0 then begin
              if Assigned(FOnOutputString) then begin
                pBuffer[dTransRead] := #0;
                sBuffer := StringReplace(
                  AnsiString(pBuffer),
                  #$D#$D#$A,
                  #$D#$A,
                  [rfReplaceAll, rfIgnoreCase]
                );
                if not FOnOutputString(sBuffer, FUserData) then begin
                  _Abort;
                  Result := cConsoleAppAbortedByUser;
                  Exit;
                end; 
              end;
            end else begin
              if not FAborted then begin
                Assert(False, 'dTransRead = 0');
              end;
            end;
          end else begin
            if not FAborted then begin
              Assert(False, 'GetOverlappedResult Fail!');
            end;
          end;
          if not OnProcessMessages then begin
            _Abort;
            Result := cConsoleAppAbortedByUser;
            Exit;
          end;
        until (dRead < cBufferSize);
      until (dRunning <> WAIT_TIMEOUT);
      if not GetExitCodeProcess(piProcess.hProcess, Result) then begin
        if not FAborted then begin
          raise EConsoleAppRunner.Create('GetExitCodeProcess Fail!');
        end;
      end;
      if not FIsEchoOff then begin
        FOnOutputString('<< ExitCode = ' + IntToStr(Result) + cCRLF, FUserData);
      end;
    finally
      CloseHandle(piProcess.hProcess);
      CloseHandle(piProcess.hThread);
    end;
  end else if Assigned(FOnOutputString) then begin
    FOnOutputString('>> ' + VCmdLine + ' [' + ACurrentDirectory + ']' + cCRLF, FUserData);
    FOnOutputString('<< ' + 'Error: ' + SysErrorMessage(GetLastError), FUserData);
  end;
end;

end.
