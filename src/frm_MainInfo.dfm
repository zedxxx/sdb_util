object frmMainInfo: TfrmMainInfo
  Left = 0
  Top = 0
  Caption = 'sdb_info'
  ClientHeight = 480
  ClientWidth = 715
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object lblRootCachePath: TLabel
    Left = 8
    Top = 406
    Width = 85
    Height = 13
    Align = alCustom
    Anchors = [akLeft, akBottom]
    Caption = 'Root Cache Path:'
  end
  object btnRun: TButton
    Left = 543
    Top = 430
    Width = 83
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = 'Scan'
    TabOrder = 0
    OnClick = btnRunClick
  end
  object pnlVT: TPanel
    Left = 0
    Top = 0
    Width = 715
    Height = 397
    Align = alCustom
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelOuter = bvNone
    TabOrder = 1
  end
  object edtPath: TEdit
    Left = 99
    Top = 403
    Width = 574
    Height = 21
    Align = alCustom
    Anchors = [akLeft, akRight, akBottom]
    TabOrder = 2
    OnChange = edtPathChange
  end
  object statBottom: TStatusBar
    Left = 0
    Top = 461
    Width = 715
    Height = 19
    Panels = <
      item
        Width = 120
      end
      item
        Width = 50
      end>
  end
  object chkShowUnk: TCheckBox
    Left = 167
    Top = 438
    Width = 193
    Height = 17
    Align = alCustom
    Anchors = [akLeft, akBottom]
    Caption = 'Show files with Unknown status'
    Checked = True
    State = cbChecked
    TabOrder = 4
    OnClick = OnShowOptClick
  end
  object chkShowOk: TCheckBox
    Left = 8
    Top = 438
    Width = 153
    Height = 17
    Align = alCustom
    Anchors = [akLeft, akBottom]
    Caption = 'Show files with OK status'
    Checked = True
    State = cbChecked
    TabOrder = 5
    OnClick = OnShowOptClick
  end
  object btnExit: TButton
    Left = 632
    Top = 430
    Width = 75
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = 'Exit'
    TabOrder = 6
    OnClick = btnExitClick
  end
  object btnSelectCachePath: TButton
    Left = 679
    Top = 403
    Width = 28
    Height = 21
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = '...'
    TabOrder = 7
    OnClick = btnSelectCachePathClick
  end
  object tmrProgressCheck: TTimer
    Enabled = False
    Interval = 250
    OnTimer = tmrProgressCheckTimer
    Left = 504
    Top = 232
  end
end
