unit i_ConsoleAppRunner;

interface

type
  TOnOutputString = function(const ABuffer: string; const AUserData: Pointer): Boolean of object;
  TOnProcessMessages = function(const AUserData: Pointer): Boolean of object;

  IConsoleAppRunner = interface
    ['{00EF2BE9-9903-47D2-A0B9-C85FA7513337}']
    function Run(
      const ACommand: string;
      const AParameters: string;
      const ACurrentDirectory: string;
      const AEnvinronment: string = ''
    ): Cardinal;

    function GetTerminateOnAbort: Boolean;
    procedure SetTerminateOnAbort(const AValue: Boolean);
    property TerminateOnAbort: Boolean read GetTerminateOnAbort write SetTerminateOnAbort;
  end;

implementation

end.
