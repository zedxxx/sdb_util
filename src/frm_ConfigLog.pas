unit frm_ConfigLog;

interface

uses
  Windows,
  Messages,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  Dialogs,
  StdCtrls,
  Spin,
  i_MainConfig;

type
  TfrmConfigLog = class(TForm)
    chkLimitMemLogSize: TCheckBox;
    btnCancel: TButton;
    btnApply: TButton;
    cbbFileLogMode: TComboBox;
    lblFileLogMode: TLabel;
    chkTruncateLog: TCheckBox;
    seMemLogSize: TSpinEdit;
    procedure FormShow(Sender: TObject);
    procedure btnApplyClick(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure cbbFileLogModeChange(Sender: TObject);
    procedure chkLimitMemLogSizeClick(Sender: TObject);
  private
    FMainConfig: IMainConfig;
  public
    constructor Create(
      AOwner: TComponent;
      const AMainConfig: IMainConfig
    ); reintroduce;
  end;

implementation

{$R *.dfm}

constructor TfrmConfigLog.Create(
  AOwner: TComponent;
  const AMainConfig: IMainConfig
);
begin
  inherited Create(AOwner);
  FMainConfig := AMainConfig;
end;

procedure TfrmConfigLog.FormShow(Sender: TObject);
begin
  chkTruncateLog.Checked := FMainConfig.TruncateLogFile;
  chkTruncateLog.Enabled := FMainConfig.LogFileMode <> lfmNone;

  chkLimitMemLogSize.Checked := FMainConfig.LimitInMemoryLogSize;
  seMemLogSize.Value := FMainConfig.InMemoryLogSize;
  seMemLogSize.Enabled := FMainConfig.LimitInMemoryLogSize;

  cbbFileLogMode.ItemIndex := Integer(FMainConfig.LogFileMode);
end;

procedure TfrmConfigLog.btnApplyClick(Sender: TObject);
begin
  FMainConfig.TruncateLogFile := chkTruncateLog.Checked;
  FMainConfig.LimitInMemoryLogSize := chkLimitMemLogSize.Checked;
  FMainConfig.LogFileMode := TLogFileMode(cbbFileLogMode.ItemIndex);
  FMainConfig.InMemoryLogSize := seMemLogSize.Value;

  Close;
end;

procedure TfrmConfigLog.btnCancelClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmConfigLog.cbbFileLogModeChange(Sender: TObject);
begin
  chkTruncateLog.Enabled := cbbFileLogMode.ItemIndex > 0;
end;

procedure TfrmConfigLog.chkLimitMemLogSizeClick(Sender: TObject);
begin
  seMemLogSize.Enabled := chkLimitMemLogSize.Checked;
end;

end.
