unit frm_Main;

interface

uses
  Messages,
  SysUtils,
  Classes,
  Graphics,
  Controls,
  Forms,
  StdCtrls,
  ComCtrls,
  frm_Config,
  frm_ConfigLog,
  i_LogFile,
  i_FormConfig,
  i_MainConfig,
  i_BerkeleyUtils,
  i_BerkeleyUtilsConfig,
  i_ConfigDataWriteProvider;

const
  WM_AUTOSTART = WM_USER + 1;
  WM_OUTPUTSTRING = WM_USER + 2;

type
  TfrmMain = class(TForm)
    mmoCmd: TMemo;
    btnRun: TButton;
    edtCmdStr: TEdit;
    cbbUtils: TComboBox;
    lblPath: TLabel;
    btnExit: TButton;
    btnOpenFolder: TButton;
    statBottom: TStatusBar;
    btnConfig: TButton;
    chkWithSubFolders: TCheckBox;
    btnClearLog: TButton;
    btnConfigLog: TButton;
    procedure btnRunClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnConfigClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure WMGetMinMaxInfo(var M: TWMGetMinMaxInfo); message WM_GetMinMaxInfo;
    procedure WMAutoStart(var M: TMessage); message WM_AUTOSTART;
    procedure WMOutputString(var M: TMessage); message WM_OUTPUTSTRING;
    procedure btnOpenFolderClick(Sender: TObject);
    procedure edtCmdStrChange(Sender: TObject);
    procedure cbbUtilsChange(Sender: TObject);
    procedure btnClearLogClick(Sender: TObject);
    procedure chkWithSubFoldersClick(Sender: TObject);
    procedure btnConfigLogClick(Sender: TObject);
  private
    FfrmConfig: TfrmConfig;
    FfrmConfigLog: TfrmConfigLog;
    FConfigProvider: IConfigDataWriteProvider;
    FFormConfig: IFormConfig;
    FMainConfig: IMainConfig;
    FBerkeleyUtilsConfig: IBerkeleyUtilsConfig;
    FIsRunning: Boolean;
    FIsAborted: Boolean;
    FUtils: IBerkeleyUtils;
    FLongTimeOperationPrefix: string;
    FIsLongTimeOperation: Boolean;
    FLastLongTimeOperationProgressSymbol: Byte;
    FProgressUpdateTick: Cardinal;
    FLogFile: ILogFile;
    FMemLogSize: Int64;
    FTmpLogSize: Int64;
    FAutoStart: Boolean;
    FProcessMsg: Boolean;
    FStringsTmp: TStringList;
    procedure ClearLog; inline;
    function GetOperationProgress: string;
    function OnProcessMessages(const AUserData: Pointer): Boolean;
    function OnOutputString(const ABuffer: string; const AUserData: Pointer): Boolean;
    procedure OnLongTimeOperation(
      Sender: TObject;
      const AOperationName: string;
      const AIsOperationFin: Boolean
    );
    procedure UtilRun;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  end;

var
  frmMain: TfrmMain;

implementation

uses
  Windows,
  {$WARN UNIT_PLATFORM OFF}
  FileCtrl,
  {$WARN UNIT_PLATFORM ON}
  IniFiles,
  u_Tools,
  u_LogFile,
  u_FormConfig,
  u_MainConfig,
  u_ResourceStrings,
  u_BerkeleyUtils,
  u_BerkeleyUtilsConfig,
  u_ConfigDataWriteProviderByIniFile;

const
  cFormConfigSection = 'Form';
  cMainConfigSection = 'Main';
  cUtilsConfigSettings = 'Utils';
  cDefConsoleColor = $00291919;
  cDefConsoleFontColor = clSilver;
  cLongTimeOperationProgress: array [0..7] of string =
    ('|', '/', '--', '\', '|', '/', '--', '\');
  cCopyrightStr = 'Copyright ' + #169 + ' 2012-2016, zed';

{$R *.dfm}

procedure ParseCmdLineParams(
  out AAutoStart: Boolean
);
var
  I: Integer;
begin
  AAutoStart := False;
  if ParamCount > 0 then begin
    for I := 1 to ParamCount do begin
      if SameText('--autostart', ParamStr(I)) then begin
        AAutoStart := True;
      end;
    end;
  end;
end;

constructor TfrmMain.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  FProcessMsg := True;

  ParseCmdLineParams(FAutoStart);

  FConfigProvider := TConfigDataWriteProviderByIniFile.Create(
    TMemIniFile.Create(
      ChangeFileExt(ExtractFileName(ParamStr(0)), '.ini')
    )
  );

  FFormConfig := TFormConfig.Create(
    FConfigProvider.GetOrCreateSubItem(cFormConfigSection),
    Self.BoundsRect,
    cDefConsoleColor,
    cDefConsoleFontColor
  );

  FMainConfig := TMainConfig.Create(
    FConfigProvider.GetOrCreateSubItem(cMainConfigSection)
  );

  FBerkeleyUtilsConfig := TBerkeleyUtilsConfig.Create(
    FConfigProvider.GetOrCreateSubItem(cUtilsConfigSettings)
  );

  FfrmConfig := TfrmConfig.Create(Self, FBerkeleyUtilsConfig);
  FfrmConfigLog := TfrmConfigLog.Create(Self, FMainConfig);

  FUtils := TBerkeleyUtils.Create(
    Self.OnOutputString,
    Self.OnProcessMessages,
    Self.OnLongTimeOperation
  );

  FLongTimeOperationPrefix := '';
  FIsLongTimeOperation := False;
  FLastLongTimeOperationProgressSymbol := 0;
  FProgressUpdateTick := 0;

  FLogFile := nil;
  FStringsTmp := TStringList.Create;
  FMemLogSize := 0;
  FTmpLogSize := 0;

  Self.Caption := Self.Caption + ' ' + GetAppVersion;
end;

destructor TfrmMain.Destroy;
begin
  FfrmConfig.Free;
  FfrmConfigLog.Free;
  FUtils := nil;
  FBerkeleyUtilsConfig := nil;
  FFormConfig := nil;
  FMainConfig := nil;
  FConfigProvider := nil;
  FLogFile := nil;
  FStringsTmp.Free;
  inherited Destroy;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  Self.BoundsRect := FFormConfig.GetBoundsRect;

  mmoCmd.Color := FFormConfig.GetConsoleColor;
  mmoCmd.Font.Color := FFormConfig.GetConsoleFontColor;
  mmoCmd.Lines.Add('');
  mmoCmd.Lines.Add(rsHint);

  cbbUtils.ItemIndex := Integer(FBerkeleyUtilsConfig.UtilType);
  edtCmdStr.Text := FBerkeleyUtilsConfig.RootFolder;
  chkWithSubFolders.Checked := FBerkeleyUtilsConfig.WithSubFolders;

  statBottom.Panels.Items[0].Text := cCopyrightStr;

  FIsRunning := False;
  FIsAborted := False;
end;

procedure TfrmMain.FormDestroy(Sender: TObject);
begin
  FFormConfig.SetWindowPosition(Self.BoundsRect);
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  edtCmdStr.SetFocus;
  if FAutoStart then begin
    PostMessage(Self.Handle, WM_AUTOSTART, 0, 0);
  end;
end;

procedure TfrmMain.WMGetMinMaxInfo(var M: TWMGetMinMaxInfo);
begin
  M.MinMaxInfo^.PTMinTrackSize.X := 650;
  M.MinMaxInfo^.PTMinTrackSize.Y := 300;
end;

procedure TfrmMain.WMAutoStart(var M: TMessage);
begin
  btnRunClick(Self);
end;

procedure TfrmMain.ClearLog;
begin
  if (FLogFile <> nil) and (FMainConfig.LogFileMode = lfmNormal) then begin
    FLogFile.WriteText(mmoCmd.Lines.Text);
  end;
  mmoCmd.Lines.Clear;
  FMemLogSize := 0;
end;

procedure TfrmMain.WMOutputString(var M: TMessage);
begin
  if FStringsTmp.Count > 0 then begin
    mmoCmd.Lines.BeginUpdate;
    try
      if FMainConfig.LimitInMemoryLogSize and
        (FMemLogSize + FTmpLogSize > FMainConfig.InMemoryLogSize * 1024 * 1024) then
      begin
        ClearLog;
      end;
      Inc(FMemLogSize, FTmpLogSize);
      mmoCmd.Lines.AddStrings(FStringsTmp);
      mmoCmd.Perform(EM_LINESCROLL, 0, mmoCmd.Lines.Count - 1);
    finally
      mmoCmd.Lines.EndUpdate;
    end;
    FStringsTmp.Clear;
    FTmpLogSize := 0;
  end;
  FProcessMsg := True;
end;

procedure TfrmMain.btnExitClick(Sender: TObject);
begin
  if FIsRunning then begin
    FIsAborted := True;
    Application.ProcessMessages;
  end;
  Close;
end;

procedure TfrmMain.btnClearLogClick(Sender: TObject);
begin
  ClearLog;
  if not FIsRunning then begin
    mmoCmd.Lines.Add('');
    mmoCmd.Lines.Add(rsHint);
  end;
end;

procedure TfrmMain.btnOpenFolderClick(Sender: TObject);
var
  VPath: string;
begin
  VPath := edtCmdStr.Text;
  if SelectDirectory('Select Directory', '', VPath) then begin
    edtCmdStr.Text := IncludeTrailingPathDelimiter(VPath);
  end;
end;

procedure TfrmMain.btnConfigClick(Sender: TObject);
begin
  FfrmConfig.Show;
end;

procedure TfrmMain.btnConfigLogClick(Sender: TObject);
begin
  FfrmConfigLog.Show;
end;

procedure TfrmMain.btnRunClick(Sender: TObject);
begin
  if FIsRunning then begin
    FIsAborted := True;
    FIsRunning := False;
    btnRun.Caption := rsRun;
  end else begin
    FIsAborted := False;
    FIsRunning := True;
    btnRun.Caption := rsAbort;
    UtilRun;
  end;
end;

procedure TfrmMain.cbbUtilsChange(Sender: TObject);
begin
  FBerkeleyUtilsConfig.UtilType := TUtilType(cbbUtils.ItemIndex);
end;

procedure TfrmMain.chkWithSubFoldersClick(Sender: TObject);
begin
  FBerkeleyUtilsConfig.WithSubFolders := chkWithSubFolders.Checked;
end;

procedure TfrmMain.edtCmdStrChange(Sender: TObject);
begin
  FBerkeleyUtilsConfig.RootFolder := edtCmdStr.Text;
end;

function TfrmMain.GetOperationProgress: string;
var
  I: Integer;
begin
  if FIsRunning and FIsLongTimeOperation and (FLongTimeOperationPrefix <> '') then begin
    I := FLastLongTimeOperationProgressSymbol;
    if I >= 8 then begin
      I := 0;
    end;
    if GetTickCount - FProgressUpdateTick > 500 then begin
      FLastLongTimeOperationProgressSymbol := I + 1;
      FProgressUpdateTick := GetTickCount;
    end;
    Result := FLongTimeOperationPrefix + '...  ' + cLongTimeOperationProgress[I];
  end;
end;

function TfrmMain.OnProcessMessages(const AUserData: Pointer): Boolean;
begin
  if Assigned(FUtils) then begin
    statBottom.Panels.Items[1].Text :=
      rsOk + ': ' + FloatToStrF(FUtils.ProcessedOk, ffNumber, 12, 0) + '  ' +
      rsErrors + ': ' + FloatToStrF(FUtils.ProcessedFail, ffNumber, 12, 0) + '  ' +
      rsSkipped + ': ' + FloatToStrF(FUtils.Skipped, ffNumber, 12, 0);
    statBottom.Panels.Items[2].Text := GetOperationProgress;
  end;
  Application.ProcessMessages;
  Result := not FIsAborted;
end;

function TfrmMain.OnOutputString(const ABuffer: string; const AUserData: Pointer): Boolean;
begin
  if ABuffer <> '' then begin
    if (FLogFile <> nil) and (FMainConfig.LogFileMode = lfmExtreme) then begin
      FLogFile.WriteText(ABuffer);
    end;
    Inc(FTmpLogSize, Length(ABuffer));
    FStringsTmp.Text := FStringsTmp.Text + ABuffer;
  end;
  if FProcessMsg then begin
    FProcessMsg := False;
    PostMessage(Self.Handle, WM_OUTPUTSTRING, 0, 0);
  end;
  Result := not FIsAborted;
end;

procedure TfrmMain.OnLongTimeOperation(
  Sender: TObject;
  const AOperationName: string;
  const AIsOperationFin: Boolean
);
begin
  FLongTimeOperationPrefix := AOperationName;
  FIsLongTimeOperation := not AIsOperationFin;
end;

procedure TfrmMain.UtilRun;
begin
  if (FLogFile = nil) and (FMainConfig.LogFileMode in [lfmNormal, lfmExtreme]) then begin
    FLogFile := TLogFile.Create(
      ChangeFileExt(ExtractFileName(ParamStr(0)), '.log')
    );
    if FMainConfig.TruncateLogFile then begin
      FLogFile.Truncate;
    end;
  end;

  mmoCmd.Lines.Clear;
  FUtils.Run(FBerkeleyUtilsConfig.CreateStatic);

  if (FLogFile <> nil) and (FMainConfig.LogFileMode = lfmNormal) then begin
    FLogFile.WriteText(mmoCmd.Lines.Text);
  end;

  FIsRunning := False;
  btnRun.Caption := rsRun;
  
  Self.OnProcessMessages(nil);
end;

end.
