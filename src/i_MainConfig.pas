unit i_MainConfig;

interface


type
  TLogFileMode = (lfmNone, lfmNormal, lfmExtreme);

  IMainConfig = interface
    ['{B7B8DFB3-EECC-4400-B26F-272CB0FD62F5}']
    function GetTruncateLogFile: Boolean;
    procedure SetTruncateLogFile(const AValue: Boolean);
    property TruncateLogFile: Boolean read GetTruncateLogFile write SetTruncateLogFile;

    function GetLogFileMode: TLogFileMode;
    procedure SetLogFileMode(const AValue: TLogFileMode);
    property LogFileMode: TLogFileMode read GetLogFileMode write SetLogFileMode;

    function GetInMemoryLogSize: Integer;
    procedure SetInMemoryLogSize(const AValue: Integer);
    property InMemoryLogSize: Integer read GetInMemoryLogSize write SetInMemoryLogSize;

    function GetLimitInMemoryLogSize: Boolean;
    procedure SetLimitInMemoryLogSize(const AValue: Boolean);
    property LimitInMemoryLogSize: Boolean read GetLimitInMemoryLogSize write SetLimitInMemoryLogSize;
  end;

implementation

end.
