unit u_BerkeleyDBVerifyCache;

interface

{.$DEFINE USE_ORM}

uses
  SysUtils,
  t_BerkeleyDB,
  i_Timer,
  i_ConsoleAppRunner,
  i_BerkeleyDBVerifyCache,
  {$IFDEF USE_ORM}
  u_BerkeleyDBVerifyCacheImplORM
  {$ELSE}
  u_BerkeleyDBVerifyCacheImplDirect
  {$ENDIF}
  ;

type
  TBerkeleyDBVerifyCache = class(TInterfacedObject, IBerkeleyDBVerifyCache)
  private
    FImpl: IBerkeleyDBVerifyCache;
    FOnOutputString: TOnOutputString;
    FLogPrefix: string;
    FTimer: ITimer;
    FDuration: Int64;
    FEnabled: Boolean;
    procedure LogE(const E: Exception);
    function DurationToStr(const ADuration: Int64): string;
  private
    { IBerkeleyDBVerifyCache }
    function Add(
      const AFileName: string;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): TRecID;

    function Delete(
      const AID: TRecID
    ): Boolean;

    function Read(
      const AFileName: string;
      out AIsChanged: Boolean;
      out AFileInfo: TFileInfoRec
    ): Boolean;

    function Update(
      const AID: TRecID;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): Boolean;

    function UpdateFull(
      const AFileName: string;
      const AID: TRecID;
      const AStatus: TBerkeleyDBVerifyStatus;
      const AResultCode: Cardinal
    ): Boolean;

    function GetIsAvailable: Boolean;
  public
    constructor Create(
      const AFileName: string;
      const AOnOutputString: TOnOutputString = nil
    );
    destructor Destroy; override;
  end;

implementation

uses
  Math,
  u_TimerByQueryPerformanceCounter;

const
  cCRLF = #13#10#13#10;

function VerifyStatusToStr(const AStatus: TBerkeleyDBVerifyStatus): string; inline;
begin
  case AStatus of
    vsNone:    Result := 'None';
    vsOK:      Result := 'OK';
    vsError:   Result := 'Error';
    vsAborted: Result := 'Aborted';
    vsBusy:    Result := 'Busy';
  else
    begin
      Result := 'Unknown';
      Assert(False, 'Unknown Verify Status value: ' + IntToStr(Integer(AStatus)));
    end;
  end;
  Result := Result + ' (' + IntToStr(Integer(AStatus)) + ')';
end;

function VerifyResultToStr(const AResult: Cardinal): string; inline;
begin
  Result := '0x' + IntToHex(AResult, 8) + ' (' + IntToStr(AResult) + ')';
end;

{ TBerkeleyDBVerifyCache }

constructor TBerkeleyDBVerifyCache.Create(
  const AFileName: string;
  const AOnOutputString: TOnOutputString
);
begin
  inherited Create;
  Assert(AFileName <> '');
  FOnOutputString := AOnOutputString;
  FLogPrefix := 'VerifyCache';
  FTimer := MakeTimerByQueryPerformanceCounter;
  try
    {$IFDEF USE_ORM}
    FImpl := TBerkeleyDBVerifyCacheImplORM.Create(AFileName);
    {$ELSE}
    FImpl := TBerkeleyDBVerifyCacheImplDirect.Create(AFileName);
    {$ENDIF}
    FEnabled := True;
  except
    on E: Exception do begin
      FEnabled := False;
      LogE(E);
    end;
  end;
end;

destructor TBerkeleyDBVerifyCache.Destroy;
begin
  FImpl := nil;
  FTimer := nil;
  inherited Destroy;
end;

procedure TBerkeleyDBVerifyCache.LogE(const E: Exception);
begin
  if Assigned(FOnOutputString) then begin
    FOnOutputString('<< !!! EXCEPTION: ' + E.Message + cCRLF, nil);
  end;
end;

function TBerkeleyDBVerifyCache.DurationToStr(const ADuration: Int64): string;
var
  VMinutes: Integer;
  VDuration: Double;
begin
  VDuration := ADuration / FTimer.Freq;
  if VDuration < 1 then begin
    Result := Format('[%.4f ms]', [1000 * VDuration]);
  end else if VDuration < 60 then begin
    Result := Format('[%.2f sec]', [VDuration]);
  end else begin
    VMinutes := Floor(VDuration / 60);
    Result := Format('[%d min %d sec]', [VMinutes, Round(VDuration - VMinutes * 60)]);
  end;
end;

function TBerkeleyDBVerifyCache.Add(
  const AFileName: string;
  const AStatus: TBerkeleyDBVerifyStatus;
  const AResultCode: Cardinal
): TRecID;
begin
  if not FEnabled then begin
    Result := 0;
    Exit;
  end;

  try
    Assert(AFileName <> '', 'FileName is empty!');
    
    if Assigned(FOnOutputString) then begin
      FOnOutputString('>> ' + FLogPrefix + '.Add: ' + AFileName + cCRLF, nil);
    end;

    FDuration := FTimer.CurrentTime;
    Result := FImpl.Add(AFileName, AStatus, AResultCode);
    FDuration := FTimer.CurrentTime - FDuration;

    if Assigned(FOnOutputString) then begin
      FOnOutputString(
        '<< ID: ' + IntToStr(Result) +
        '; Status: ' + VerifyStatusToStr(AStatus) +
        '; Result: ' + VerifyResultToStr(AResultCode) +
        ' ' + DurationToStr(FDuration) + cCRLF,
        nil
      );
    end;
  except
    on E: Exception do begin
      Result := 0;
      LogE(E);
    end;
  end;
end;

function TBerkeleyDBVerifyCache.Read(
  const AFileName: string;
  out AIsChanged: Boolean;
  out AFileInfo: TFileInfoRec
): Boolean;
begin
  if not FEnabled then begin
    Result := False;
    Exit;
  end;

  try
    if Assigned(FOnOutputString) then begin
      FOnOutputString('>> ' + FLogPrefix + '.Read: ' + AFileName + cCRLF, nil);
    end;

    Assert(AFileName <> '', 'FileName is empty!');

    FDuration := FTimer.CurrentTime;
    Result := FImpl.Read(AFileName, AIsChanged, AFileInfo);
    FDuration := FTimer.CurrentTime - FDuration;

    if Assigned(FOnOutputString) then begin
      if Result then begin
        FOnOutputString(
          '<< Exists: True' +
          '; Changed: ' + BoolToStr(AIsChanged, True) +
          '; Verified: ' + FormatDateTime('yyyy-mm-dd hh:mm:ss.zzz', AFileInfo.FVerifyTime) +
          ' ' + DurationToStr(FDuration) + cCRLF,
          nil
        );
      end else begin
        FOnOutputString('<< Exists: False ' + DurationToStr(FDuration) + cCRLF, nil);
      end;
    end;
  except
    on E: Exception do begin
      Result := False;
      LogE(E);
    end;
  end;
end;

function TBerkeleyDBVerifyCache.Update(
  const AID: TRecID;
  const AStatus: TBerkeleyDBVerifyStatus;
  const AResultCode: Cardinal
): Boolean;
begin
  if not FEnabled then begin
    Result := False;
    Exit;
  end;

  try
    if Assigned(FOnOutputString) then begin
      FOnOutputString('>> ' + FLogPrefix + '.Update: ID = ' + IntToStr(AID) + cCRLF, nil);
    end;

    Assert(AID <> 0, 'Invalid ID value: ' + IntToStr(AID));

    FDuration := FTimer.CurrentTime;
    Result := FImpl.Update(AID, AStatus, AResultCode);
    FDuration := FTimer.CurrentTime - FDuration;

    if Assigned(FOnOutputString) then begin
      FOnOutputString(
        '<< Updated: ' + BoolToStr(Result, True) +
        '; Status: ' + VerifyStatusToStr(AStatus) +
        '; Result: ' + VerifyResultToStr(AResultCode) +
        ' ' + DurationToStr(FDuration) + cCRLF,
        nil
      );
    end;
  except
    on E: Exception do begin
      Result := False;
      LogE(E);
    end;
  end;
end;

function TBerkeleyDBVerifyCache.UpdateFull(
  const AFileName: string;
  const AID: TRecID;
  const AStatus: TBerkeleyDBVerifyStatus;
  const AResultCode: Cardinal
): Boolean;
begin
  if not FEnabled then begin
    Result := False;
    Exit;
  end;
  
  try
    if Assigned(FOnOutputString) then begin
      FOnOutputString('>> ' + FLogPrefix + '.UpdateFull: ID = ' + IntToStr(AID) + cCRLF, nil);
    end;

    Assert(AFileName <> '', 'FileName is empty!');
    Assert(AID <> 0, 'Invalid ID value: ' + IntToStr(AID));

    FDuration := FTimer.CurrentTime;
    Result := FImpl.UpdateFull(AFileName, AID, AStatus, AResultCode);
    FDuration := FTimer.CurrentTime - FDuration;

    if Assigned(FOnOutputString) then begin
      FOnOutputString(
        '<< Updated: ' + BoolToStr(Result, True) +
        '; Status: ' + VerifyStatusToStr(AStatus) +
        '; Result: ' + VerifyResultToStr(AResultCode) +
        ' ' + DurationToStr(FDuration) + cCRLF,
        nil
      );
    end;
  except
    on E: Exception do begin
      Result := False;
      LogE(E);
    end;
  end;
end;

function TBerkeleyDBVerifyCache.Delete(const AID: TRecID): Boolean;
begin
  if not FEnabled then begin
    Result := False;
    Exit;
  end;

  try
    if Assigned(FOnOutputString) then begin
      FOnOutputString('>> ' + FLogPrefix + '.Delete: ID = ' + IntToStr(AID) + cCRLF, nil);
    end;

    Assert(AID <> 0, 'Invalid ID value: ' + IntToStr(AID));

    FDuration := FTimer.CurrentTime;
    Result := FImpl.Delete(AID);
    FDuration := FTimer.CurrentTime - FDuration;

    if Assigned(FOnOutputString) then begin
      FOnOutputString(
        '<< Deleted: ' + BoolToStr(Result, True) +
        ' ' + DurationToStr(FDuration) + cCRLF,
        nil
      );
    end;
  except
    on E: Exception do begin
      Result := False;
      LogE(E);
    end;
  end;
end;

function TBerkeleyDBVerifyCache.GetIsAvailable: Boolean;
begin
  Result := FEnabled and (FImpl <> nil) and FImpl.IsAvailable;
end;

end.
