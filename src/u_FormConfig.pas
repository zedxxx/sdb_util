unit u_FormConfig;

interface

uses
  Types,
  i_FormConfig,
  i_ConfigDataProvider,
  i_ConfigDataWriteProvider;

type
  TFormConfig = class(TInterfacedObject, IFormConfig)
  private
    FConfigData: IConfigDataWriteProvider;
    FBoundsRect: TRect;
    FConsoleColor: Integer;
    FConsoleFontColor: Integer;
  private
    procedure DoReadConfig(const AConfigData: IConfigDataProvider);
    procedure DoWriteConfig(const AConfigData: IConfigDataWriteProvider);
    // IFormConfig
    function GetBoundsRect: TRect;
    procedure SetWindowPosition(const ARect: TRect);
    function GetConsoleColor: Integer;
    procedure SetConsoleColor(const AColor: Integer);
    function GetConsoleFontColor: Integer;
    procedure SetConsoleFontColor(const AColor: Integer);
  public
    constructor Create(
      const AConfigData: IConfigDataWriteProvider;
      const ABoundsRect: TRect;
      const AConsoleColor: Integer;
      const AConsoleFontColor: Integer
    );
    destructor Destroy; override;
  end;

implementation

uses
  SysUtils;

{ TMainFormConfig }

constructor TFormConfig.Create(
  const AConfigData: IConfigDataWriteProvider;
  const ABoundsRect: TRect;
  const AConsoleColor: Integer;
  const AConsoleFontColor: Integer
);
begin
  inherited Create;
  FConfigData := AConfigData;
  FBoundsRect := ABoundsRect;
  FConsoleColor := AConsoleColor;
  FConsoleFontColor := AConsoleFontColor;
  DoReadConfig(FConfigData);
end;

destructor TFormConfig.Destroy;
begin
  DoWriteConfig(FConfigData);
  inherited Destroy;
end;

procedure TFormConfig.DoReadConfig(const AConfigData: IConfigDataProvider);
begin
  if AConfigData <> nil then begin
    FBoundsRect := Bounds(
      AConfigData.ReadInteger('Left', FBoundsRect.Left),
      AConfigData.ReadInteger('Top', FBoundsRect.Top),
      AConfigData.ReadInteger('Width', FBoundsRect.Right - FBoundsRect.Top),
      AConfigData.ReadInteger('Height', FBoundsRect.Bottom - FBoundsRect.Top)
    );

    FConsoleColor := StrToInt(
      AConfigData.ReadString('ConsoleColor', '$' + IntToHex(FConsoleColor, 8))
    );

    FConsoleFontColor := StrToInt(
      AConfigData.ReadString('ConsoleFontColor', '$' + IntToHex(FConsoleFontColor, 8))
    );
  end;
end;

procedure TFormConfig.DoWriteConfig(const AConfigData: IConfigDataWriteProvider);
begin
  if AConfigData <> nil then begin
    AConfigData.WriteInteger('Left', FBoundsRect.Left);
    AConfigData.WriteInteger('Top', FBoundsRect.Top);
    AConfigData.WriteInteger('Width', FBoundsRect.Right - FBoundsRect.Left);
    AConfigData.WriteInteger('Height', FBoundsRect.Bottom - FBoundsRect.Top);
    AConfigData.WriteString('ConsoleColor', '$' + IntToHex(FConsoleColor, 8));
    AConfigData.WriteString('ConsoleFontColor', '$' + IntToHex(FConsoleFontColor, 8));
  end;
end;

function TFormConfig.GetBoundsRect: TRect;
begin
  Result := FBoundsRect;
end;

procedure TFormConfig.SetWindowPosition(const ARect: TRect);
begin
  FBoundsRect := ARect;
end;

function TFormConfig.GetConsoleColor: Integer;
begin
  Result := FConsoleColor;
end;

procedure TFormConfig.SetConsoleColor(const AColor: Integer);
begin
  FConsoleColor := AColor;
end;

function TFormConfig.GetConsoleFontColor: Integer;
begin
  Result := FConsoleFontColor;
end;

procedure TFormConfig.SetConsoleFontColor(const AColor: Integer);
begin
  FConsoleFontColor := AColor;
end;

end.
