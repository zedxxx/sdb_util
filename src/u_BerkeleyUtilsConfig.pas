unit u_BerkeleyUtilsConfig;

interface

uses
  i_DbConfigPresetList,
  i_BerkeleyUtilsConfig,
  i_ConfigDataProvider,
  i_ConfigDataWriteProvider;

type
  TBerkeleyUtilsConfig = class(TInterfacedObject, IBerkeleyUtilsConfig)
  private
    FConfigData: IConfigDataWriteProvider;
    FRootFolder: string;
    FWithSubFolders: Boolean;
    FCatastrophicRecover: Boolean;
    FRemoveEnvOnLSNReset: Boolean;
    FRemoveBadFileOnSuccessRestore: Boolean;
    FDumpMode: TDumpMode;
    FVerifyMode: TVerifyMode;
    FUtilType: TUtilType;
    FOverwriteDbConfig: Boolean;
    FDbConfigPresetList: IDbConfigPresetList;
    FUseDumpCleaner: Boolean;
    FUsePipelineOnRestore: Boolean;
  private
    procedure DoReadConfig(const AConfigData: IConfigDataProvider);
    procedure DoWriteConfig(const AConfigData: IConfigDataWriteProvider);
  private
    { IBerkeleyUtilsConfig }
    function GetRootFolder: string;
    procedure SetRootFolder(const AValue: string);

    function GetWithSubFolders: Boolean;
    procedure SetWithSubFolders(const AValue: Boolean);

    function GetCatastrophicRecover: Boolean;
    procedure SetCatastrophicRecover(const AValue: Boolean);

    function GetRemoveEnvOnLSNReset: Boolean;
    procedure SetRemoveEnvOnLSNReset(const AValue: Boolean);

    function GetRemoveBadFileOnSuccessRestore: Boolean;
    procedure SetRemoveBadFileOnSuccessRestore(const AValue: Boolean);

    function GetDumpMode: TDumpMode;
    procedure SetDumpMode(const AValue: TDumpMode);

    function GetVerifyMode: TVerifyMode;
    procedure SetVerifyMode(const AValue: TVerifyMode);

    function GetUtilType: TUtilType;
    procedure SetUtilType(const AValue: TUtilType);

    function GetOverwriteDbConfig: Boolean;
    procedure SetOverwriteDbConfig(const AValue: Boolean);

    function GetDbConfigPresetList: IDbConfigPresetList;

    function GetUseDumpCleaner: Boolean;
    procedure SetUseDumpCleaner(const AValue: Boolean);

    function GetUsePipelineOnRestore: Boolean;
    procedure SetUsePipelineOnRestore(const AValue: Boolean);

    function CreateStatic: IBerkeleyUtilsConfigStatic;
  public
    constructor Create(const AConfigData: IConfigDataWriteProvider);
    destructor Destroy; override;
  end;

  TBerkeleyUtilsConfigStatic = class(TInterfacedObject, IBerkeleyUtilsConfigStatic)
  private
    FRootFolder: string;
    FWithSubFolders: Boolean;
    FCatastrophicRecover: Boolean;
    FRemoveEnvOnLSNReset: Boolean;
    FRemoveBadFileOnSuccessRestore: Boolean;
    FDumpMode: TDumpMode;
    FVerifyMode: TVerifyMode;
    FUtilType: TUtilType;
    FOverwriteDbConfig: Boolean;
    FDbConfigPresetList: IDbConfigPresetListStatic;
    FUseDumpCleaner: Boolean;
    FUsePipelineOnRestore: Boolean;
  private
    function GetRootFolder: string;
    function GetWithSubFolders: Boolean;
    function GetCatastrophicRecover: Boolean;
    function GetRemoveEnvOnLSNReset: Boolean;
    function GetRemoveBadFileOnSuccessRestore: Boolean;
    function GetDumpMode: TDumpMode;
    function GetVerifyMode: TVerifyMode;
    function GetUtilType: TUtilType;
    function GetOverwriteDbConfig: Boolean;
    function GetDbConfigPresetList: IDbConfigPresetListStatic;
    function GetUseDumpCleaner: Boolean;
    function GetUsePipelineOnRestore: Boolean;
  public
    constructor Create(
      const ARootFolder: string;
      const AWithSubFolders: Boolean;
      const ACatastrophicRecover: Boolean;
      const ARemoveEnvOnLSNReset: Boolean;
      const ARemoveBadFileOnSuccessRestore: Boolean;
      const ADumpMode: TDumpMode;
      const AVerifyMode: TVerifyMode;
      const AUtilType: TUtilType;
      const AOverwriteDbConfig: Boolean;
      const ADbConfigPresetList: IDbConfigPresetListStatic;
      const AUseDumpCleaner: Boolean;
      const AUsePipelineOnRestore: Boolean
    );
  end;

implementation

uses
  u_DbConfigPresetList;

{ TBerkeleyUtilsConfig }

constructor TBerkeleyUtilsConfig.Create(const AConfigData: IConfigDataWriteProvider);
begin
  inherited Create;
  FConfigData := AConfigData;
  FRootFolder := '';
  FWithSubFolders := True;
  FCatastrophicRecover := False;
  FRemoveEnvOnLSNReset := True;
  FRemoveBadFileOnSuccessRestore := True;
  FDumpMode := dmDefault;
  FVerifyMode := vmRename;
  FUtilType := utAuto;
  FUseDumpCleaner := True;
  FUsePipelineOnRestore := False;
  FOverwriteDbConfig := True;
  FDbConfigPresetList := TDbConfigPresetList.Create(AConfigData);
  DoReadConfig(FConfigData);
end;

destructor TBerkeleyUtilsConfig.Destroy;
begin
  DoWriteConfig(FConfigData);
  FDbConfigPresetList := nil;
  inherited Destroy;
end;

procedure TBerkeleyUtilsConfig.DoReadConfig(const AConfigData: IConfigDataProvider);
begin
  if AConfigData <> nil then begin
    FRootFolder := AConfigData.ReadString('RootFolder', FRootFolder);
    FWithSubFolders := AConfigData.ReadBool('WithSubFolders', FWithSubFolders);            
    FCatastrophicRecover := AConfigData.ReadBool('CatastrophicRecover', FCatastrophicRecover);
    FRemoveEnvOnLSNReset := AConfigData.ReadBool('RemoveEnvOnLSNReset', FRemoveEnvOnLSNReset);
    FRemoveBadFileOnSuccessRestore := AConfigData.ReadBool('RemoveBadFileOnSuccessRestore', FRemoveBadFileOnSuccessRestore);
    FDumpMode := TDumpMode(AConfigData.ReadInteger('DumpMode', Integer(FDumpMode)));
    FVerifyMode := TVerifyMode(AConfigData.ReadInteger('VerifyMode', Integer(FVerifyMode)));
    FUtilType := TUtilType(AConfigData.ReadInteger('UtilType', Integer(FUtilType)));
    FOverwriteDbConfig := AConfigData.ReadBool('OverwriteDbConfig', FOverwriteDbConfig);
    FUseDumpCleaner := AConfigData.ReadBool('UseDumpCleaner', FUseDumpCleaner);
    FUsePipelineOnRestore := AConfigData.ReadBool('UsePipelineOnRestore', FUsePipelineOnRestore);
  end;
end;

procedure TBerkeleyUtilsConfig.DoWriteConfig(const AConfigData: IConfigDataWriteProvider);
begin
  if AConfigData <> nil then begin
    AConfigData.WriteString('RootFolder', FRootFolder);
    AConfigData.WriteBool('WithSubFolders', FWithSubFolders);
    AConfigData.WriteBool('CatastrophicRecover', FCatastrophicRecover);
    AConfigData.WriteBool('RemoveEnvOnLSNReset', FRemoveEnvOnLSNReset);
    AConfigData.WriteBool('RemoveBadFileOnSuccessRestore', FRemoveBadFileOnSuccessRestore);
    AConfigData.WriteInteger('DumpMode', Integer(FDumpMode));
    AConfigData.WriteInteger('VerifyMode', Integer(FVerifyMode));
    AConfigData.WriteInteger('UtilType', Integer(FUtilType));
    AConfigData.WriteBool('OverwriteDbConfig', FOverwriteDbConfig);
    AConfigData.WriteBool('UseDumpCleaner', FUseDumpCleaner);
    AConfigData.WriteBool('UsePipelineOnRestore', FUsePipelineOnRestore);
  end;
end;

function TBerkeleyUtilsConfig.GetRootFolder: string;
begin
  Result := FRootFolder;
end;

procedure TBerkeleyUtilsConfig.SetRootFolder(const AValue: string);
begin
  FRootFolder := AValue;
end;

function TBerkeleyUtilsConfig.GetWithSubFolders: Boolean;
begin
  Result := FWithSubFolders;
end;

procedure TBerkeleyUtilsConfig.SetWithSubFolders(const AValue: Boolean);
begin
  FWithSubFolders := AValue;
end;

function TBerkeleyUtilsConfig.GetCatastrophicRecover: Boolean;
begin
  Result := FCatastrophicRecover;
end;

procedure TBerkeleyUtilsConfig.SetCatastrophicRecover(const AValue: Boolean);
begin
  FCatastrophicRecover := AValue;
end;

function TBerkeleyUtilsConfig.GetRemoveEnvOnLSNReset: Boolean;
begin
  Result := FRemoveEnvOnLSNReset;
end;

procedure TBerkeleyUtilsConfig.SetRemoveEnvOnLSNReset(const AValue: Boolean);
begin
  FRemoveEnvOnLSNReset := AValue;
end;

function TBerkeleyUtilsConfig.GetRemoveBadFileOnSuccessRestore: Boolean;
begin
  Result := FRemoveBadFileOnSuccessRestore;
end;

procedure TBerkeleyUtilsConfig.SetRemoveBadFileOnSuccessRestore(const AValue: Boolean);
begin
  FRemoveBadFileOnSuccessRestore := AValue;
end;

function TBerkeleyUtilsConfig.GetVerifyMode: TVerifyMode;
begin
  Result := FVerifyMode;
end;

function TBerkeleyUtilsConfig.GetDumpMode: TDumpMode;
begin
  Result := FDumpMode;
end;

procedure TBerkeleyUtilsConfig.SetDumpMode(const AValue: TDumpMode);
begin
  FDumpMode := AValue;
end;

procedure TBerkeleyUtilsConfig.SetVerifyMode(const AValue: TVerifyMode);
begin
  FVerifyMode := AValue;
end;

function TBerkeleyUtilsConfig.GetUtilType: TUtilType;
begin
  Result := FUtilType;
end;

procedure TBerkeleyUtilsConfig.SetUtilType(const AValue: TUtilType);
begin
  FUtilType := AValue;
end;

function TBerkeleyUtilsConfig.GetOverwriteDbConfig: Boolean;
begin
  Result := FOverwriteDbConfig;
end;

procedure TBerkeleyUtilsConfig.SetOverwriteDbConfig(const AValue: Boolean);
begin
  FOverwriteDbConfig := AValue;
end;

function TBerkeleyUtilsConfig.GetDbConfigPresetList: IDbConfigPresetList;
begin
  Result := FDbConfigPresetList;
end;

function TBerkeleyUtilsConfig.GetUseDumpCleaner: Boolean;
begin
  Result := FUseDumpCleaner;
end;

procedure TBerkeleyUtilsConfig.SetUseDumpCleaner(const AValue: Boolean);
begin
  FUseDumpCleaner := AValue;
end;

function TBerkeleyUtilsConfig.GetUsePipelineOnRestore: Boolean;
begin
  Result := FUsePipelineOnRestore;
end;

procedure TBerkeleyUtilsConfig.SetUsePipelineOnRestore(const AValue: Boolean);
begin
  FUsePipelineOnRestore := AValue;
end;

function TBerkeleyUtilsConfig.CreateStatic: IBerkeleyUtilsConfigStatic;
begin
  Result := TBerkeleyUtilsConfigStatic.Create(
    FRootFolder,
    FWithSubFolders,
    FCatastrophicRecover,
    FRemoveEnvOnLSNReset,
    FRemoveBadFileOnSuccessRestore,
    FDumpMode,
    FVerifyMode,
    FUtilType,
    FOverwriteDbConfig,
    FDbConfigPresetList.CreateStatic,
    FUseDumpCleaner,
    FUsePipelineOnRestore
  );
end;

{ TBerkeleyUtilsConfigStatic }

constructor TBerkeleyUtilsConfigStatic.Create(
  const ARootFolder: string;
  const AWithSubFolders: Boolean;
  const ACatastrophicRecover: Boolean;
  const ARemoveEnvOnLSNReset: Boolean;
  const ARemoveBadFileOnSuccessRestore: Boolean;
  const ADumpMode: TDumpMode;
  const AVerifyMode: TVerifyMode;
  const AUtilType: TUtilType;
  const AOverwriteDbConfig: Boolean;
  const ADbConfigPresetList: IDbConfigPresetListStatic;
  const AUseDumpCleaner: Boolean;
  const AUsePipelineOnRestore: Boolean
);
begin
  inherited Create;
  FRootFolder := ARootFolder;
  FWithSubFolders := AWithSubFolders;
  FCatastrophicRecover := ACatastrophicRecover;
  FRemoveEnvOnLSNReset := ARemoveEnvOnLSNReset;
  FRemoveBadFileOnSuccessRestore := ARemoveBadFileOnSuccessRestore;
  FDumpMode := ADumpMode;
  FVerifyMode := AVerifyMode;
  FUtilType := AUtilType;
  FOverwriteDbConfig := AOverwriteDbConfig;
  FDbConfigPresetList := ADbConfigPresetList;
  FUseDumpCleaner := AUseDumpCleaner;
  FUsePipelineOnRestore := AUsePipelineOnRestore;
end;

function TBerkeleyUtilsConfigStatic.GetRootFolder: string;
begin
  Result := FRootFolder;
end;

function TBerkeleyUtilsConfigStatic.GetWithSubFolders: Boolean;
begin
  Result := FWithSubFolders;
end;

function TBerkeleyUtilsConfigStatic.GetCatastrophicRecover: Boolean;
begin
  Result := FCatastrophicRecover;
end;

function TBerkeleyUtilsConfigStatic.GetRemoveEnvOnLSNReset: Boolean;
begin
  Result := FRemoveEnvOnLSNReset;
end;

function TBerkeleyUtilsConfigStatic.GetRemoveBadFileOnSuccessRestore: Boolean;
begin
  Result := FRemoveBadFileOnSuccessRestore;
end;

function TBerkeleyUtilsConfigStatic.GetDumpMode: TDumpMode;
begin
  Result := FDumpMode;
end;

function TBerkeleyUtilsConfigStatic.GetVerifyMode: TVerifyMode;
begin
  Result := FVerifyMode;
end;

function TBerkeleyUtilsConfigStatic.GetUtilType: TUtilType;
begin
  Result := FUtilType;
end;

function TBerkeleyUtilsConfigStatic.GetOverwriteDbConfig: Boolean;
begin
  Result := FOverwriteDbConfig;
end;

function TBerkeleyUtilsConfigStatic.GetDbConfigPresetList: IDbConfigPresetListStatic;
begin
  Result := FDbConfigPresetList;
end;

function TBerkeleyUtilsConfigStatic.GetUseDumpCleaner: Boolean;
begin
  Result := FUseDumpCleaner;
end;

function TBerkeleyUtilsConfigStatic.GetUsePipelineOnRestore: Boolean;
begin
  Result := FUsePipelineOnRestore;
end;

end.
