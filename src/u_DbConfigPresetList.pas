unit u_DbConfigPresetList;

interface

uses
  Classes,
  i_DbConfigPresetList,
  i_ConfigDataProvider,
  i_ConfigDataWriteProvider;

type
  TPresetArray = array [TDbConfigPresetType] of TStringList;

  TDbConfigPresetList = class(TInterfacedObject, IDbConfigPresetList)
  private
    FActivePreset: TDbConfigPresetType;
    FPreset: TPresetArray;
    FConfigData: IConfigDataWriteProvider;
    procedure MakeDurabilityPresets;
  private
    procedure DoReadConfig(const AConfigData: IConfigDataProvider);
    procedure DoWriteConfig(const AConfigData: IConfigDataWriteProvider);
  private
    { IDbConfigPresetList }
    function GetPresetItem(const AType: TDbConfigPresetType): string;
    procedure SetPresetItem(const AType: TDbConfigPresetType; const AValue: string);
    function GetActivePreset: TDbConfigPresetType;
    procedure SetActivePreset(const AValue: TDbConfigPresetType);
    function CreateStatic: IDbConfigPresetListStatic;
  public
    constructor Create(const AConfigData: IConfigDataWriteProvider);
    destructor Destroy; override;
  end;

  TDbConfigPresetListStatic = class(TInterfacedObject, IDbConfigPresetListStatic)
  private
    FActivePreset: TDbConfigPresetType;
    FPreset: array [TDbConfigPresetType] of string;
  private
    { IDbConfigPresetListStatic }
    function GetPresetItem(const AType: TDbConfigPresetType): string;
    function GetActivePreset: TDbConfigPresetType;
  public
    constructor Create(
      const AActivePreset: TDbConfigPresetType;
      const APreset: TPresetArray
    );
  end;

implementation

uses
  SysUtils;

{ TDbConfigPresetList }

constructor TDbConfigPresetList.Create(const AConfigData: IConfigDataWriteProvider);
var
  I: TDbConfigPresetType;
begin
  inherited Create;
  FConfigData := AConfigData;
  for I := Low(FPreset) to High(FPreset) do begin
    FPreset[I] := TStringList.Create;
  end;
  MakeDurabilityPresets;
  DoReadConfig(FConfigData);
end;

destructor TDbConfigPresetList.Destroy;
var
  I: TDbConfigPresetType;
begin
  DoWriteConfig(FConfigData);
  for I := Low(FPreset) to High(FPreset) do begin
    FPreset[I].Free;
  end;
  inherited Destroy;
end;

procedure TDbConfigPresetList.DoReadConfig(const AConfigData: IConfigDataProvider);
var
  I: Integer;
  VLine: string;
  VLinesCount: Integer;
  VConfigData: IConfigDataProvider;
begin
  if AConfigData <> nil then begin
    FActivePreset := TDbConfigPresetType(AConfigData.ReadInteger('DbConfigPreset', Integer(dbcpHighDurability)));
    FPreset[dbcpUserDefined].Clear;
    VConfigData := AConfigData.GetSubItem('UserDbConfig');
    if Assigned(VConfigData) then begin
      VLinesCount := VConfigData.ReadInteger('Lines', 0);
      for I := 0 to VLinesCount - 1 do begin
        VLine := VConfigData.ReadString('Line_' + IntToStr(I), '');
        if VLine <> '' then begin
          FPreset[dbcpUserDefined].Add(VLine);
        end;
      end;
    end;
  end;
end;

procedure TDbConfigPresetList.DoWriteConfig(const AConfigData: IConfigDataWriteProvider);
var
  I: Integer;
  VConfigData: IConfigDataWriteProvider;
begin
  if AConfigData <> nil then begin
    AConfigData.WriteInteger('DbConfigPreset', Integer(FActivePreset));
    VConfigData := AConfigData.GetOrCreateSubItem('UserDbConfig');
    VConfigData.DeleteValues;
    VConfigData.WriteInteger('Lines', FPreset[dbcpUserDefined].Count);
    for I := 0 to FPreset[dbcpUserDefined].Count - 1 do begin
      VConfigData.WriteString('Line_' + IntToStr(I), FPreset[dbcpUserDefined].Strings[I]);
    end;
  end;
end;

procedure TDbConfigPresetList.MakeDurabilityPresets;

  procedure _AddCommonSettings(const AList: TStringList);
  begin
    AList.Add('set_lg_dir .');
    AList.Add('set_data_dir ..');
    AList.Add('log_set_config DB_LOG_AUTO_REMOVE on');
    AList.Add('set_cachesize 0 2097152 1');
    AList.Add('set_lg_max 10485760');
    AList.Add('set_lg_bsize 2097152');
  end;

  procedure _MakeLowDurability(const AList: TStringList);
  begin
    AList.Add('set_flags DB_TXN_NOSYNC on');
    AList.Add('set_flags DB_TXN_WRITE_NOSYNC on');
    AList.Add('set_flags DB_DIRECT_DB off');
    AList.Add('set_flags DB_DSYNC_DB off');
    AList.Add('log_set_config DB_LOG_DIRECT off');
    AList.Add('log_set_config DB_LOG_DSYNC off');
    _AddCommonSettings(AList);
  end;

  procedure _MakeNormalDurability(const AList: TStringList);
  begin
    AList.Add('set_flags DB_TXN_NOSYNC off');
    AList.Add('set_flags DB_TXN_WRITE_NOSYNC on');
    AList.Add('set_flags DB_DIRECT_DB off');
    AList.Add('set_flags DB_DSYNC_DB off');
    AList.Add('log_set_config DB_LOG_DIRECT off');
    AList.Add('log_set_config DB_LOG_DSYNC off');
    _AddCommonSettings(AList);
  end;

  procedure _MakeHighDurability(const AList: TStringList);
  begin
    AList.Add('set_flags DB_TXN_NOSYNC off');
    AList.Add('set_flags DB_TXN_WRITE_NOSYNC off');
    AList.Add('set_flags DB_DIRECT_DB off');
    AList.Add('set_flags DB_DSYNC_DB off');
    AList.Add('log_set_config DB_LOG_DIRECT off');
    AList.Add('log_set_config DB_LOG_DSYNC off');
    _AddCommonSettings(AList);
  end;

  procedure _MakeExtraHighDurability(const AList: TStringList);
  begin
    AList.Add('set_flags DB_TXN_NOSYNC off');
    AList.Add('set_flags DB_TXN_WRITE_NOSYNC off');
    AList.Add('set_flags DB_DIRECT_DB on');
    AList.Add('set_flags DB_DSYNC_DB on');
    AList.Add('log_set_config DB_LOG_DIRECT on');
    AList.Add('log_set_config DB_LOG_DSYNC on');
    _AddCommonSettings(AList);
  end;

begin
   _MakeLowDurability(FPreset[dbcpLowDurability]);
   _MakeNormalDurability(FPreset[dbcpNormalDurability]);
   _MakeHighDurability(FPreset[dbcpHighDurability]);
   _MakeExtraHighDurability(FPreset[dbcpExtraHighDurability]);
end;

function TDbConfigPresetList.GetPresetItem(const AType: TDbConfigPresetType): string;
begin
  Result := FPreset[AType].Text;
end;

procedure TDbConfigPresetList.SetPresetItem(const AType: TDbConfigPresetType; const AValue: string);
begin
  FPreset[AType].Text := AValue;
end;

function TDbConfigPresetList.GetActivePreset: TDbConfigPresetType;
begin
  Result := FActivePreset;
end;

procedure TDbConfigPresetList.SetActivePreset(const AValue: TDbConfigPresetType);
begin
  FActivePreset := AValue;
end;

function TDbConfigPresetList.CreateStatic: IDbConfigPresetListStatic;
begin
  Result := TDbConfigPresetListStatic.Create(FActivePreset, FPreset);
end;

{ TDbConfigPresetListStatic }

constructor TDbConfigPresetListStatic.Create(
  const AActivePreset: TDbConfigPresetType;
  const APreset: TPresetArray
);
var
  I: TDbConfigPresetType;
begin
  inherited Create;
  FActivePreset := AActivePreset;
  for I := Low(FPreset) to High(FPreset) do begin
    FPreset[I] := APreset[I].Text;
  end;
end;

function TDbConfigPresetListStatic.GetPresetItem(const AType: TDbConfigPresetType): string;
begin
  Result := FPreset[AType];
end;

function TDbConfigPresetListStatic.GetActivePreset: TDbConfigPresetType;
begin
  Result := FActivePreset;
end;  

end.
