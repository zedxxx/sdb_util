unit u_BerkeleyUtils;

interface

uses
  Classes,
  i_BerkeleyUtils,
  i_BerkeleyUtilsConfig,
  i_ConsoleAppRunner;

type
  TOnLongTimeOperation = procedure(
    Sender: TObject;
    const AOperationName: string;
    const AIsOperationFin: Boolean
  ) of object;

  TBerkeleyUtils = class(TInterfacedObject, IBerkeleyUtils)
  private
    FOnOutputString: TOnOutputString;
    FOnProcessMessages: TOnProcessMessages;
    FOnLongTimeOperation: TOnLongTimeOperation;
    FAbort: Boolean;
    FProcessedOk: UInt64;
    FProcessedFail: UInt64;
    FSkipped: UInt64;
    FSize: Int64;
    FUserData: Pointer;
    FDbConfig: TStringList;
  private
    function OnProcessMessages(const AUserData: Pointer): Boolean;
    function PrepareDbConfig(const AEnvHome: string; const AConfig: IBerkeleyUtilsConfigStatic): Boolean;
    function Recover(const AConfig: IBerkeleyUtilsConfigStatic): UInt64;
    function Verify(const AConfig: IBerkeleyUtilsConfigStatic): UInt64;
    function ResetLSN(const AConfig: IBerkeleyUtilsConfigStatic): UInt64;
    function Restore(const AConfig: IBerkeleyUtilsConfigStatic): UInt64;
    function RestoreFile(const AFileName: string; const AConfig: IBerkeleyUtilsConfigStatic): Cardinal;
    function RestoreFilePipeline(const AFileName: string; const AConfig: IBerkeleyUtilsConfigStatic): Cardinal;

    function db_recover(const AEnvHome: string; const ACatastrophic: Boolean): Cardinal;
    function db_verify(const AFileName: string; const AFileSize: Int64): Cardinal;
    function db_load(const AFileName: string): Cardinal; overload;
    function db_load(const ADumpFileName: string; const AOutputFileName: string): Cardinal; overload;
    function db_dump(const ADumpFileName: string; const AOutputFileName: string; const AMode: TDumpMode): Cardinal;

    function sdb_dump_cleaner(const ADumpFileName: string): Boolean;

    function GetConsole: IConsoleAppRunner;
    property Console: IConsoleAppRunner read GetConsole;
  private
    { IBerkeleyUtils }
    function Run(const AConfig: IBerkeleyUtilsConfigStatic): UInt64;
    function GetProcessedOk: UInt64;
    function GetProcessedFail: UInt64;
    function GetSkipped: UInt64;
  public
    constructor Create(
      const AOnOutputString: TOnOutputString;
      const AOnProcessMessages: TOnProcessMessages;
      const AOnLongTimeOperation: TOnLongTimeOperation
    );
    destructor Destroy; override;
  end;

implementation

uses
  Windows,
  SysUtils,
  Dialogs,
  Controls,
  t_BerkeleyDB,
  c_BerkeleyUtils,
  i_FileNameIterator,
  i_BerkeleyDBVerifyCache,
  u_Tools,
  u_ResourceStrings,
  u_TreeFolderRemover,
  u_ConsoleAppRunner,
  u_BerkeleyDBTools,
  u_BerkeleyDBVerifyCache,
  u_FileNameIteratorHelper;

const
  cUndefResult = $BADBAD;
  cBigFileSize = 25 * 1024 * 1024; // 25Mb
  cDbConfigFileName = 'DB_CONFIG';
  cCRLF = #13#10#13#10;

function IsConsoleProcessAborted(const AExitCode: Cardinal): Boolean; inline;
begin
  Result := (AExitCode = cConsoleAppAbortedByUser) or (AExitCode = cConsoleAppError);
end;

{ TBerkeleyUtils }

constructor TBerkeleyUtils.Create(
  const AOnOutputString: TOnOutputString;
  const AOnProcessMessages: TOnProcessMessages;
  const AOnLongTimeOperation: TOnLongTimeOperation
);
begin
  inherited Create;
  FAbort := False;
  FProcessedOk := 0;
  FProcessedFail := 0;
  FSkipped := 0;
  FSize := 0;
  FUserData := nil;
  FOnOutputString := AOnOutputString;
  FOnProcessMessages := AOnProcessMessages;
  FOnLongTimeOperation := AOnLongTimeOperation;
  FDbConfig := TStringList.Create;
end;

destructor TBerkeleyUtils.Destroy;
begin
  FDbConfig.Free;
  inherited Destroy;
end;

function UtilTypeToStr(const AUtilType: TUtilType): string;
begin
  case AUtilType of
    utAuto: Result := 'Auto (Recover + Verify)';
    utRecover: Result := 'Recover';
    utResetLSN: Result := 'ResetLSN';
    utVerify: Result := 'Verify';
    utRestore: Result := 'Restore';
  else
    begin
      Result := 'Unknown';
      Assert(False, 'Unknown UtilType value: ' + IntToStr(Integer(AUtilType)));
    end;
  end;
end;

function TBerkeleyUtils.Run(const AConfig: IBerkeleyUtilsConfigStatic): UInt64;
var
  VCount: UInt64;
  VInfoStr: string;
  VDateStr: string;
  VUtilType: string;
begin
  FAbort := False;
  FProcessedOk := 0;
  FProcessedFail := 0;
  FSkipped := 0;
  FSize := 0;
  OnProcessMessages(Self);

  VDateStr := FormatDateTime('yyyy-mm-dd hh:mm:ss.zzz', Now);
  VUtilType := UtilTypeToStr(AConfig.UtilType);

  FOnOutputString(
    Format('[+] %s [%s]: %s', [VDateStr, VUtilType, AConfig.RootFolder + cCRLF]),
    FUserData
  );

  case AConfig.UtilType of
    utRecover: Result := Recover(AConfig);
    utResetLSN: Result := ResetLSN(AConfig);
    utVerify: Result := Verify(AConfig);
    utRestore: Result := Restore(AConfig);
  else // utAuto
    begin
      VCount := Recover(AConfig);
      Result := VCount;
      VCount := Verify(AConfig);
      Result := Result + VCount;
    end;
  end;

  if Result > 0 then begin
    VInfoStr := Format('%s: %d', [rsTotalProcessed, Result]);
    if FSize > 0 then begin
      VInfoStr := VInfoStr + ' (' + DataSizeToStr(FSize) + ')';
    end;
  end else begin
    VInfoStr := rsNothingToDo;
  end;

  FOnOutputString(VInfoStr + cCRLF, FUserData);

  VDateStr := FormatDateTime('yyyy-mm-dd hh:mm:ss.zzz', Now);
  FOnOutputString(
    Format('[-] %s [%s]: %s', [VDateStr, VUtilType, AConfig.RootFolder + cCRLF]),
    FUserData
  );
end;

function TBerkeleyUtils.GetProcessedOk: UInt64;
begin
  Result := FProcessedOk;
end;

function TBerkeleyUtils.GetProcessedFail: UInt64;
begin
  Result := FProcessedFail;
end;

function TBerkeleyUtils.GetSkipped: UInt64;
begin
  Result := FSkipped;
end;

function TBerkeleyUtils.GetConsole: IConsoleAppRunner;
begin
  Result := TConsoleAppRunner.Create(False, FOnOutputString, Self.OnProcessMessages);
end;

function TBerkeleyUtils.OnProcessMessages(const AUserData: Pointer): Boolean;
begin
  Result := FOnProcessMessages(AUserData);
  FAbort := not Result;
end;

function TBerkeleyUtils.PrepareDbConfig(
  const AEnvHome: string;
  const AConfig: IBerkeleyUtilsConfigStatic
): Boolean;
var
  VExists: Boolean;
  VConfigFile: string;
begin
  VConfigFile := IncludeTrailingPathDelimiter(AEnvHome) + cDbConfigFileName;
  VExists := FileExists(VConfigFile);
  if VExists and AConfig.OverwriteDbConfig then begin
    VExists := not DeleteFile(VConfigFile);
    if VExists then begin
      FOnOutputString(rsCantDeleteDbConfig + ': ' + VConfigFile + cCRLF, FUserData);
      Result := False;
      Exit;
    end;
  end;
  if not VExists then begin
    FDbConfig.SaveToFile(VConfigFile);
  end;
  Result := True;
end;

function TBerkeleyUtils.Recover(const AConfig: IBerkeleyUtilsConfigStatic): UInt64;
var
  VEnv: string;
  VResult: Cardinal;
  VFileName: string;
  VFilesIterator: IFileNameIterator;
begin
  FAbort := False;
  Result := 0;

  VFilesIterator := CreateFileNameIterator(
    AConfig.RootFolder,
    ['env'],
    False,
    AConfig.WithSubFolders
  );

  FDbConfig.Clear;
  FDbConfig.Text := AConfig.DbConfigPresetList.Item[AConfig.DbConfigPresetList.Preset];

  while VFilesIterator.Next(VFileName) do begin
    VEnv := IncludeTrailingPathDelimiter(AConfig.RootFolder) +
      IncludeTrailingPathDelimiter(VFileName);
    if IsBerkeleyDBEnvFolder(VEnv) then begin
      if not PrepareDbConfig(VEnv, AConfig) then begin
        FAbort := True;
      end else begin
        Inc(Result);
        VResult := db_recover(VEnv, AConfig.CatastrophicRecover);
        if IsConsoleProcessAborted(VResult) then begin
          FAbort := True;
        end else if VResult = 0 then begin
          Inc(FProcessedOk);
        end else begin
          Inc(FProcessedFail);
        end;
      end;
    end;
    if FAbort then begin
      Break;
    end;
  end;
end;

function TBerkeleyUtils.Verify(const AConfig: IBerkeleyUtilsConfigStatic): UInt64;
var
  VFileName: string;
  VResult: Cardinal;
  VFilesIterator: IFileNameIterator;
  VOldBadFileName: string;
  VCache: IBerkeleyDBVerifyCache;
  VRecID: TRecID;
  VStatus: TBerkeleyDBVerifyStatus;
  VIsChanged: Boolean;
  VDoVerify: Boolean;
  VDoUpdate: Boolean;
  VSize: Int64;
  VFileInfo: TFileInfoRec;
begin
  FAbort := False;
  Result := 0;

  VCache := TBerkeleyDBVerifyCache.Create('db_verify.db3', FOnOutputString);

  if not VCache.IsAvailable then begin
    if MessageDlg(rsVerifyCacheNotAvailable, mtError, [mbIgnore, mbAbort], 0) = mrAbort then begin
      FAbort := True;
      Exit;
    end;
  end;

  VFilesIterator := CreateFileNameIterator(
    AConfig.RootFolder,
    ['*.*'],
    True,
    AConfig.WithSubFolders
  );

  while VFilesIterator.Next(VFileName) do begin

    if not OnProcessMessages(FUserData) then begin
      Break;
    end;

    VFileName := IncludeTrailingPathDelimiter(AConfig.RootFolder) + VFileName;
    if IsBerkeleyDBCacheFile(VFileName) then begin
      Inc(Result);

      VRecID := 0;
      VResult := cUndefResult;
      VDoVerify := True;
      VDoUpdate := True;
      VSize := 0;
      
      ZeroMemory(@VFileInfo, SizeOf(TFileInfoRec));

      if VCache.Read(VFileName, VIsChanged, VFileInfo) then begin
        VRecID := VFileInfo.FID;
        VStatus := VFileInfo.FVerifyStatus;
        VResult := VFileInfo.FVerifyResult;
        Assert(VRecID <> 0);
        if not VIsChanged then begin
          VSize := VFileInfo.FSize;
          case VStatus of
            vsOk: begin
              Inc(FSkipped);
              Inc(FSize, VSize);
              Continue;
            end;
            vsBusy: begin
              // last check was interrupted by unknown reason
              VDoVerify := False;
              Assert(VResult = cUndefResult);
            end;
            vsError: begin
              VDoVerify := False;
              VDoUpdate := False;
              Assert(VResult <> 0);
              Assert(not IsConsoleProcessAborted(VResult));
            end;
          end;
        end;
      end;

      if VSize = 0 then begin
        VSize := GetFileSize(VFileName);
      end;
      Inc(FSize, VSize);

      if VDoVerify then begin
        if VRecID <> 0 then begin
          VCache.UpdateFull(VFileName, VRecID, vsBusy, cUndefResult);
        end else begin
          VRecID := VCache.Add(VFileName, vsBusy, cUndefResult);
        end;
        VResult := db_verify(VFileName, VSize);
      end;

      if IsConsoleProcessAborted(VResult) then begin
        FAbort := True;
        VCache.Update(VRecID, vsAborted, VResult);
      end else if VResult = 0 then begin
        Inc(FProcessedOk);
        VCache.Update(VRecID, vsOK, VResult);
      end else begin
        Inc(FProcessedFail);
        if VDoUpdate then begin
          VCache.Update(VRecID, vsError, VResult);
        end;
        VOldBadFileName := ChangeFileExt(VFileName, '.bad');
        case AConfig.VerifyMode of
          vmRename:
            begin
              if FileExists(VOldBadFileName) then begin
                DeleteFile(VOldBadFileName);
              end;
              if RenameFile(VFileName, VFileName + '.bad') then begin
                VCache.Delete(VRecID);
                FOnOutputString(rsBrokenFileIsRenamed + ': ' + VFileName + '.bad' + cCRLF, FUserData);
              end else begin
                FOnOutputString(rsCantRenameBrokenFile + ': ' + VFileName + cCRLF, FUserData);
              end;
            end;
          vmRemove:
            begin
              if DeleteFile(VFileName) then begin
                VCache.Delete(VRecID);
                FOnOutputString(rsBrokenFileIsDeleted + ': ' + VFileName + cCRLF, FUserData);
              end else begin
                FOnOutputString(rsCantDeleteBrokenFile + ': ' + VFileName + cCRLF, FUserData);
              end;
            end;
          vmRestore:
            begin
              if FileExists(VOldBadFileName) then begin
                DeleteFile(VOldBadFileName);
              end;
              if RenameFile(VFileName, VFileName + '.bad') then begin
                VCache.Delete(VRecID);
                VResult := RestoreFile(VFileName + '.bad', AConfig);
                if IsConsoleProcessAborted(VResult) then begin
                  FAbort := True;
                end else if VResult = 0 then begin
                  // error is fixed
                  Inc(FProcessedOk);
                  Dec(FProcessedFail);
                  VCache.Add(VFileName, vsOK, 0);
                end;
              end else begin
                FOnOutputString(rsCantRenameBrokenFile + ': ' + VFileName + cCRLF, FUserData);
              end;
            end;
        else // vmReport
          begin
            // do nothing
          end;
        end;
      end;
    end;
    if FAbort then begin
      Break;
    end;
  end;
end;

function TBerkeleyUtils.ResetLSN(const AConfig: IBerkeleyUtilsConfigStatic): UInt64;
var
  VResult: Cardinal;
  VFileName: string;
  VFilesIterator: IFileNameIterator;
  VEnvList: TStringList;
  I: Integer;
begin
  FAbort := False;
  Result := 0;

  VEnvList := TStringList.Create;
  try
    VFilesIterator := CreateFileNameIterator(
      AConfig.RootFolder,
      ['*.*'],
      False,
      AConfig.WithSubFolders
    );

    while VFilesIterator.Next(VFileName) do begin
      VFileName := IncludeTrailingPathDelimiter(AConfig.RootFolder) + VFileName;
      if IsBerkeleyDBEnvFolder(VFileName) then begin
        if AConfig.RemoveEnvOnLSNReset then begin
          VEnvList.Add(VFileName);
        end;
      end else if IsBerkeleyDBCacheFile(VFileName) then begin
        Inc(Result);
        VResult := db_load(VFileName);
        if IsConsoleProcessAborted(VResult) then begin
          FAbort := True;
        end else if VResult = 0 then begin
          Inc(FProcessedOk);
        end else begin
          Inc(FProcessedFail);
        end;
      end;
      if FAbort then begin
        Break;
      end;
    end;
    if not FAbort and AConfig.RemoveEnvOnLSNReset then begin
      for I := 0 to VEnvList.Count - 1 do begin
        if FullRemoveDir(VEnvList.Strings[I], True, False, True) then begin
          FOnOutputString(rsEnvIsDeleted + ': ' + VEnvList.Strings[I] + cCRLF, FUserData);
        end else begin
          FOnOutputString(rsCantDeleteEnv + ': ' + VEnvList.Strings[I] + cCRLF, FUserData);
        end;
      end;
    end;
  finally
    VEnvList.Free;
  end;
end;

function TBerkeleyUtils.Restore(const AConfig: IBerkeleyUtilsConfigStatic): UInt64;
var
  VResult: Cardinal;
  VFileName: string;
  VFilesIterator: IFileNameIterator;
begin
  FAbort := False;
  Result := 0;

  VFilesIterator := CreateFileNameIterator(
    AConfig.RootFolder,
    ['*.bad'],
    True,
    AConfig.WithSubFolders
  );

  while VFilesIterator.Next(VFileName) do begin
    VFileName := IncludeTrailingPathDelimiter(AConfig.RootFolder) + VFileName;
    if FileExists(VFileName) then begin
      Inc(Result);
      VResult := RestoreFile(VFileName, AConfig);
      if IsConsoleProcessAborted(VResult) then begin
        FAbort := True;
      end else if VResult = 0 then begin
        Inc(FProcessedOk);
      end else begin
        Inc(FProcessedFail);
      end;
    end;
    if FAbort then begin
      Break;
    end;
  end;
end;

function TBerkeleyUtils.RestoreFile(
  const AFileName: string;
  const AConfig: IBerkeleyUtilsConfigStatic
): Cardinal;
var
  VFile: string;
  VDump: string;
begin
  if AConfig.UsePipelineOnRestore then begin
    Result := RestoreFilePipeline(AFileName, AConfig);
    Exit;
  end;

  VFile := AFileName; // xxx.yyy.sdb.bad or xxx.yyy.tne.bad
  VDump := ChangeFileExt(VFile, '.dump'); // xxx.yyy.sdb.dump

  if FileExists(VDump) then begin
    if not DeleteFile(VDump) then begin
      FOnOutputString(rsCantDeleteDumpFile + ': ' + VDump + cCRLF, FUserData);
      Result := cUndefResult;
      Exit;
    end;
  end;

  Result := db_dump(VDump, VFile, AConfig.DumpMode);

  if (Result = 0) or ((AConfig.DumpMode <> dmDefault) and FileExists(VDump)) then begin
    VFile := ChangeFileExt(VFile, ''); // xxx.yyy.sdb
    if FileExists(VFile) then begin
      if not DeleteFile(VFile) then begin
        FOnOutputString(rsCantDeleteBrokenFile + ': ' + VFile + cCRLF, FUserData);
        Result := cUndefResult;
        Exit;
      end;
    end;

    if AConfig.UseDumpCleaner then begin
      sdb_dump_cleaner(VDump);
    end;

    Result := db_load(VDump, VFile);
    if Result = 0 then begin
      Result := db_verify(VFile, GetFileSize(VFile));
    end;

    if Result = 0 then begin
      // ok
      if AConfig.RemoveBadFileOnSuccessRestore then begin
        DeleteFile(AFileName); // xxx.yyy.sdb.bad
      end;
    end else begin
      // restore failure
      DeleteFile(VFile); // xxx.yyy.sdb
    end;
  end;

  DeleteFile(VDump); // xxx.yyy.sdb.dump
end;

function TBerkeleyUtils.RestoreFilePipeline(
  const AFileName: string;
  const AConfig: IBerkeleyUtilsConfigStatic
): Cardinal;
var
  VAppDir: string;
  VOutFile: string;
  VCmdFile: string;
  VCmdText: AnsiString;
  VDumpOpt: AnsiString;
  VLoadOpt: AnsiString;
  VCleaner: AnsiString;
  VMemStream: TMemoryStream;
begin
  VOutFile := ChangeFileExt(AFileName, ''); // xxx.yyy.sdb

  VAppDir := ExtractFilePath(ParamStr(0));

  VCmdFile := VAppDir + 'sdb_restore.bat';

  case AConfig.DumpMode of
    dmSalvage: VDumpOpt := '-r ';
    dmAggressiveSalvage: VDumpOpt := '-R '
  else
    VDumpOpt := '';
  end;

  if VDumpOpt <> '' then begin
    VLoadOpt := '-c db_pagesize=1024 ';
  end else begin
    VLoadOpt := '';
  end;

  if AConfig.UseDumpCleaner then begin
    VCleaner := sdb_dump_cleaner_exe + ' | ';
  end else begin
    VCleaner := '';
  end;

  VCmdText :=
    '@echo off' + #13#10 +
    'set FIN=' + ExtractFileName(AFileName) + #13#10 + // xxx.yyy.sdb.bad
    'set FOUT=' + ExtractFileName(VOutFile) + #13#10 +
    'db_dump.exe ' + VDumpOpt + '%FIN% | ' + VCleaner + 'db_load.exe ' + VLoadOpt + '%FOUT%' + #13#10;

  FOnOutputString('Writing to ' + VCmdFile + ':' + cCRLF, FUserData);
  FOnOutputString(VCmdText + cCRLF, FUserData);

  VMemStream := TMemoryStream.Create;
  try
    VMemStream.WriteBuffer(VCmdText[1], Length(VCmdText));
    VMemStream.SaveToFile(VCmdFile);
  finally
    VMemStream.Free;
  end;

  if Assigned(FOnLongTimeOperation) then begin
    FOnLongTimeOperation(Self, rsCreateDump, False);
  end;
  try
    Console.TerminateOnAbort := True;

    Result := Console.Run(
      VCmdFile,
      '',
      ExtractFilePath(AFileName),
      'PATH=' + VAppDir + #0
    );

    if Result = 0 then begin
      Result := db_verify(VOutFile, GetFileSize(VOutFile));
    end;

    if Result = 0 then begin
      // ok
      if AConfig.RemoveBadFileOnSuccessRestore then begin
        DeleteFile(AFileName); // xxx.yyy.sdb.bad
      end;
    end else begin
      // restore failure
      DeleteFile(VOutFile); // xxx.yyy.sdb
    end;
  finally
    if Assigned(FOnLongTimeOperation) then begin
      FOnLongTimeOperation(Self, '', True);
    end;
  end;
end;

function TBerkeleyUtils.db_recover(
  const AEnvHome: string;
  const ACatastrophic: Boolean
): Cardinal;
var
  VCatastrophic: string;
begin
  if ACatastrophic then begin
    VCatastrophic := '-c ';
  end else begin
    VCatastrophic := '';
  end;
  Console.TerminateOnAbort := False;
  Result := Console.Run(
    'db_recover.exe',
    VCatastrophic + '-v',
    AEnvHome
  );
end;

function TBerkeleyUtils.db_verify(
  const AFileName: string;
  const AFileSize: Int64
): Cardinal;
var
  VLongTimeOperation: Boolean;
begin
  VLongTimeOperation := AFileSize > cBigFileSize;
  if VLongTimeOperation and Assigned(FOnLongTimeOperation) then begin
    FOnLongTimeOperation(
      Self,
      Format('%s %s (%s)', [rsVerify, ExtractFileName(AFileName), DataSizeToStr(AFileSize)]),
      False
    );
  end;
  try
    Console.TerminateOnAbort := True;
    Result := Console.Run(
      'db_verify.exe',
      ExtractFileName(AFileName),
      ExtractFilePath(AFileName)
    );
  finally
    if VLongTimeOperation and Assigned(FOnLongTimeOperation) then begin
      FOnLongTimeOperation(Self, '', True);
    end;
  end;
end;

function TBerkeleyUtils.db_load(const AFileName: string): Cardinal;
var
  VLongTimeOperation: Boolean;
begin
  VLongTimeOperation := GetFileSize(AFileName) > cBigFileSize;
  if VLongTimeOperation and Assigned(FOnLongTimeOperation) then begin
    FOnLongTimeOperation(Self, rsResetLSN + ' ' + ExtractFileName(AFileName), False);
  end;
  try
    Console.TerminateOnAbort := False;
    Result := Console.Run(
      'db_load.exe',
      '-r lsn ' + ExtractFileName(AFileName),
      ExtractFilePath(AFileName)
    );
  finally
    if VLongTimeOperation and Assigned(FOnLongTimeOperation) then begin
      FOnLongTimeOperation(Self, '', True);
    end;
  end;
end;

function TBerkeleyUtils.db_load(const ADumpFileName: string; const AOutputFileName: string): Cardinal;
begin
  if Assigned(FOnLongTimeOperation) then begin
    FOnLongTimeOperation(Self, rsRestoreDataFromDump, False);
  end;
  try
    Console.TerminateOnAbort := True;
    Result := Console.Run(
      'db_load.exe',
      '-f ' + ExtractFileName(ADumpFileName) + ' ' + ExtractFileName(AOutputFileName),
      ExtractFilePath(ADumpFileName)
    );
  finally
    if Assigned(FOnLongTimeOperation) then begin
      FOnLongTimeOperation(Self, '', True);
    end;
  end;
end;

function TBerkeleyUtils.db_dump(const ADumpFileName: string; const AOutputFileName: string; const AMode: TDumpMode): Cardinal;
var
  VSalvage: string;
begin
  if Assigned(FOnLongTimeOperation) then begin
    FOnLongTimeOperation(Self, rsCreateDump, False);
  end;
  try
    case AMode of
      dmSalvage: VSalvage := '-r ';
      dmAggressiveSalvage: VSalvage := '-R '
    else
      VSalvage := '';
    end;
    Console.TerminateOnAbort := True;
    Result := Console.Run(
      'db_dump.exe',
      VSalvage + '-f ' + ExtractFileName(ADumpFileName) + ' ' + ExtractFileName(AOutputFileName),
      ExtractFilePath(ADumpFileName)
    );
  finally
    if Assigned(FOnLongTimeOperation) then begin
      FOnLongTimeOperation(Self, '', True);
    end;
  end;
end;

function TBerkeleyUtils.sdb_dump_cleaner(const ADumpFileName: string): Boolean;
var
  VRetCode: Cardinal;
  VCleanedDump: string;
begin
  Result := False;
  if Assigned(FOnLongTimeOperation) then begin
    FOnLongTimeOperation(Self, rsCreateDump, False);
  end;
  try
    VCleanedDump := ADumpFileName + '.clean';
    Console.TerminateOnAbort := True;
    VRetCode := Console.Run(
      sdb_dump_cleaner_exe,
      '-fi ' + ExtractFileName(ADumpFileName) + ' -fo ' + ExtractFileName(VCleanedDump),
      ExtractFilePath(ADumpFileName)
    );
    if (VRetCode = 0) and FileExists(VCleanedDump) then begin
      if DeleteFile(ADumpFileName) then begin
        if not RenameFile(VCleanedDump, ADumpFileName) then begin
          RaiseLastOSError;
        end;
      end else begin
        RaiseLastOSError;
      end;
      Result := True;
    end;
  finally
    if Assigned(FOnLongTimeOperation) then begin
      FOnLongTimeOperation(Self, '', True);
    end;
  end;
end;

end.
