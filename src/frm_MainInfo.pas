unit frm_MainInfo;

interface

uses
  Windows,
  Messages,
  Dialogs,
  SysUtils,
  Variants,
  Classes,
  Graphics,
  Controls,
  Forms,
  StdCtrls,
  ExtCtrls,
  ComCtrls,
  VirtualTrees,
  i_FormInfoConfig,
  i_ConfigDataWriteProvider;

type
  TStatRec = record
    FSize: Int64;
    FCountOk: Int64;
    FCountUnk: Int64;
    FCountErr: Int64;
    FCountBad: Int64;
    FCountTotal: Int64;
  end;

  TfrmMainInfo = class(TForm)
    btnRun: TButton;
    pnlVT: TPanel;
    edtPath: TEdit;
    statBottom: TStatusBar;
    tmrProgressCheck: TTimer;
    chkShowUnk: TCheckBox;
    chkShowOk: TCheckBox;
    btnExit: TButton;
    lblRootCachePath: TLabel;
    btnSelectCachePath: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnRunClick(Sender: TObject);
    procedure tmrProgressCheckTimer(Sender: TObject);
    procedure btnExitClick(Sender: TObject);
    procedure btnSelectCachePathClick(Sender: TObject);
    procedure edtPathChange(Sender: TObject);
    procedure OnShowOptClick(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
  private
    FVirtualTree: TVirtualStringTree;
    FStat: TStatRec;
    FTreeColors: TColorsRec;
    FFormConfig: IFormInfoConfig;
    FConfigProvider: IConfigDataWriteProvider;
    procedure OnVTFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
    procedure OnVTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
      Column: TColumnIndex; TextType: TVSTTextType; var CellText: UnicodeString);
    procedure OnVTBeforeCellPaint(Sender: TBaseVirtualTree;
      TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
      CellPaintMode: TVTCellPaintMode; CellRect: TRect; var ContentRect: TRect);
    procedure OnVTCompareNodes(Sender: TBaseVirtualTree; Node1,
      Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
    procedure OnVTHeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
  end;

var
  frmMainInfo: TfrmMainInfo;

implementation

uses
  {$WARN UNIT_PLATFORM OFF}
  FileCtrl,
  {$WARN UNIT_PLATFORM ON}
  IniFiles,
  DateUtils,
  ExplorerSort,
  t_BerkeleyDB,
  i_BerkeleyDBVerifyCache,
  i_FileNameIterator,
  u_FormInfoConfig,
  u_FileNameIteratorHelper,
  u_BerkeleyDBVerifyCache,
  u_ResourceStrings,
  u_BerkeleyDBTools,
  u_Tools,
  u_ConfigDataWriteProviderByIniFile;

{$R *.dfm}

type
  TTreeNodeRec = record
    FileName: string;
    FileSize: Int64;
    IsChanged: Boolean;
    VerifyStatus: TBerkeleyDBVerifyStatus;
    VerifyTime: TDateTime;
  end;
  PTreeNodeRec = ^TTreeNodeRec;

procedure TfrmMainInfo.FormCreate(Sender: TObject);
var
  I: Integer;
  VTreeColors: TColorsRec;
  VTreeColumnsState: TTreeColumnsState;
  VTreeShowOpt: TTreeShowOptRec;
begin
  FConfigProvider := TConfigDataWriteProviderByIniFile.Create(
    TMemIniFile.Create('sdb_util.ini')
  );

  // default params
  VTreeColors.ColorForOK := RGB(236,255,242); // green
  VTreeColors.ColorForErr := RGB(255,236,236); // red
  VTreeColors.ColorForChanged := RGB(236,242,255); // blue
  VTreeColors.ColorForUnk := RGB(242,242,242); // grey

  VTreeColumnsState.Name[0] := 'Num';
  VTreeColumnsState.Name[1] := 'FileName';
  VTreeColumnsState.Name[2] := 'Status';
  VTreeColumnsState.Name[3] := 'Size';
  VTreeColumnsState.Name[4] := 'Verified';

  VTreeColumnsState.State[0].Size := 50;
  VTreeColumnsState.State[0].Position := 0;

  VTreeColumnsState.State[1].Size := 300;
  VTreeColumnsState.State[1].Position := 1;

  VTreeColumnsState.State[2].Size := 80;
  VTreeColumnsState.State[2].Position := 2;

  VTreeColumnsState.State[3].Size := 80;
  VTreeColumnsState.State[3].Position := 3;

  VTreeColumnsState.State[4].Size := 150;
  VTreeColumnsState.State[4].Position := 4;

  VTreeShowOpt.ShowWithOkStatus := True;
  VTreeShowOpt.ShowWithUnkStatus := True;
  VTreeShowOpt.SortColumn := NoColumn;
  VTreeShowOpt.SortDirection := Integer(sdAscending);

  // create and read config
  FFormConfig := TFormInfoConfig.Create(
    FConfigProvider.GetOrCreateSubItem('MainInfo'),
    Self.BoundsRect,
    VTreeColors,
    VTreeColumnsState,
    VTreeShowOpt,
    ''
  );

  // create and init tree view
  FVirtualTree := TVirtualStringTree.Create(nil);
  with FVirtualTree do begin
    Parent := pnlVT;
    OnFreeNode := OnVTFreeNode;
    OnGetText := OnVTGetText;
    OnBeforeCellPaint := OnVTBeforeCellPaint;
    OnCompareNodes := OnVTCompareNodes;
    OnHeaderClick := OnVTHeaderClick;
    TreeOptions.MiscOptions:=[toReadOnly];
    NodeDataSize := SizeOf(TTreeNodeRec);
    Left := 0;
    Top := 0;
    Align := alClient;
    Visible := True;

    ScrollBarOptions.ScrollBars := ssBoth;

    TreeOptions.MiscOptions := [
      toFullRepaintOnResize,
      toInitOnSave,
      toToggleOnDblClick,
      toWheelPanning
    ];

    TreeOptions.PaintOptions := [
      toShowButtons,
      toShowDropmark,
      toThemeAware,
      toUseBlendedImages
    ];

    TreeOptions.SelectionOptions := [
      toFullRowSelect
    ];

    Header.Options := [
      hoColumnResize,
      hoDblClickResize,
      hoDrag,
      hoHotTrack,
      hoRestrictDrag,
      hoVisible
    ];

    VTreeColumnsState := FFormConfig.TreeColumnsState;
    for I := 0 to 4 do begin
      with Header.Columns.Add do begin
        Text := VTreeColumnsState.Name[I];
        Position := VTreeColumnsState.State[I].Position;
        Width := VTreeColumnsState.State[I].Size;
        if I = 0 then begin // column Num
          Options := [
            coAllowClick,
            coEnabled,
            coParentBidiMode,
            coParentColor,
            coResizable,
            coVisible,
            coAutoSpring
          ];
        end else begin
          Options := [
            coAllowClick,
            coDraggable,
            coEnabled,
            coParentBidiMode,
            coParentColor,
            coResizable,
            coShowDropMark,
            coVisible,
            coAutoSpring
          ];
        end;
        if I = 3 then begin // column Size
          Alignment := taRightJustify;
          CaptionAlignment := taLeftJustify;
        end;
      end;
    end;
    Header.AutoSizeIndex := 0;
    VTreeShowOpt := FFormConfig.TreeShowOpt;
    Header.SortColumn := VTreeShowOpt.SortColumn;
    Header.SortDirection := TSortDirection(VTreeShowOpt.SortDirection);
  end;

  FTreeColors := FFormConfig.TreeColors;

  edtPath.Text := FFormConfig.CachePath;
  chkShowOk.Checked := FFormConfig.TreeShowOpt.ShowWithOkStatus;
  chkShowUnk.Checked := FFormConfig.TreeShowOpt.ShowWithUnkStatus;
  Self.BoundsRect := FFormConfig.GetBoundsRect;

  ZeroMemory(@FStat, SizeOf(TStatRec));

  Self.Caption := Self.Caption + ' ' + GetAppVersion;
end;

procedure TfrmMainInfo.FormDestroy(Sender: TObject);
var
  I: Integer;
  VColumn: TVirtualTreeColumn;
  VTreeColumnsState: TTreeColumnsState;
  VTreeShowOptRec: TTreeShowOptRec;
begin
  if Length(VTreeColumnsState.State) = FVirtualTree.Header.Columns.Count then begin
    for I := 0 to FVirtualTree.Header.Columns.Count - 1 do begin
      VColumn := FVirtualTree.Header.Columns.Items[I];
      VTreeColumnsState.State[I].Size := VColumn.Width;
      VTreeColumnsState.State[I].Position := VColumn.Position;
    end;
    FFormConfig.TreeColumnsState := VTreeColumnsState;
  end;

  VTreeShowOptRec := FFormConfig.TreeShowOpt;
  VTreeShowOptRec.SortColumn := FVirtualTree.Header.SortColumn;
  VTreeShowOptRec.SortDirection := Integer(FVirtualTree.Header.SortDirection);
  FFormConfig.TreeShowOpt := VTreeShowOptRec;

  FFormConfig.SetWindowPosition(Self.BoundsRect);
  FFormConfig := nil; // save config
end;

procedure TfrmMainInfo.OnVTFreeNode(Sender: TBaseVirtualTree; Node: PVirtualNode);
var
  VData: PTreeNodeRec;
begin
  VData := Sender.GetNodeData(Node);
  if Assigned(VData) then begin
    Finalize(VData^);
  end;
end;

procedure TfrmMainInfo.OnVTHeaderClick(Sender: TVTHeader; HitInfo: TVTHeaderHitInfo);
begin
  if HitInfo.Column = 0 then begin
    Sender.SortColumn := NoColumn;
    Exit;
  end;
  if Sender.SortColumn = HitInfo.Column then begin
    if Sender.SortDirection = sdAscending then begin
      Sender.SortDirection := sdDescending;
    end else begin
      Sender.SortDirection := sdAscending;
    end;
  end else begin
    Sender.SortColumn := HitInfo.Column;
  end;
end;

procedure TfrmMainInfo.OnVTCompareNodes(Sender: TBaseVirtualTree; Node1,
  Node2: PVirtualNode; Column: TColumnIndex; var Result: Integer);
var
  VData1, VData2: PTreeNodeRec;
begin
  Result := 0;
  if Column = FVirtualTree.Header.SortColumn then begin
    VData1 := Sender.GetNodeData(Node1);
    VData2 := Sender.GetNodeData(Node2);
    case Column of
      1: Result := CompareStringOrdinal(VData1.FileName, VData2.FileName);
      2: Result := Integer(VData1.VerifyStatus) - Integer(VData2.VerifyStatus);
      3: Result := VData1.FileSize - VData2.FileSize;
      4: Result := CompareDateTime(VData1.VerifyTime, VData2.VerifyTime);
    end;
  end;
end;

function VerifyStatusToStr(const AStatus: TBerkeleyDBVerifyStatus): string; inline;
begin
  case AStatus of
    vsNone:    Result := 'None';
    vsOK:      Result := 'OK';
    vsError:   Result := 'Error';
    vsAborted: Result := 'Aborted';
    vsBusy:    Result := 'Busy';
  else
    begin
      Result := 'Unknown';
      Assert(False, 'Unknown Verify Status value: ' + IntToStr(Integer(AStatus)));
    end;
  end;
end;

procedure TfrmMainInfo.OnVTGetText(Sender: TBaseVirtualTree; Node: PVirtualNode;
  Column: TColumnIndex; TextType: TVSTTextType; var CellText: UnicodeString);
var
  VData: PTreeNodeRec;
begin
  VData := Sender.GetNodeData(Node);
  if Assigned(VData) then begin
    case Column of
      0: CellText := IntToStr(Node.Index + 1);

      1: CellText := VData.FileName;

      2: begin
        if (VData.VerifyStatus = vsOK) and VData.IsChanged then begin
          CellText := 'Changed';
        end else begin
          CellText := VerifyStatusToStr(VData.VerifyStatus);
        end;
      end;

      3: CellText := DataSizeToStr(VData.FileSize) + '  ';

      4: begin
        if VData.VerifyStatus = vsNone then begin
          CellText := '';
        end else begin
          CellText := FormatDateTime('yyyy-mm-dd hh:mm:ss.zzz', VData.VerifyTime);
        end;
      end;
    end;
  end;
end;

procedure TfrmMainInfo.OnVTBeforeCellPaint(Sender: TBaseVirtualTree;
  TargetCanvas: TCanvas; Node: PVirtualNode; Column: TColumnIndex;
  CellPaintMode: TVTCellPaintMode; CellRect: TRect; var ContentRect: TRect);
var
  VData: PTreeNodeRec;
  VColor: TColor;
begin
  VData := Sender.GetNodeData(Node);
  if Assigned(VData) then begin
    case VData.VerifyStatus of
      vsOK: begin
        if VData.IsChanged then begin
          VColor := FTreeColors.ColorForChanged;
        end else begin
          VColor := FTreeColors.ColorForOK;
        end;
      end;
      vsError, vsBusy: VColor := FTreeColors.ColorForErr;
    else
      VColor := FTreeColors.ColorForUnk;
    end;
    TargetCanvas.Brush.Color := VColor;
    TargetCanvas.FillRect(CellRect);
  end;
end;

procedure TfrmMainInfo.tmrProgressCheckTimer(Sender: TObject);
begin
  with FStat do begin
    statBottom.Panels.Items[0].Text := 'Size: ' + DataSizeToStr(FSize);
    statBottom.Panels.Items[1].Text := Format(
      'Total: %d Ok: %d Err: %d Bad: %d Unk: %d',
      [FCountTotal, FCountOk, FCountErr, FCountBad, FCountUnk]
    );
  end;
end;

procedure TfrmMainInfo.btnExitClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMainInfo.btnRunClick(Sender: TObject);
var
  VFileName: string;
  VFullFileName: string;
  VRootFolder: string;
  VIterator: IFileNameIterator;
  VCache: IBerkeleyDBVerifyCache;
  VNode: PVirtualNode;
  VData: PTreeNodeRec;
  VIsChanged: Boolean;
  VFileInfo: TFileInfoRec;
  VDoAddNode: Boolean;
  VIsBadFile: Boolean;
  VIsExistInCache: Boolean;
begin
  ZeroMemory(@FStat, SizeOf(TStatRec));
  tmrProgressCheck.Enabled := True;
  FVirtualTree.Clear;
  FVirtualTree.Enabled := False;
  FVirtualTree.BeginUpdate;
  try
    VRootFolder := Trim(edtPath.Text);
    if VRootFolder = '' then begin
      MessageDlg('Set cache path first!', mtError, [mbOK], 0);
      Exit;
    end;
    VRootFolder := IncludeTrailingPathDelimiter(VRootFolder);
    VCache := TBerkeleyDBVerifyCache.Create('db_verify.db3', nil);
    if not VCache.IsAvailable then begin
      if MessageDlg(rsVerifyCacheNotAvailable, mtError, [mbIgnore, mbAbort], 0) = mrAbort then begin
        Exit;
      end;
    end;
    VIterator := CreateFileNameIterator(VRootFolder, ['*.*'], True, True);
    while VIterator.Next(VFileName) do begin
      Application.ProcessMessages;
      VFullFileName := VRootFolder + VFileName;
      VIsBadFile := IsBerkeleyDBBadFile(VFullFileName);
      if VIsBadFile or IsBerkeleyDBCacheFile(VFullFileName) then begin
        Inc(FStat.FCountTotal);
        VDoAddNode := True;
        if not VIsBadFile then begin
          VIsExistInCache := VCache.Read(VFullFileName, VIsChanged, VFileInfo);
        end else begin
          VIsExistInCache := False;
          Inc(FStat.FCountBad);
        end;

        if VIsExistInCache then begin
          case VFileInfo.FVerifyStatus of
            vsOK: begin
              if VIsChanged then begin
                Inc(FStat.FCountUnk);
                VDoAddNode := chkShowUnk.Checked;
              end else begin
                Inc(FStat.FCountOk);
                VDoAddNode := chkShowOk.Checked;
              end;
            end;
            vsError, vsBusy, vsNone: begin
              Inc(FStat.FCountErr);
            end;
            vsAborted: begin
              Inc(FStat.FCountUnk);
              VDoAddNode := chkShowUnk.Checked;
            end;
          end;
        end else begin
          Inc(FStat.FCountUnk);
          VDoAddNode := chkShowUnk.Checked;
        end;

        if not VDoAddNode then begin
          Continue;
        end;

        VNode := FVirtualTree.AddChild(nil);
        VData := FVirtualTree.GetNodeData(VNode);
        if Assigned(VData) then begin
          VData.FileName := VFileName;
          VData.FileSize := GetFileSize(VFullFileName);
          Inc(FStat.FSize, VData.FileSize);
          if VIsExistInCache then begin
            VData.IsChanged := VIsChanged;
            VData.VerifyStatus := VFileInfo.FVerifyStatus;
            VData.VerifyTime := VFileInfo.FVerifyTime;
          end else begin
            VData.VerifyStatus := vsNone;
            VData.VerifyTime := 0;
          end;
        end;
      end;
    end;
  finally
    FVirtualTree.EndUpdate;
    FVirtualTree.Enabled := True;
    tmrProgressCheck.Enabled := False;
  end;
  tmrProgressCheckTimer(Self);
end;

procedure TfrmMainInfo.btnSelectCachePathClick(Sender: TObject);
var
  VPath: string;
begin
  VPath := edtPath.Text;
  if SelectDirectory('Select Directory', '', VPath) then begin
    edtPath.Text := IncludeTrailingPathDelimiter(VPath);
  end;
end;

procedure TfrmMainInfo.OnShowOptClick(Sender: TObject);
var
  VOpt: TTreeShowOptRec;
begin
  VOpt.ShowWithOkStatus := chkShowOk.Checked;
  VOpt.ShowWithUnkStatus := chkShowUnk.Checked;
  FFormConfig.TreeShowOpt := VOpt;
end;

procedure TfrmMainInfo.edtPathChange(Sender: TObject);
begin
  FFormConfig.CachePath := Trim(edtPath.Text);
end;

end.
