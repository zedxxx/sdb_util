unit t_BerkeleyDB;

interface

type
  TRecID = Int64;

  TBerkeleyDBVerifyStatus = (
    vsNone = 0,
    vsOK,
    vsError,
    vsAborted,
    vsBusy
  );

  TBerkeleyDBFileType = (
    ftUnk = 0,
    ftSDB,
    ftSDBv,
    ftTNE,
    ftTNEv
  );

implementation

end.
