unit i_BerkeleyUtilsConfig;

interface

uses
  i_DbConfigPresetList;

type
  TVerifyMode = (vmReport = 0, vmRename = 1, vmRemove = 2, vmRestore = 3);
  TDumpMode = (dmDefault = 0, dmSalvage = 1, dmAggressiveSalvage = 2);
  TUtilType = (utAuto = 0, utRecover = 1, utResetLSN = 2, utVerify = 3, utRestore = 4);
  
  IBerkeleyUtilsConfigStatic = interface
    ['{8A35ABB5-2AEB-4EA8-9E25-ABD7B67F4B48}']
    function GetRootFolder: string;
    property RootFolder: string read GetRootFolder;

    function GetWithSubFolders: Boolean;
    property WithSubFolders: Boolean read GetWithSubFolders;

    function GetCatastrophicRecover: Boolean;
    property CatastrophicRecover: Boolean read GetCatastrophicRecover;

    function GetRemoveEnvOnLSNReset: Boolean;
    property RemoveEnvOnLSNReset: Boolean read GetRemoveEnvOnLSNReset;

    function GetRemoveBadFileOnSuccessRestore: Boolean;
    property RemoveBadFileOnSuccessRestore: Boolean read GetRemoveBadFileOnSuccessRestore;

    function GetDumpMode: TDumpMode;
    property DumpMode: TDumpMode read GetDumpMode;

    function GetVerifyMode: TVerifyMode;
    property VerifyMode: TVerifyMode read GetVerifyMode;

    function GetUtilType: TUtilType;
    property UtilType: TUtilType read GetUtilType;

    function GetOverwriteDbConfig: Boolean;
    property OverwriteDbConfig: Boolean read GetOverwriteDbConfig;

    function GetDbConfigPresetList: IDbConfigPresetListStatic;
    property DbConfigPresetList: IDbConfigPresetListStatic read GetDbConfigPresetList;

    function GetUseDumpCleaner: Boolean;
    property UseDumpCleaner: Boolean read GetUseDumpCleaner;

    function GetUsePipelineOnRestore: Boolean;
    property UsePipelineOnRestore: Boolean read GetUsePipelineOnRestore;
  end;

  IBerkeleyUtilsConfig = interface
    ['{8A35ABB5-2AEB-4EA8-9E25-ABD7B67F4B48}']   
    function GetRootFolder: string;
    procedure SetRootFolder(const AValue: string);
    property RootFolder: string read GetRootFolder write SetRootFolder;

    function GetWithSubFolders: Boolean;
    procedure SetWithSubFolders(const AValue: Boolean);
    property WithSubFolders: Boolean read GetWithSubFolders write SetWithSubFolders;

    function GetCatastrophicRecover: Boolean;
    procedure SetCatastrophicRecover(const AValue: Boolean);
    property CatastrophicRecover: Boolean read GetCatastrophicRecover write SetCatastrophicRecover;

    function GetRemoveEnvOnLSNReset: Boolean;
    procedure SetRemoveEnvOnLSNReset(const AValue: Boolean);
    property RemoveEnvOnLSNReset: Boolean read GetRemoveEnvOnLSNReset write SetRemoveEnvOnLSNReset;

    function GetRemoveBadFileOnSuccessRestore: Boolean;
    procedure SetRemoveBadFileOnSuccessRestore(const AValue: Boolean);
    property RemoveBadFileOnSuccessRestore: Boolean read GetRemoveBadFileOnSuccessRestore write SetRemoveBadFileOnSuccessRestore;

    function GetDumpMode: TDumpMode;
    procedure SetDumpMode(const AValue: TDumpMode);
    property DumpMode: TDumpMode read GetDumpMode write SetDumpMode;

    function GetVerifyMode: TVerifyMode;
    procedure SetVerifyMode(const AValue: TVerifyMode);
    property VerifyMode: TVerifyMode read GetVerifyMode write SetVerifyMode;

    function GetUtilType: TUtilType;
    procedure SetUtilType(const AValue: TUtilType);
    property UtilType: TUtilType read GetUtilType write SetUtilType;

    function GetOverwriteDbConfig: Boolean;
    procedure SetOverwriteDbConfig(const AValue: Boolean);
    property OverwriteDbConfig: Boolean read GetOverwriteDbConfig write SetOverwriteDbConfig;

    function GetDbConfigPresetList: IDbConfigPresetList;
    property DbConfigPresetList: IDbConfigPresetList read GetDbConfigPresetList;

    function GetUseDumpCleaner: Boolean;
    procedure SetUseDumpCleaner(const AValue: Boolean);
    property UseDumpCleaner: Boolean read GetUseDumpCleaner write SetUseDumpCleaner;

    function GetUsePipelineOnRestore: Boolean;
    procedure SetUsePipelineOnRestore(const AValue: Boolean);
    property UsePipelineOnRestore: Boolean read GetUsePipelineOnRestore write SetUsePipelineOnRestore;

    function CreateStatic: IBerkeleyUtilsConfigStatic;
  end;

implementation

end.
