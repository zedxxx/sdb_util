object frmConfigLog: TfrmConfigLog
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsToolWindow
  Caption = 'Logging Config'
  ClientHeight = 118
  ClientWidth = 244
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblFileLogMode: TLabel
    Left = 10
    Top = 58
    Width = 94
    Height = 13
    Caption = 'Log file write mode:'
  end
  object chkLimitMemLogSize: TCheckBox
    Left = 8
    Top = 8
    Width = 165
    Height = 17
    Caption = 'Limit in-memory log size, MB:'
    Checked = True
    State = cbChecked
    TabOrder = 0
    OnClick = chkLimitMemLogSizeClick
  end
  object btnCancel: TButton
    Left = 156
    Top = 85
    Width = 80
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = 'Cancel'
    TabOrder = 1
    OnClick = btnCancelClick
    ExplicitLeft = 633
    ExplicitTop = 447
  end
  object btnApply: TButton
    Left = 70
    Top = 85
    Width = 80
    Height = 25
    Align = alCustom
    Anchors = [akRight, akBottom]
    Caption = 'Apply'
    TabOrder = 2
    OnClick = btnApplyClick
    ExplicitLeft = 547
    ExplicitTop = 447
  end
  object cbbFileLogMode: TComboBox
    Left = 124
    Top = 54
    Width = 112
    Height = 21
    ItemHeight = 13
    TabOrder = 3
    Text = 'Normal'
    OnChange = cbbFileLogModeChange
    Items.Strings = (
      'None'
      'Normal'
      'Extreme')
  end
  object chkTruncateLog: TCheckBox
    Left = 8
    Top = 31
    Width = 237
    Height = 17
    Caption = 'Truncate log file at start'
    Checked = True
    State = cbChecked
    TabOrder = 4
  end
  object seMemLogSize: TSpinEdit
    Left = 179
    Top = 6
    Width = 57
    Height = 22
    MaxValue = 1000
    MinValue = 1
    TabOrder = 5
    Value = 1
  end
end
