unit i_FormInfoConfig;

interface

uses
  Types;

type
  TColorsRec = record
    ColorForOK: Integer;
    ColorForErr: Integer;
    ColorForChanged: Integer;
    ColorForUnk: Integer;
  end;

  TTreeColumnRec = record
    Size: Integer;
    Position: Integer;
  end;

  TTreeColumnsState = record
    Name: array [0..4] of string; // fixed values
    State: array [0..4] of TTreeColumnRec;
  end;

  TTreeShowOptRec = record
    ShowWithOkStatus: Boolean;
    ShowWithUnkStatus: Boolean;
    SortColumn: Integer;
    SortDirection: Integer;
  end;

  IFormInfoConfig = interface
    ['{75C6158A-1E29-4158-9186-E16F16F54C0C}']
    function GetBoundsRect: TRect;
    procedure SetWindowPosition(const ARect: TRect);

    function GetTreeColors: TColorsRec;
    procedure SetTreeColors(const AColors: TColorsRec);
    property TreeColors: TColorsRec read GetTreeColors write SetTreeColors;

    function GetTreeColumnsState: TTreeColumnsState;
    procedure SetTreeColumnsState(const AValue: TTreeColumnsState);
    property TreeColumnsState: TTreeColumnsState read GetTreeColumnsState write SetTreeColumnsState;

    function GetTreeShowOpt: TTreeShowOptRec;
    procedure SetTreeShowOpt(const AValue: TTreeShowOptRec);
    property TreeShowOpt: TTreeShowOptRec read GetTreeShowOpt write SetTreeShowOpt;

    function GetCachePath: string;
    procedure SetCachePath(const AValue: string);
    property CachePath: string read GetCachePath write SetCachePath;
  end;

implementation

end.
