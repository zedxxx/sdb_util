unit u_FileNameIteratorHelper;

interface

uses
  i_FileNameIterator;

function CreateFileNameIterator(
  const ARootDir: string;
  const AMasksList: array of string;
  const AFilesOnly: Boolean;
  const AWithSubFolders: Boolean
): IFileNameIterator;

implementation

uses
  Classes,
  u_FoldersIteratorRecursiveByLevels,
  u_FileNameIteratorInFolderByMask,
  u_FileNameIteratorInFolderByMaskList,
  u_FileNameIteratorFolderWithSubfolders;

function CreateFileNameIterator(
  const ARootDir: string;
  const AMasksList: array of string;
  const AFilesOnly: Boolean;
  const AWithSubFolders: Boolean
): IFileNameIterator;
const
  cMaxFolderDepth = 100;
var
  I: Integer;
  VProcessFileMasks: TStringList;
  VFoldersIteratorFactory: IFileNameIteratorFactory;
  VFilesInFolderIteratorFactory: IFileNameIteratorFactory;
begin
  VProcessFileMasks := TStringList.Create;
  try
    for I := 0 to Length(AMasksList) - 1 do begin
      VProcessFileMasks.Add(AMasksList[I]);
    end;
    if AWithSubFolders then begin
      VFoldersIteratorFactory :=
        TFoldersIteratorRecursiveByLevelsFactory.Create(cMaxFolderDepth);

      VFilesInFolderIteratorFactory :=
        TFileNameIteratorInFolderByMaskListFactory.Create(
          VProcessFileMasks,
          AFilesOnly
        );

      Result := TFileNameIteratorFolderWithSubfolders.Create(
        ARootDir,
        '',
        VFoldersIteratorFactory,
        VFilesInFolderIteratorFactory
      );

    end else begin
      Result := TFileNameIteratorInFolderByMaskList.Create(
          ARootDir,
          '',
          VProcessFileMasks,
          AFilesOnly
        );
    end;
  finally
    VProcessFileMasks.Free;
  end;
end;

end.
