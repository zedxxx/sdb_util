unit u_BerkeleyDBTools;

interface

uses
  t_BerkeleyDB;

type
  TBerkeleyDBFileNameParser = record
    FZoom: Byte;
    FFileX: Int64;
    FFileY: Int64;
    FFileType: TBerkeleyDBFileType;
    FFileName: string;
    FCachePath: string;
    function DoParse(const AFileName: string): Boolean;
  end;

function IsBerkeleyDBCacheFile(const AFileName: string): Boolean;
function IsBerkeleyDBBadFile(const AFileName: string): Boolean; inline;
function IsBerkeleyDBEnvFolder(const ADir: string): Boolean; inline;

implementation

uses
  SysUtils,
  RegExpr;

{ TBerkeleyDBFileNameParser }

function TBerkeleyDBFileNameParser.DoParse(const AFileName: string): Boolean;
var
  VExt: string;
  VRegExpr: TRegExpr;
begin
  Result := False;
  FFileName := AnsiLowerCase(AFileName);
  VRegExpr := TRegExpr.Create;
  try
    VRegExpr.Expression := '(z(\d+)\\(\d+)\\(\d+)\\(\d+)\.(\d+)\.(\w+)$)';
    if VRegExpr.Exec(FFileName) then begin
      FCachePath := StringReplace(FFileName, VRegExpr.Match[1], '', []);
      FFileX := StrToInt(VRegExpr.Match[5]);
      FFileY := StrToInt(VRegExpr.Match[6]);
      FZoom := StrToInt(VRegExpr.Match[2]) - 1;
      VExt := VRegExpr.Match[7];
      if SameStr(VExt, 'sdb') then begin
        FFileType := ftSDB;
      end else if SameStr(VExt, 'sdbv') then begin
        FFileType := ftSDBv;
      end else if SameStr(VExt, 'tne') then begin
        FFileType := ftTNE;
      end else if SameStr(VExt, 'tnev') then begin
        FFileType := ftTNEv;
      end else begin
        FFileType := ftUnk;
        Assert(False, 'Unknown file extension: ' + VExt);
      end;
      Result := True;
    end;
  finally
    VRegExpr.Free;
  end;
end;

function IsBerkeleyDBCacheFile(const AFileName: string): Boolean;
const
  cBerkeleyDBFiles: array [0..3] of string = (
    '.sdbv', '.sdb', '.tnev', '.tne'
  );
var
  I: Integer;
  VExt: string;
begin
  Result := False;
  VExt := LowerCase(ExtractFileExt(AFileName));
  if VExt <> '' then begin
    for I := Low(cBerkeleyDBFiles) to High(cBerkeleyDBFiles) do begin
      if VExt = cBerkeleyDBFiles[I] then begin
        Result := FileExists(AFileName);
        Break;
      end;
    end;
  end;
end;

function IsBerkeleyDBBadFile(const AFileName: string): Boolean;
var
  VExt: string;
begin
  VExt := LowerCase(ExtractFileExt(AFileName));
  Result := (VExt = '.bad') and FileExists(AFileName);
end;

function IsBerkeleyDBEnvFolder(const ADir: string): Boolean; inline;
begin
  Result :=
    (ADir <> '') and
    (Pos('env', ADir) > 0) and
    DirectoryExists(IncludeTrailingPathDelimiter(ADir));
end;

end.
